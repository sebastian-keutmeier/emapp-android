# Energica BLE App (Android) #

## What is this repository for?

This is a an open source app that connects to Energica electric motorcycles over Bluetooth 
low energy (BLE) to collect and display information received from the bike.

For non Energica motorcycles later on we also added generic OBDII support.
And more generic tools like the range calculator.

Android Play Store link https://play.google.com/store/apps/details?id=be.hcpl.android.energica

iOS App Store link https://apps.apple.com/app/id1559468042

Zero specific Android App https://play.google.com/store/apps/details?id=be.hcpl.android.zengo

Zero specific iOS App https://apps.apple.com/us/app/zerong/id1488172044

![App screenshot 1](https://bitbucket.org/cappelleh/emapp-android/raw/52cfc37c7bb9fcf597fd0213f7b5e2b081c7f6b4/release/Screenshot_1635011310.png "App Screenshot 1")
![App screenshot 2](https://bitbucket.org/cappelleh/emapp-android/raw/d2c42c9097e54211a81d38481790cd2b1498e5ab/release/Screenshot_1635011312.png "App Screenshot 2")
![App screenshot 3](https://bitbucket.org/cappelleh/emapp-android/raw/d2c42c9097e54211a81d38481790cd2b1498e5ab/release/Screenshot_1635011324.png "App Screenshot 3")
![App screenshot 4](https://bitbucket.org/cappelleh/emapp-android/raw/d2c42c9097e54211a81d38481790cd2b1498e5ab/release/Screenshot_1635011340.png "App Screenshot 4")

## Roots ##

Energica BLE connection is based on Official MyEnergica Android App version 1.9.5.

```
APPLICATION_ID = "com.energica.myenergica";
VERSION_CODE = 5;
VERSION_NAME = "1.9.5";
```

Generic OBDII connection is based on an opensource Android app with the name 
[Android OBD reader](https://app.circleci.com/pipelines/github/pires/android-obd-reader).

DISCLAIMER: USE AT YOUR OWN RISK. Shouldn't harm the bike since we are just submitting a few info 
requests and the known horn, trip reset and stop charge commands. Just to be sure I removed all 
input fields so all requests are fixed.  

## How do I get set up?

### Installation 

This is the [play store link](https://play.google.com/store/apps/details?id=be.hcpl.android.energica).
Or check downloads on this repo or the build pipelines in case you don't like the Google Play Store.
You can also look in the release folders for debug apk builds. Note that these aren't signed so require
some development options checked on your device, see next paragraph.

For non play store (debug) builds you'll have to allow installation of apps from unknown sources on your device. 
See [these instructions](https://www.androidauthority.com/how-to-install-apks-31494/) for more details. 
Note that this setting has moved a lot between Android versions and manufacturers also change it. 
So not always straight forward. If you can't find it it's best to just download it from the app store 
and wait for an update there with the latest features.

### Connecting w/ Energica

If you have the official MyEnergica app on your phone installed already and set up this app should connect right away. 
On modern Android versions the app will request permission to run Bluetooth (that is the location request). 
 
If the connection doesn't work make sure the MyEnergica App is currently closed, look at the running apps on your device and kill it. 
Same goes the other way around, if you want to connect with the MyEnergica App you should make sure this app is closed. 

If you haven't had the phone connected to the bike yet or another phone was connected prior to this install make sure 
to remove the Bluetooth MAC address registered on your bike. The Energica motorcycles only allow connection to a single Device.
This can be reset from the menu on the dashboard. 
  
If the app or your device was used with another motorcycle before you'll also have to use the reset option 
from the settings screen of this app.

If you still don't get a connection try to restart the app (no need to reboot the phone) in combination with switching 
the bike OFF and ON again. That sometimes helps since both will try to connect on startup. 
Plus the bike can only be found using BLE scanning right after the startup.

There is no need to perform so called bluetooth device pairing. So if you're prompted for a bluetooth pin you're attempting
to pair the two. That is not how this connection works (it does for Zero but that's another story).

### Connecting w/ Generic OBDII

For this it should be sufficient to physically connect a Bluetooth OBDII adapter and then use your device 
bluetooth settings to scan for it create a connection. Once that connection is set up you'll find the device
listed in this app and connect from there. 

//TODO more information on what dongles work

### Troubleshooting

You can only have one physical device connected to your Energica motorcycle so either do this on the phone already 
connected (using MyEnergica app) or reset the connection on the bike first. 

You can have several apps installed on the device that can connect to this BLE interface but only one at a time. 
So if you currently have the MyEnergica app running in the background that one will connect and prevent this app 
to connect. Also true the other way around if you want to use the MyEnergica app. Just kill the other app from the 
running apps first.
  
Check the state description on the first view below the motorcycle to see if you're currently in Connected 
(state of bike showing) or Searching. Searching shouldn't take much longer than 30 seconds. If it takes longer 
go closer to your bike or check the other issues. 
 
If connection doesn't work it helps to kill and restart the app and to toggle the ignition key on the bike.

There is something in place that looks like an auto reconnect feature to prevent the connection to die when your phone 
is no longer active. I need to look into that feature though cause it doesn't look like it will work ATM. Fix coming soon.

## Pushing data to the cloud

Since version 2.5 of this app you can enable pushing data to a remote service provided by [https://ev-monitor.com](https://ev-monitor.com).
They provide a dashboard to visualize the data you push. That project is being developed right now so expect the 
dashboard to be extended soon.

By default nothing is shared, you'll have to register for an account with ev-monitor.com first. Once you have that
activated you can go into the settings within this app and enable pushing data and provide your credentials. With those
credentials you can also select a vehicle from the ones you've created on their dashboard. 

You can enable or disable pushing data any time. You can also separately check sharing of location or not. Location shared
is based on GPS data collected during routes logged, so only when the GPS service is running. The other data is shared only
when either the BLE or the OBD service is running. Stop either service is enough to prevent data being shared. 

## Exporting Data on device

All the data that is plotted in the graphs in this App can be exported as CSV or comma separated value files.
For that use the export data option on top of the graphs.

Next use a file browser (or use the Downloads app) on your phone to navigate to below directory where you'll find all the extracted files. 

```
internal storage > Android > data > be.hcpl.android.energica > files > Download
```   
 
## What this App will not provide 

The Official MyEnergica App is not a bad app. In fact it does a few things quite good, so good that I won't even bother 
implementing those in this app. Think of that horn button :D or the diagnostics screen. Ok fine, I'll keep the horn button. 
But the diagnostics I won't cause that is just too much work and really well implemented in the MyEnergica app.
  
I couldn't find the speed limit setting in the original app. So I'll keep that in for those that want a lower limit than 
the 90 kph that is set in ECO mode. Just be aware that it only changes the speed limit in ECO mode and that it doesn't work 
for anything higher than 90 kph (or lower than 30 kph).  
  

## What this App will focus on

Graphs, more graphs, data and basic functionality.


## Contribution guidelines

### Charge locations

Charge station integration is provided by https://openchargemap.io so do show them some love 
by getting their app. And once you've got their app installed and you're charging at a charger 
that isn't in their system yet you can just add it from there.

### Code contributions

This is a public repo so everyone can see code. However only limited people have access to publish code. 
If you want in just let me know. If you rather do your own thing feel free to branch this repo. 
 
For those pushing code here make sure to do so on develop branches. The limited free plan build pipelines are now configured 
to run only from the master branch. Once a release is tested and fine we can move to the master branch. 
 
Also tag releases so we have an easy way to check back on previous builds. 

[Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

[Learn BLE](https://developer.android.com/guide/topics/connectivity/bluetooth-le)

* Writing tests - but only if you are in the mood
* Code review - not required, just fork this repo
* Other guidelines - enjoy life

## Roadmap

### someday

* even more Energica specific can bus data decoded
* charge graph from OBD connection incomplete when view not active
* make pushing data interval configurable (on location, time...)
* fix reconnect when user disabled and enabled bluetooth

### maybe

* convert more code to kotlin (technical task)
* have a dummy setup for testing BLE data
* clean up use of event bus where no longer needed

## App Version History

### 3.O

* update Android target SDK and Google Maps SDK to latest versions
* merged EmApp range, blog and video views into this app

### 2.15

* small layout improvements
* fixed DC charging indication (ID 16)

### 2.14

* added battery voltage readout from Energica OBDII data
* implemented parsing Charge state field from Energica OBD2 data

### 2.13

* show warning when pushing data failed
* manual refresh option for log files
* fixed logical naming of auto connect feature settings toggles

### 2.12

* OBD2 bluetooth connection improvements 

### 2.11

* implement auto reconnect for OBDII (like BLE has)

### 2.10

* fixed plotting time based graph
* added time to logs from OBD2 data
* use icons on map view toolbar to start/stop services

### 2.9

* prevent pushing empty data to cloud service
* limit logs to last hour + made more readable
* allow pushing debug data to cloud
* debug data generation option added for Energica OBDII data

### 2.8

* fixed BLE connection state values from service
* fixed OBDII connection state values from service
* new icon for OBDII connection service
* replace default values with dashes (--) 
* also show OBDII SOC and temp data on map
* added time label on X axis for second OBDII graph
* update app menu layout for faster OBD and BLE access

### 2.7

* fixed crash on first use of app logs
* improved state update (in progress) from OBD connection service
* fixed obfuscation issue for new network calls

### 2.6

* fixed fetching vehicles
* move BLE connection to service (like GPS)
* fixed app logs using filesystem

### 2.5

* fixed viewport & layout on time based OBD2 graph
* fixed generic odometer calculated value

### 2.4

* fix parsing for ODO meter for supported vehicles
* allow for switching between time and soc based graph
* initial ev-monitor cloud storage integration

### 2.3

* add support for generic OBD2 connectoin (Zero and more)
* reorganise screens

### 2.2

* filter too high values from OBDII
* allow for notifications on changed SOC also (every 5%)
* improve connection state for OBDII
* show all updated values on each notification update
* fixed crashes on parsing data (StringIndexOutOfBoundsException and NumberFormatException)

### 2.1

* only show useful OBD2 output
* add charging graph from OBD2 data (current, temp, soc)
* fix sticky service for ob2 including notification
* show changes in temp and current as a notification

### 2.0

* add optional speed as a big overlay on map (personal feature)
* add support for OBDII dongles
* remember last used OBD device for faster connect

### 1.17

* added openchargemap.io reference to about view in app
* don't start gps tracking on resume, have a start tracking option in the settings instead
* pass preferred unit from config to openchargemap api (and update literals)
* include expected range left from calculated range values
* let user configure what fields should be visible on map overlay

### 1.16

* show nearby chargers from public api (openchargemap because it's free)
* add preferences for nearby charger fetching
* small layout improvements
* allow for navigation to nearby chargers

### 1.15

* option added for sending logs to dev
* reduced GPS position update statements in logs
* calculate actual average consumption from distance (odo) and battery used
* more small layout and color fixes
* don't use bike speed values for stats
* ignore bike GPS data when device GPS data is enabled (should improve distance measurement)

### 1.14

* fixed reset BLE connection from new settings
* small layout fixes

### 1.13

* Another auto reconnect improvement
* have a settings screen so more room left for map itself
* an in app about screen added with source code link
* connection state icons added in toolbar
* allow manual override of mi vs km units (for non BLE Energica users)
* also show a notification while service is running on older devices
* optionally log directly to gpx on device for backup

### 1.12

* more BLE connection issues resolved
* improved ConnectionManager code
* added warning about reset function

### 1.11

* faster initial location lock
* Fixed manual connect/reconnect while bike still ON (w/o app restart)
* Improved detection of connection lost (bike key OFF)
* improved auto connect on startup and permission and bluetooth setting changed 
* auto reconnects after bike OFF and ON keyswitch toggle
* have force use of phone GPS checked by default
* added reserve indication (Wh) on maps view
* keep last connection values visible on map

### 1.10

* bug fix for speed indication above map
* fixed missing plotted route on standby
* fixed background GPS on Android 8 and higher
* added confirmation on data export
* fixed crash on resuming app after being in background for longer
* show last known location on map right away if available

### 1.9

* add ride stats for logged route 
    * avg speed, top speed, gps speed, bike speed
    * number of charge stops, total charge time (bike in CHARGE mode or manual)
    * total distance of route (GPS based)
* allow for manual control of charge stops (start and stop charge session)
* include charge stops on map in export data for gpx file

### 1.8

* use readable date timestamps for exported files
* show ride speed on top of map (current GPS, bike and avg)
* show charge stops on route (initial implementation)
* small visual improvements
* reset route option? with a big warning

### 1.7

* fixed release build (obfuscation)
* alternative debug app name
* reduced in app log output gps data
* confirm gpx export with Toast message

### 1.6

* add gpx route export option and log route to file while riding
* initial map view implementation
* added event bus to push data to views
* allow use of device gps only instead of bike

### 1.5

* added export data option and direct to file logging
* improved documentation
* small screen UI improvements
* made app compatible with android 5.0 (api level 21)

### 1.4

* fixed several connect and state issues
* replaced deprecated BLE scanning code
* changed ACCESS_COARSE_LOCATION to ACCESS_FINE_LOCATION for compatibility 
* more code clean up and improvements
* small UI changes

### 1.3

* removed demo mode completely
* simplified loop logic and reduced data update refresh rate
* added app version to logs data view

### 1.2

* improved readability of charge graphs
* more charge values displayed
* allow for resetting charge graph and change scale (AC/DC)
* removal of manual input fields for security reasons

### 1.1 

* code clean up removing unused logic
* two initial graphs implemented
* removed diagnostics button
* completed charging graph added more charging numbers on view

### 1.0

Initial app release

## References

BLE disconnects were partly fixed in 1.11 but then I still encountered disconnects after the initial connection was made.
Could be a device timeout https://stackoverflow.com/questions/44785996/android-ble-device-continually-disconnects-and-re-connects-every-30-seconds

Since version 1.12 handling of already "discovered" device is improved so we don't have to scan on each connect.

Implemented graphs with [this library](https://tjah.medium.com/how-to-create-a-simple-graph-in-android-6c484324a4c1)

Event bus https://github.com/greenrobot/EventBus note that this only works when app is in foreground,
thus for service communication there is still intent broadcasting in place.

Calculate average speed https://stackoverflow.com/questions/8555102/how-to-calculate-moving-average-speed-from-gps

App Compat toolbar https://guides.codepath.com/android/using-the-app-toolbar

Android Dialog theme styling https://web.archive.org/web/20200214041924/http://blog.supenta.com/2014/07/02/how-to-style-alertdialogs-like-a-pro/

Chargemap api docs https://openchargemap.io/site/develop/api