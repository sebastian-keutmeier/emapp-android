package be.hcpl.android.energica.event

import be.hcpl.android.energica.helpers.Const.DISTANCE_UNIT_MI
import be.hcpl.android.energica.helpers.Const.KM_TO_MI
import java.text.DecimalFormat

data class StatsEvent(
    val currentUnitBike: Int,
    val currentUnitSystem: Int,
    val currentSpeedGps: Int,
    val currentSpeedBike: Int,
    val averageSpeed: Int,
    val topSpeed: Int,
    val numberOfChargeStops: Int,
    val totalChargeTimeInMs: Long,
    val totalDistance: Double // in meters
) {

    override fun toString(): String {
        val unitBike = if (currentUnitBike == DISTANCE_UNIT_MI) "mph" else "kph"
        val unitSystem = if (currentUnitSystem == DISTANCE_UNIT_MI) "mph" else "kph"
        val unitDistance = if (currentUnitSystem == DISTANCE_UNIT_MI) "mi" else "km"
        val distance = if (currentUnitSystem == DISTANCE_UNIT_MI) totalDistance / 1000 * KM_TO_MI else totalDistance / 1000
        return "currentSpeedGps=$currentSpeedGps $unitSystem\n" +
                "currentSpeedBike=$currentSpeedBike $unitBike\n" +
                "averageSpeed=$averageSpeed $unitSystem\n" +
                "topSpeed=$topSpeed $unitSystem\n" +
                "chargeStops=$numberOfChargeStops\n" +
                "totalChargeTime=${totalChargeTimeInMs / 1000 / 60} minutes\n" +
                "totalDistance=${DecimalFormat("#0.000").format(distance)} $unitDistance"
    }

}

