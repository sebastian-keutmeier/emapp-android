package be.hcpl.android.energica.event

import java.io.Serializable

class ConnectionRejectedEvent : Serializable
