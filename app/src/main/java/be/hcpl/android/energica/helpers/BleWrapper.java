package be.hcpl.android.energica.helpers;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.os.AsyncTask;
import android.os.ParcelUuid;

import org.greenrobot.eventbus.EventBus;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;

import be.hcpl.android.energica.event.NewStateEvent;
import be.hcpl.android.energica.interfaces.BleWrapperUiCallback;

import static android.bluetooth.le.ScanSettings.CALLBACK_TYPE_ALL_MATCHES;
import static android.bluetooth.le.ScanSettings.MATCH_MODE_AGGRESSIVE;
import static android.bluetooth.le.ScanSettings.SCAN_MODE_LOW_LATENCY;
import static be.hcpl.android.energica.helpers.Const.STATE_NOT_CONNECTED;
import static be.hcpl.android.energica.model.ble.BleDefinedUUIDs.Descriptor.CHAR_CLIENT_CONFIG;

@SuppressLint("MissingPermission") // see manifest
public class BleWrapper {

    public static final int BLE_STATE_CONNECTED = 2;
    public static final int BLE_STATE_DISCONNECTED = 0;
    public static final int STATE_OK = 0;

    private final Context context;
    private final BluetoothGattCallback bleCallback = new BluetoothGattCallback() {

        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if (newState == BLE_STATE_CONNECTED) {
                log("BLE service connected");
                connected = true;
                uiCallback.uiDeviceConnected(bluetoothGatt, bluetoothDevice);
                startServicesDiscovery();
            } else if (newState == BLE_STATE_DISCONNECTED) {
                log("BLE service disconnected");
                connected = false;
                uiCallback.uiDeviceDisconnected(bluetoothGatt, bluetoothDevice);
            }
        }

        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == STATE_OK) {
                getSupportedServices();
            }
        }

        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (status == STATE_OK) {
                getCharacteristicValue(characteristic);
            }
        }

        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            getCharacteristicValue(characteristic);
        }

        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (status == STATE_OK) {
                uiCallback.uiSuccessfulWrite(bluetoothGatt, bluetoothDevice, bluetoothSelectedService, characteristic);
            } else {
                uiCallback.uiFailedWrite(bluetoothGatt, bluetoothDevice, bluetoothSelectedService, characteristic);
            }
        }

        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            if (status == STATE_OK) {
                uiCallback.uiNewRssiAvailable(bluetoothGatt, bluetoothDevice, rssi);
            }
        }
    };

    private void log(final String message) {
        ExportData.Companion.log(context, message);
    }

    private BluetoothAdapter btAdapter = null;
    private BluetoothDevice bluetoothDevice = null;
    private BluetoothGatt bluetoothGatt = null;
    private List<BluetoothGattService> bluetoothGattServices = null;
    private BluetoothManager bluetoothManager = null;
    private BluetoothGattService bluetoothSelectedService = null;
    private boolean connected = false;
    private final BleWrapperUiCallback uiCallback;
    // new scan callback
    private final ScanCallback leScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            uiCallback.uiDeviceFound(result.getDevice(), result.getRssi(), result.getScanRecord().getBytes());
        }
    };

    public BleWrapper(Context parent, BleWrapperUiCallback callback) {
        context = parent;
        uiCallback = callback == null ? new BleWrapperUiCallback.Null() : callback;
    }

    public BluetoothAdapter getAdapter() {
        return btAdapter;
    }

    public BluetoothDevice getDevice() {
        return bluetoothDevice;
    }

    public boolean isConnected() {
        return connected;
    }

    public boolean isBtEnabled() {
        BluetoothAdapter adapter;
        BluetoothManager manager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        if (manager == null || (adapter = manager.getAdapter()) == null) {
            return false;
        }
        return adapter.isEnabled();
    }

    public void startScanning() {
        if( btAdapter == null ) {
            return;
        }
        // removed since deprecated btAdapter.startLeScan(new UUID[]{UUID.fromString(Const.ENERGICA_SERVICE_UUID)}, deviceFoundCallback);
        final BluetoothLeScanner btScanner = btAdapter.getBluetoothLeScanner();
        final List<ScanFilter> scanFilters = Collections.singletonList(new ScanFilter.Builder().setServiceUuid(ParcelUuid.fromString(Const.ENERGICA_SERVICE_UUID)).build());
        // for filter settings see https://developer.android.com/reference/android/bluetooth/le/ScanSettings
        final ScanSettings.Builder builder = new ScanSettings.Builder();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            builder
                .setCallbackType(CALLBACK_TYPE_ALL_MATCHES)
                .setMatchMode(MATCH_MODE_AGGRESSIVE);
        }
        final ScanSettings scanSettings = builder.setScanMode(SCAN_MODE_LOW_LATENCY).build();
        if (bluetoothGatt != null) {
            refreshDeviceCache(bluetoothGatt); // now done in disconnect also
        }
        AsyncTask.execute(() -> {
            if( btScanner != null ) {
                log("start BLE scanning");
                btScanner.startScan(scanFilters, scanSettings, leScanCallback);
            }
        });
    }

    public void stopScanning() {
        if( btAdapter == null ) {
            return;
        }
        // removed since deprecated btAdapter.stopLeScan(deviceFoundCallback);
        final BluetoothLeScanner btScanner = btAdapter.getBluetoothLeScanner();
        AsyncTask.execute(() -> {
            if( btScanner != null ) {
                log("stopped BLE scanning");
                btScanner.flushPendingScanResults(leScanCallback);
                btScanner.stopScan(leScanCallback);
            }
        });
    }

    public void initialize() {
        if (bluetoothManager == null) {
            bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        }
        if (btAdapter == null && bluetoothManager != null) {
            btAdapter = bluetoothManager.getAdapter();
        }
    }

    private void refreshDeviceCache(BluetoothGatt gatt) {
        try {
            Method localMethod = gatt.getClass().getMethod("refresh");
            boolean result = (Boolean) localMethod.invoke(gatt, new Object[0]);
            log("Executed refresh BLE device ("+result+")");
        } catch (Exception e) {
            log("An exception occurred while refreshing device");
        }
    }

    public boolean connect(final String deviceAddress) {
        if (btAdapter == null || deviceAddress == null) {
            return false;
        }
        bluetoothDevice = btAdapter.getRemoteDevice(deviceAddress);
        if (bluetoothDevice == null) {
            return false;
        }
        bluetoothGatt = bluetoothDevice.connectGatt(context, false, bleCallback);
        return true;
    }

    public void disconnect() {
        if (bluetoothGatt != null) {
            bluetoothGatt.disconnect();
            log("disconnecting from services");
            refreshDeviceCache(bluetoothGatt);
            bluetoothGatt = null;
        }
        uiCallback.uiDeviceDisconnected(bluetoothGatt, bluetoothDevice); // FIXME gatt will always be null here
    }

    public void close() {
        if (bluetoothGatt != null) {
            bluetoothGatt.close();
        }
        bluetoothGatt = null;
    }

    public void startServicesDiscovery() {
        if (bluetoothGatt != null) {
            bluetoothGatt.discoverServices();
        }
    }

    public void getSupportedServices() {
        if (bluetoothGattServices != null && bluetoothGattServices.size() > 0) {
            bluetoothGattServices.clear();
        }
        if (bluetoothGatt != null) {
            bluetoothGattServices = bluetoothGatt.getServices();
        }
        uiCallback.uiAvailableServices(bluetoothGatt, bluetoothDevice, bluetoothGattServices);
    }

    public void getCharacteristicsForService(BluetoothGattService service) {
        if (service != null) {
            uiCallback.uiCharacteristicForService(bluetoothGatt, bluetoothDevice, service, service.getCharacteristics());
            bluetoothSelectedService = service;
        }
    }

    public void getCharacteristicValue(android.bluetooth.BluetoothGattCharacteristic characteristic) {
        if (characteristic != null) {
            final byte[] value = characteristic.getValue();
            // unused UUID uuid = characteristic.getUuid();
            // removed some unused bit shifting here...
            uiCallback.uiNewValueForCharacteristic(characteristic, value);
        }
    }

    public void writeDataToCharacteristic(BluetoothGattCharacteristic ch, byte[] dataToWrite) {
        if (btAdapter != null && bluetoothGatt != null && ch != null) {
            ch.setValue(dataToWrite);
            bluetoothGatt.writeCharacteristic(ch);
        }
    }

    public void setNotificationForCharacteristic(BluetoothGattCharacteristic ch, boolean enabled) {
        if (btAdapter != null && bluetoothGatt != null) {
            if (!bluetoothGatt.setCharacteristicNotification(ch, enabled)) {
                log("Setting proper notification status for characteristic failed!");
            }
            BluetoothGattDescriptor descriptor = ch.getDescriptor(CHAR_CLIENT_CONFIG);
            if (descriptor != null) {
                descriptor.setValue(enabled ? BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE : BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
                bluetoothGatt.writeDescriptor(descriptor);
            }
        }
    }
}
