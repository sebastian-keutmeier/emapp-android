package be.hcpl.android.energica.model.ble;

import java.util.Date;

public class GpsData {

    public int altitude;
    public int connectedSatellites;
    public int course;
    public Date date;
    public int dateDay;
    public int dateHour;
    public long dateMilliseconds;
    public int dateMinutes;
    public int dateMonth;
    public int dateSeconds;
    public int dateYear;
    public int fix;
    public int latSign;
    public double latitude;
    public int latitudeDeciMilliminutes;
    public int latitudeDegrees;
    public int latitudeMinutes;
    public int lonSign;
    public double longitude;
    public int longitudeDeciMilliminutes;
    public int longitudeDegrees;
    public int longitudeMinutes;
    public int speed;

    public GpsData() {
        latSign = 1;
        lonSign = 1;
    }

    public double convertLat() {
        return ((double) latSign) * (((double) latitudeDegrees) + ((((double) latitudeMinutes) + (((double) latitudeDeciMilliminutes) / 10000.0d)) / 60.0d));
    }

    public double convertLon() {
        return ((double) lonSign) * (((double) longitudeDegrees) + ((((double) longitudeMinutes) + (((double) longitudeDeciMilliminutes) / 10000.0d)) / 60.0d));
    }

    @Override
    public String toString() {
        return "GpsData{" +
                "altitude=" + altitude +
                ", connectedSatellites=" + connectedSatellites +
                ", course=" + course +
                ", date=" + date +
                ", dateDay=" + dateDay +
                ", dateHour=" + dateHour +
                ", dateMilliseconds=" + dateMilliseconds +
                ", dateMinutes=" + dateMinutes +
                ", dateMonth=" + dateMonth +
                ", dateSeconds=" + dateSeconds +
                ", dateYear=" + dateYear +
                ", fix=" + fix +
                ", latSign=" + latSign +
                ", latitude=" + latitude +
                ", latitudeDeciMilliminutes=" + latitudeDeciMilliminutes +
                ", latitudeDegrees=" + latitudeDegrees +
                ", latitudeMinutes=" + latitudeMinutes +
                ", lonSign=" + lonSign +
                ", longitude=" + longitude +
                ", longitudeDeciMilliminutes=" + longitudeDeciMilliminutes +
                ", longitudeDegrees=" + longitudeDegrees +
                ", longitudeMinutes=" + longitudeMinutes +
                ", speed=" + speed +
                '}';
    }

}
