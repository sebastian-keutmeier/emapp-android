package be.hcpl.android.energica.config

import com.github.pires.obd.commands.protocol.ObdRawCommand
import okhttp3.internal.toHexString

class Energica201Command : ObdRawCommand("001") {

    override fun getName() = "Energica 201 001"

    override fun getFormattedResult(): String {
        return try{
            // 200 10 00 00 00 00 00 00 00
            "Charge State=${int(3, 5)}"
        } catch(e: Exception) {
            "ERROR ${e.localizedMessage} RAW=$result"
        }
    }

    fun getRawResult() =
        try {
            "DEC ${int(3, 5)} ${int(5, 7)} ${int(7, 9)} ${int(9, 11)} ${int(11, 13)} ${int(13, 15)} ${int(15, 17)} ${int(17, 19)}"
        } catch(e: Exception) {
            "ERROR ${e.localizedMessage} RAW=$result"
        }

    fun getChargeState() = int(3, 5)

    fun generateDummyData() {
        this.rawData = "200${debugChargeState()}00000000000000"
    }

    private fun debugChargeState(): String {
        // TODO some random logic here to change charge state from time to time
        return CHARGE_STATE_DC.toHexString().fixFormat()
    }

    private fun String.fixFormat() = if (this.length < 2) "0${this.uppercase()}" else this.uppercase()

    companion object {

        const val CHARGE_STATE_IDLE = 1
        const val CHARGE_STATE_AC = 2
        const val CHARGE_STATE_DC = 10
        const val CHARGE_STATE_DC_ALT = 16

    }

}
