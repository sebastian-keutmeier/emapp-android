package be.hcpl.android.energica.services.ble;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Handler;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import be.hcpl.android.energica.event.ChargeLimitEvent;
import be.hcpl.android.energica.event.ConnectionRejectedEvent;
import be.hcpl.android.energica.event.LocationEvent;
import be.hcpl.android.energica.event.NewStateEvent;
import be.hcpl.android.energica.helpers.BleWrapper;
import be.hcpl.android.energica.helpers.CommParser;
import be.hcpl.android.energica.helpers.Const;
import be.hcpl.android.energica.helpers.ExportData;
import be.hcpl.android.energica.helpers.Help;
import be.hcpl.android.energica.interfaces.BleWrapperUiCallback;
import be.hcpl.android.energica.interfaces.CommParserCallback;
import be.hcpl.android.energica.model.ble.BleDefinedUUIDs;
import be.hcpl.android.energica.model.ble.GpsData;
import be.hcpl.android.energica.model.ble.SecurityAccess;
import be.hcpl.android.energica.model.ble.VehicleStatus;
import be.hcpl.android.energica.services.evmonitor.EvMonitorRepo;

import static be.hcpl.android.energica.helpers.Const.MPS_TO_KPH;
import static be.hcpl.android.energica.helpers.Const.SETTINGS_DEVICE;
import static be.hcpl.android.energica.helpers.Const.STATE_NOT_CONNECTED;
import static be.hcpl.android.energica.helpers.Const.STATE_SEARCHING;

public class ConnectionManager implements BleWrapperUiCallback, CommParserCallback {

    private Context context;
    private BleWrapper bleWrapper;
    private final CommParser commParser;
    private String MAC;
    private final Runnable authRunnable = () -> {
        // informs bike about this device, this is some session that is created and maintained
        if (bleWrapper == null || bleWrapper.getDevice() == null) {
            return;
        }
        String address = bleWrapper.getDevice().getAddress();
        byte[] data = new byte[6];
        for (int i = 0; i < 17; i += 3) {
            data[i / 3] = (byte) ((Character.digit(address.charAt(i), 16) << 4) + Character.digit(address.charAt(i + 1), 16));
        }
        bleWrapper.writeDataToCharacteristic(
                ConnectionManager.this.writeBleGatt,
                new byte[]{4, 17, 0, -2, data[0], data[1], data[2], data[3], data[4], data[5]});
    };
    private VehicleStatus currentStatus = new VehicleStatus();
    private boolean deviceConnected = false;
    private boolean isScanning = false; // marks BLE scanning in progress
    private boolean isConnecting = false; // in some conditions with auto reconnect we were stuck
    private final Handler handler = new Handler();
    private boolean shouldReconnect = false; // only enable auto connects if user connected manually once
    private BluetoothGattCharacteristic notifyBleGatt = null;
    private BluetoothGattCharacteristic writeBleGatt = null;

    private final SharedPreferences prefs;
    private final EvMonitorRepo evMonitorRepo;

    // region singleton

    private static ConnectionManager instance;
    private EventCallback eventCallback; // temp solution to send info back to service

    private ConnectionManager(final Context context, final EventCallback eventCallback) {
        this.context = context;
        this.eventCallback = eventCallback;
        prefs = Help.getSharedPrefs(context);
        evMonitorRepo = new EvMonitorRepo(context, prefs);
        bleWrapper = new BleWrapper(context, this);
        commParser = new CommParser(bleWrapper, null, this);
        bleWrapper.initialize();
    }

    public static void init(final Context context, final EventCallback eventCallback) {
        if (instance == null) {
            instance = new ConnectionManager(context, eventCallback);
        }
    }

    public interface EventCallback {

        void post(Serializable event);

    }

    private void post(Serializable event){
        if( eventCallback != null ) eventCallback.post(event);
    }

    public static ConnectionManager getInstance() {
        return instance;
    }

    // endregion

    public VehicleStatus getCurrentStatus() {
        return currentStatus;
    }

    public boolean isBluetoothEnabled() {
        return bleWrapper.isBtEnabled();
    }

    public boolean isConnected() {
        return bleWrapper.isConnected() && deviceConnected && notifyBleGatt != null && writeBleGatt != null;
    }

    public boolean isScanning() {
        return isScanning;
    }

    public void onSeedReceived(Integer seed) {
        SecurityAccess seedAndKey = new SecurityAccess(seed);
        byte[] sendBuffer = {4, 17, 0, 0, seedAndKey.getKeyByte(0), seedAndKey.getKeyByte(1), seedAndKey.getKeyByte(2), seedAndKey.getKeyByte(
                3), 0, 0};
        if (writeBleGatt != null && bleWrapper.getAdapter() != null) {
            bleWrapper.writeDataToCharacteristic(writeBleGatt, sendBuffer);
            handler.postDelayed(authRunnable, 15); // keep confirming session
        }
    }

    public void onConnectionConfirmed() {
        prefs.edit().putString(SETTINGS_DEVICE, MAC).apply(); // only once confirmed allow connect without scanning
        deviceConnected = true;
    }

    public void onVehicleStatusReceived(VehicleStatus status) {
        currentStatus = status;
        if( evMonitorRepo != null ){
            evMonitorRepo.pushEnergicaBleData(status);
        }
    }

    public void onChgPwrLimitReceived(Integer limit) {
        post(new ChargeLimitEvent(limit));
    }

    public void onParseError(byte[] record) {
        // ignored since too noisy EventBus.getDefault().post(new LogEvent("data parsing error");
    }

    public void onConnectionRejected() {
        post(new ConnectionRejectedEvent());
    }

    public void onSpeedRPMTorqueReceived(int speed, int rpm, int torque, float power) {
        // ignored
    }

    public void onGPSDataReceived(GpsData gpsData) {
        double latitude = gpsData.convertLat();
        double longitude = gpsData.convertLon();
        Location location = new Location("bike"); // use provider to select gps or bike speeds only
        location.setLatitude(latitude);
        location.setLongitude(longitude);
        location.setSpeed(gpsData.speed/MPS_TO_KPH); // this speed is received in kph (or mph) so convert it to m/s here
        location.setTime(System.currentTimeMillis());
        post(new LocationEvent(location));
    }

    public void onNearbyChargePointsRequested() {
        // ignore
    }

    public void onChargePointsRequested() {
        // ignore
    }

    public void uiDeviceConnected(BluetoothGatt gatt, BluetoothDevice device) {
        if (isScanning) {
            isScanning = false;
            bleWrapper.stopScanning();
        }
        deviceConnected = true;
        isConnecting = false;
        MAC = device.getAddress(); // recovers connected device
        triggerUpdateStateEvent();
        bleWrapper.startServicesDiscovery();
    }

    public void triggerUpdateStateEvent() {
        if (isScanning()) {
            post(new NewStateEvent(STATE_SEARCHING));
        } else if (isConnected()) {
            // no fixed state indication here cause we'll receive one from the bike
            post(new NewStateEvent(currentStatus.state));
        } else {
            post(new NewStateEvent(STATE_NOT_CONNECTED));
        }
    }

    public void uiDeviceDisconnected(BluetoothGatt gatt, BluetoothDevice device) {
        if (shouldReconnect) {
            // auto reconnect while services are running, just to prevent unexpected disconnects
            // but allow this only once
            handleFoundDevice(device.getAddress());
            return;
        }
        MAC = null; // remove this so we can reconnect to it
        isScanning = false;
        deviceConnected = false;
        notifyBleGatt = null;
        writeBleGatt = null;
        bleWrapper.close();
        triggerUpdateStateEvent();
    }

    public void uiAvailableServices(
            BluetoothGatt gatt,
            BluetoothDevice device,
            List<BluetoothGattService> services) {

        if (!device.getAddress().contains(MAC)) {
            log("Error, wrong device returned from ServiceDiscovery");
            return;
        }
        boolean serviceFound = false;
        Iterator<BluetoothGattService> it = services.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            BluetoothGattService service = it.next();
            if (UUID.fromString(Const.ENERGICA_SERVICE_UUID).equals(service.getUuid())) {
                log("EnergicaServiceUUID found");
                bleWrapper.getCharacteristicsForService(service);
                serviceFound = true;
                MAC = device.getAddress(); // not really needed but can work as a confirmation
                break;
            }
        }
        if (!serviceFound) {
            log("Error, Device doesn't support Energica Services");
        }
    }

    public void uiCharacteristicForService(
            BluetoothGatt gatt,
            BluetoothDevice device,
            BluetoothGattService service,
            List<BluetoothGattCharacteristic> chars) {

        if (!device.getAddress().contains(MAC)) {
            log("Error, wrong device returned from ServiceDiscovery");
            return;
        }
        for (BluetoothGattCharacteristic characteristic : chars) {
            if (characteristic.getUuid().compareTo(BleDefinedUUIDs.Characteristic.ENERGICA_WRITE) == 0) {
                log("ENERGICA_WRITE found");
                writeBleGatt = characteristic;
                commParser.setWriteCharacteristic(writeBleGatt);
            }
            if (characteristic.getUuid().compareTo(BleDefinedUUIDs.Characteristic.ENERGICA_NOTIFY) == 0) {
                log("ENERGICA_NOTIFY found");
                notifyBleGatt = characteristic;
                bleWrapper.setNotificationForCharacteristic(notifyBleGatt, true);
            }
        }
        if (notifyBleGatt != null && writeBleGatt != null) {
            deviceConnected = true;
        }
    }

    private void log(final String message) {
        ExportData.Companion.log(context, message);
    }

    public void uiNewValueForCharacteristic(BluetoothGattCharacteristic characteristic, byte[] rawValue) {
        if (characteristic != null && notifyBleGatt != null && notifyBleGatt.getUuid() != null && notifyBleGatt.getUuid().equals(characteristic.getUuid())) {
            commParser.parseBuffer(rawValue);
        }
    }

    public void uiNewRssiAvailable(
            BluetoothGatt gatt,
            BluetoothDevice device,
            int rssi) {

        // ignore not posting new RSSI values since too noisy
    }

    public void uiDeviceFound(
            BluetoothDevice device,
            int rssi,
            byte[] record) {

        handleFoundDevice(device.getAddress());
    }

    public void uiSuccessfulWrite(
            BluetoothGatt gatt,
            BluetoothDevice device,
            BluetoothGattService service,
            BluetoothGattCharacteristic ch) {

        // ignored
    }

    public void uiFailedWrite(
            BluetoothGatt gatt,
            BluetoothDevice device,
            BluetoothGattService service,
            BluetoothGattCharacteristic ch) {

        // ignored
    }

    public void handleFoundDevice(final String deviceAddress) {
        String pairedDevice = prefs.getString(SETTINGS_DEVICE, null);
        if( MAC != null && MAC.equals(deviceAddress) && isConnecting && !isConnected()){
            // already connecting or connected to that MAC so skip here
            return;
        }
        if (pairedDevice == null || pairedDevice.equals(deviceAddress)) {
            isConnecting = true;
            MAC = deviceAddress; // mark the MAC address already connecting to prevent multiple connects to same
            bleWrapper.connect(deviceAddress);
        }
    }

    public void setMAC(final String macValue) {
        MAC = macValue;
    }

    public void setShouldReconnect(final boolean shouldReconnect) {
        this.shouldReconnect = shouldReconnect;
    }

    public boolean getShouldReconnect() {
        return this.shouldReconnect;
    }

    public void stopScanning() {
        bleWrapper.stopScanning();
        isScanning = false;
    }

    public void disconnect() {
        bleWrapper.disconnect();
        bleWrapper.close();
    }

    public void startScanning() {
        isScanning = true;
        bleWrapper.startScanning();
    }

    public void vehicleInfoRequest() {
        commParser.vehicleInfoRequest();
    }

    public void odometerInfoRequest() {
        commParser.odometerInfoRequest();
    }

    public void setChargeTermination() {
        commParser.setChgTermination();
    }

    public void setResetTrip() {
        commParser.setResetTrip();
    }

}
