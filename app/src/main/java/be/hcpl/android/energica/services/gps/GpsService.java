package be.hcpl.android.energica.services.gps;

import static be.hcpl.android.energica.helpers.Const.BROADCAST_EVENT;
import static be.hcpl.android.energica.helpers.Const.BROADCAST_FILTER;
import static be.hcpl.android.energica.helpers.Const.BROADCAST_GPS_STATE;
import static be.hcpl.android.energica.helpers.Const.BROADCAST_LOCATION;
import static be.hcpl.android.energica.helpers.Const.BROADCAST_MESSAGE;
import static be.hcpl.android.energica.helpers.Const.GPS_STATE;
import static be.hcpl.android.energica.helpers.Const.LOCATION_LAT;
import static be.hcpl.android.energica.helpers.Const.LOCATION_LON;
import static be.hcpl.android.energica.helpers.Const.LOCATION_PRECISION;
import static be.hcpl.android.energica.helpers.Const.LOCATION_SPEED;
import static be.hcpl.android.energica.helpers.Const.MESSAGE;
import static be.hcpl.android.energica.helpers.Const.STATE_GPS_STOPPED;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import java.util.Date;

import be.hcpl.android.energica.MainActivity;
import be.hcpl.android.energica.R;
import be.hcpl.android.energica.helpers.ExportData;
import be.hcpl.android.energica.helpers.Help;
import be.hcpl.android.energica.services.evmonitor.EvMonitorRepo;

@SuppressLint("MissingPermission") // permission are in manifest
public class GpsService extends Service {

    // note that caller of this service is responsible for setting up the permission requests

    public static final int NOTIFICATION_ID = 987;
    public static final String NOTIFICATION_CHANNEL_NAME = "GPS Tracking";
    public static final String NOTIFICATION_CHANNEL_ID = "gps-channel";
    public static final String NOTIFICATION_TEXT = "GPS Tracking";
    private LocationManager locationManager = null;
    private static final int LOCATION_INTERVAL = 1_000; // ms in between updates
    private static final float LOCATION_DISTANCE = 5; // meters in between updates

    private SharedPreferences prefs;
    private boolean loggingToFile = false;
    private String log2fileName;
    private EvMonitorRepo evMonitorRepo;

    private LocationListener locationListener;

    private class LocationListener implements android.location.LocationListener {

        private final Location lastLocation;

        public LocationListener(String provider) {
            log("GPS - LocationListener created with provider: " + provider);
            lastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location) {
            //log("GPS - onLocationChanged: ");// + location); // generates way too much logging
            lastLocation.set(location);
            pushLocation(location);
            sendStateUpdate(STATE_GPS_STOPPED);
        }

        @Override
        public void onProviderDisabled(String provider) {
            log("GPS - onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider) {
            log("GPS - onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            // no need to keep logging this, too much data
            //log("GPS - onStatusChanged: " + provider);
        }
    }
    
    private void log(final String message){
        //sendMessage(message); // sends message for use elsewhere
        ExportData.Companion.log(getApplicationContext(), message); // logs to system file for in app use
    }

    private void sendStateUpdate(final int state){
        final Bundle bundle = new Bundle();
        bundle.putInt(GPS_STATE, state);
        sendBroadcast(BROADCAST_GPS_STATE, bundle);
    }

    private void pushLocation(Location location){
        // collection and then update visually when the app is resumed
        sendLocation(location);
        // optional cloud data sharing here
        evMonitorRepo.pushLocation(location.getLatitude(), location.getLongitude(), location.getAccuracy(), location.getSpeed());
        // on each iteration also check if the logging condition has changed so we can create a new log file if needed
        boolean shouldLog = prefs.getBoolean(getString(R.string.key_log_to_file), true);
        if( shouldLog && !loggingToFile ){
            // create log file
            createNewLogFile();
            loggingToFile = true;
        } else if( !shouldLog && loggingToFile ){
            // finish logging to file properly
            finishLogFile();
            // stop logging
            loggingToFile = false;
        }
        // also update log GPX route directly to file system if option is enabled
        if( loggingToFile ){
            // append new location to log file here
            appendToLogFile(location);
        }
    }

    private void createNewLogFile() {
        log2fileName = "route-" + ExportData.Companion.getTimeFormatLogStatement().format(System.currentTimeMillis()) + ".gpx";
        StringBuilder segments = new StringBuilder("<trk><name>" + log2fileName + "</name>");
        segments.append("<trkseg>\n");
        ExportData.Companion.writeToNewFile(log2fileName, ExportData.header + segments.toString(), getApplicationContext());
        log("GPS - created new file for logging from background service: "+log2fileName);
    }

    private void appendToLogFile(final Location location){
        StringBuilder segments = new StringBuilder();
        segments.append("<trkpt lat=\"")
                .append(location.getLatitude()).append("\" lon=\"").append(location.getLongitude())
                .append("\"><time>").append(ExportData.Companion.getDateformatGpx().format(
                    new Date(location.getTime()))).append("</time></trkpt>\n");
        ExportData.Companion.appendToFile(log2fileName, segments.toString(), getApplicationContext());
    }

    private void finishLogFile() {
        StringBuilder segments = new StringBuilder();
        segments.append("</trkseg></trk></gpx>");
        segments.append("</gpx>");
        ExportData.Companion.appendToFile(log2fileName, segments.toString(), getApplicationContext());
    }

    private void sendLocation(Location location){
        if( location == null ) return;
        final Bundle bundle = new Bundle();
        bundle.putDouble(LOCATION_LAT, location.getLatitude());
        bundle.putDouble(LOCATION_LON, location.getLongitude());
        bundle.putFloat(LOCATION_SPEED, location.getSpeed());
        bundle.putFloat(LOCATION_PRECISION, location.getAccuracy());
        sendBroadcast(BROADCAST_LOCATION, bundle);
    }

    private void sendMessage(String message) {
        if (message == null) return;
        final Bundle bundle = new Bundle();
        bundle.putString(MESSAGE, message);
        sendBroadcast(BROADCAST_MESSAGE, bundle);
    }

    private void sendBroadcast(String event, Bundle bundle) {
        Intent intent = new Intent(BROADCAST_FILTER);
        intent.putExtra(BROADCAST_EVENT, event);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        sendBroadcast(intent);
    }

    // region keep service running in background mode

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        log("GPS - onStartCommand");
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private Notification getNotification() {
        final PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, new Intent(getApplicationContext(), MainActivity.class), 0);
        NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, NOTIFICATION_CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
        NotificationManager notificationManager = getSystemService(NotificationManager.class);
        notificationManager.createNotificationChannel(channel);
        Notification.Builder builder = new Notification.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID);
        builder.setSmallIcon(R.drawable.ic_action_gps_fixed);
        builder.setContentText(NOTIFICATION_TEXT);
        builder.setContentIntent(contentIntent);
        return builder.build();
    }

    private Notification getCompatNotification() {
        final PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, new Intent(getApplicationContext(), MainActivity.class), 0);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());
        builder.setSmallIcon(R.drawable.ic_action_gps_fixed)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(NOTIFICATION_TEXT)
                .setContentIntent(contentIntent)
                .setAutoCancel(false)
                .setOngoing(true);
        return builder.build();
    }

    // endregion

    @Override
    public void onCreate() {
        log("GPS - onCreate");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForeground(NOTIFICATION_ID, getNotification());
            log("GPS - started as foreground service for Android 8 & up");
        } else {
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.notify(NOTIFICATION_ID, getCompatNotification());
            log("GPS -  showing compat notification for running service");
        }
        prefs = Help.getSharedPrefs(getApplicationContext());
        evMonitorRepo = new EvMonitorRepo(getApplicationContext(), prefs);
        initializeLocationManager();
        try {
            // fetch precision location using GPS provider
            locationListener = new LocationListener(LocationManager.GPS_PROVIDER);
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    locationListener);
            // try getting last known location to begin with
            Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            if( lastKnownLocation != null ){
                pushLocation(lastKnownLocation);
            }
        } catch (SecurityException ex) {
            log("GPS - fail to request location update, ignore: " + ex.getMessage());
        } catch (IllegalArgumentException ex) {
            log("GPS - gps provider does not exist: " + ex.getMessage());
        }
    }

    @Override
    public void onDestroy() {
        log("GPS - onDestroy");
        // flush all collected data to cloud
        evMonitorRepo.flushData();
        // stop gps updates
        if (locationManager != null && locationListener != null) {
            try {
                locationManager.removeUpdates(locationListener);
            } catch (Exception ex) {
                log("GPS - fail to remove location listeners, ignore: " + ex.getMessage());
            }
        }
        // remove notification
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            stopForeground(true);
        } else {
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.cancel(NOTIFICATION_ID);
        }
        // state update for whatever view is present atm
        sendStateUpdate(STATE_GPS_STOPPED);
        super.onDestroy();
    }

    private void initializeLocationManager() {
        log("GPS - initializeLocationManager");
        if (locationManager == null) {
            locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }

}
