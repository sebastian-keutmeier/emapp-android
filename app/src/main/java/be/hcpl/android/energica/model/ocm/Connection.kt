package be.hcpl.android.energica.model.ocm

import androidx.annotation.Keep

@Keep
class Connection(
    val ID: Int = 0,
    val ConnectionTypeID: Int = 0,
    val StatusTypeID: Int = 0,
    val PowerKW: String?
) {

    companion object {
        const val CONNECTOR_ID_EU_AC = 25
        const val CONNECTOR_ID_EU_DC = 33
        const val CONNECTOR_ID_US_AC = 1
        const val CONNECTOR_ID_US_DC = 32
    }
}
