package be.hcpl.android.energica.model.ble;

public class SecurityAccess {

    private Integer key;
    public final Integer mask_a = -1431655766;
    public final Integer mask_b = 1431655765;
    public final Integer offset = -1046431042;
    private Integer seed;

    public SecurityAccess(Integer seed2) {
        this.seed = seed2;
        this.key = (((this.seed & this.mask_a) >> 1) | ((this.seed & this.mask_b) << 1)) + this.offset;
    }

    public void setSeed(Integer seed2) {
        this.seed = seed2;
        this.key = (((this.seed & this.mask_a) >> 1) | ((this.seed & this.mask_b) << 1)) + this.offset;
    }

    public Integer getKey() {
        return this.key;
    }

    public byte getKeyByte(int byteNumber) {
        switch (byteNumber) {
            case 0:
                return Integer.valueOf(this.key & 255).byteValue();
            case 1:
                return Integer.valueOf((this.key >> 8) & 255).byteValue();
            case 2:
                return Integer.valueOf((this.key >> 16) & 255).byteValue();
            case 3:
                return Integer.valueOf((this.key >> 24) & 255).byteValue();
            default:
                return 0;
        }
    }
}
