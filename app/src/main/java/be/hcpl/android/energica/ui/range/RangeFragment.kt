package be.hcpl.android.energica.ui.range

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import android.widget.TextView
import androidx.appcompat.widget.AppCompatSeekBar
import androidx.core.content.ContextCompat.getColor
import androidx.core.view.children
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import be.hcpl.android.energica.R


class RangeFragment : Fragment() {

    private lateinit var viewModel: RangeViewModel

    private lateinit var selectedCapacity: TextView
    private lateinit var calculatedRange: TextView
    private lateinit var sliderSoc: AppCompatSeekBar
    private lateinit var selectedSoc: TextView
    private var capacityViews: List<View>? = null
    private var roadViews: List<View>? = null
    private var ridingViews: List<View>? = null
    private var landscapeViews: List<View>? = null
    private var weatherViews: List<View>? = null
    private lateinit var chargeTimeView: TextView
    private var chargerViews: List<View>? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewModel = ViewModelProvider(this).get(RangeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_range, container, false)

        findAllViews(root)

        // receive updates
        viewModel.selectedBattery.observe(viewLifecycleOwner, Observer {
            selectedCapacity.text = getString(R.string.selected_battery_is, it, viewModel.nominalForCapacity())
        })
        viewModel.calculatedRange.observe(viewLifecycleOwner, Observer {
            calculatedRange.text = getString(R.string.range_value, it, it * 0.621371192)
        })
        viewModel.calculatedRange.observe(viewLifecycleOwner, Observer {
            calculatedRange.text = getString(R.string.range_value, it, it * 0.621371192)
        })
        viewModel.selectedSoc.observe(viewLifecycleOwner, Observer {
            selectedSoc.text = getString(R.string.label_percentage, it)
            chargeTimeView.text = getString(R.string.estimated_charge_time,
                viewModel.chargeTimeInHours(),
                viewModel.chargeTimeInMinutes())
        })
        viewModel.selectedCharger.observe(viewLifecycleOwner, Observer {
            chargeTimeView.text = getString(R.string.estimated_charge_time,
                viewModel.chargeTimeInHours(),
                viewModel.chargeTimeInMinutes())
        })

        // perform updates
        sliderSoc.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                // ignore
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                // ignore
            }

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                viewModel.selectedSoc.setValue(progress.toDouble())
                viewModel.calculateRange()
            }
        })
        capacityViews?.forEach {
            it.setOnClickListener { view ->
                viewModel.selectedBattery.setValue((view as TextView).text.toString())
                viewModel.calculateRange()
                context?.let { ctx ->
                    capacityViews?.forEach {
                        (it as TextView).setTextColor(
                            getColor(
                                ctx,
                                R.color.white
                            )
                        )
                    }
                    view.setTextColor(getColor(ctx, R.color.colorAccent))
                }
            }
        }
        roadViews?.forEach {
            it.setOnClickListener { view ->
                viewModel.selectedRoad.setValue((view as TextView).text.toString())
                viewModel.calculateRange()
                context?.let { ctx ->
                    roadViews?.forEach {
                        (it as TextView).setTextColor(
                            getColor(
                                ctx,
                                R.color.white
                            )
                        )
                    }
                    view.setTextColor(getColor(ctx, R.color.colorAccent))
                }
            }
        }
        ridingViews?.forEach {
            it.setOnClickListener { view ->
                viewModel.selectedRiding.setValue((view as TextView).text.toString())
                viewModel.calculateRange()
                context?.let { ctx ->
                    ridingViews?.forEach {
                        (it as TextView).setTextColor(
                            getColor(
                                ctx,
                                R.color.white
                            )
                        )
                    }
                    view.setTextColor(getColor(ctx, R.color.colorAccent))
                }
            }
        }
        landscapeViews?.forEach {
            it.setOnClickListener { view ->
                viewModel.selectedLandscape.setValue((view as TextView).text.toString())
                viewModel.calculateRange()
                context?.let { ctx ->
                    landscapeViews?.forEach {
                        (it as TextView).setTextColor(
                            getColor(
                                ctx,
                                R.color.white
                            )
                        )
                    }
                    view.setTextColor(getColor(ctx, R.color.colorAccent))
                }
            }
        }
        weatherViews?.forEach {
            it.setOnClickListener { view ->
                viewModel.selectedWeather.setValue((view as TextView).text.toString())
                viewModel.calculateRange()
                context?.let { ctx ->
                    weatherViews?.forEach {
                        (it as TextView).setTextColor(
                            getColor(
                                ctx,
                                R.color.white
                            )
                        )
                    }
                    view.setTextColor(getColor(ctx, R.color.colorAccent))
                }
            }
        }
        chargerViews?.forEach {
            it.setOnClickListener { view ->
                viewModel.selectedCharger.setValue(Integer.parseInt((view as TextView).text.toString()))
                context?.let { ctx ->
                    chargerViews?.forEach {
                        (it as TextView).setTextColor(
                            getColor(
                                ctx,
                                R.color.white
                            )
                        )
                    }
                    view.setTextColor(getColor(ctx, R.color.colorAccent))
                }
            }
        }

        // init form
        viewModel.calculateRange()
        viewModel.selectedCharger.postValue(3) // defaults to 3 kW charger

        return root
    }

    private fun findAllViews(root: View) {
        selectedCapacity = root.findViewById(R.id.selected_battery)
        calculatedRange = root.findViewById(R.id.estimated_range_value)
        sliderSoc = root.findViewById(R.id.soc_slider)
        selectedSoc = root.findViewById(R.id.selected_soc)
        capacityViews = (root.findViewById(R.id.capacities) as? ViewGroup)?.children?.toList()
        roadViews = (root.findViewById(R.id.roads) as? ViewGroup)?.children?.toList()
        ridingViews = (root.findViewById(R.id.ridingstyles) as? ViewGroup)?.children?.toList()
        landscapeViews = (root.findViewById(R.id.landscapes) as? ViewGroup)?.children?.toList()
        weatherViews = (root.findViewById(R.id.weather) as? ViewGroup)?.children?.toList()
        chargeTimeView = root.findViewById(R.id.estimated_charge_time)
        chargerViews = (root.findViewById(R.id.chargers) as? ViewGroup)?.children?.toList()
    }

}
