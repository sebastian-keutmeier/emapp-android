package be.hcpl.android.energica

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import be.hcpl.android.energica.fragments.SettingsFragment
import be.hcpl.android.energica.helpers.Help
import be.hcpl.android.energica.helpers.Help.withColor
import be.hcpl.android.energica.services.ble.BleService
import be.hcpl.android.energica.services.gps.GpsService
import be.hcpl.android.energica.services.obd2.Obd2Service

class SettingsActivity : AppCompatActivity(R.layout.activity_settings) {

    // toolbar state icons
    private lateinit var gpsStateView: ImageView
    private lateinit var bleStateView: ImageView
    private lateinit var obdStateView: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.settings_container, SettingsFragment())
            .commit()

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        val titleView = findViewById<TextView>(R.id.toolbar_title)
        titleView.text = getString(R.string.menu_settings)

        // state indications
        gpsStateView = findViewById(R.id.gps_state)
        gpsStateView.visibility = View.VISIBLE
        gpsStateView.setColorFilter(withColor(applicationContext, R.color.dark_white))
        bleStateView = findViewById(R.id.ble_state)
        bleStateView.visibility = View.VISIBLE
        bleStateView.setColorFilter(withColor(applicationContext, R.color.dark_white))
        obdStateView = findViewById(R.id.obd_state)
        obdStateView.visibility = View.VISIBLE
        obdStateView.setColorFilter(withColor(applicationContext, R.color.dark_white))
    }

    override fun onResume() {
        super.onResume()
        // update state of services
        updateServiceStates()
    }

    private fun updateServiceStates() {
        val gpsServiceRunning = Help.isServiceRunning(applicationContext, GpsService::class.java)
        val bleServiceRunning = Help.isServiceRunning(applicationContext, BleService::class.java)
        val obd2ServiceRunning = Help.isServiceRunning(applicationContext, Obd2Service::class.java)
        gpsStateView.setColorFilter(Help.withStateColor(applicationContext, gpsServiceRunning))
        bleStateView.setColorFilter(Help.withStateColor(applicationContext, bleServiceRunning))
        obdStateView.setColorFilter(Help.withStateColor(applicationContext, obd2ServiceRunning))
    }

    // region menu

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.close_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.close -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    // endregion

}