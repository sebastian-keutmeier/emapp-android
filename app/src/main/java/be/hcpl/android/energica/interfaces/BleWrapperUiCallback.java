package be.hcpl.android.energica.interfaces;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import java.util.List;

public interface BleWrapperUiCallback {
    void uiAvailableServices(BluetoothGatt bluetoothGatt, BluetoothDevice bluetoothDevice, List<BluetoothGattService> list);

    void uiCharacteristicForService(BluetoothGatt bluetoothGatt, BluetoothDevice bluetoothDevice, BluetoothGattService bluetoothGattService, List<BluetoothGattCharacteristic> list);

    void uiDeviceConnected(BluetoothGatt bluetoothGatt, BluetoothDevice bluetoothDevice);

    void uiDeviceDisconnected(BluetoothGatt bluetoothGatt, BluetoothDevice bluetoothDevice);

    void uiDeviceFound(BluetoothDevice bluetoothDevice, int i, byte[] bArr);

    void uiFailedWrite(BluetoothGatt bluetoothGatt, BluetoothDevice bluetoothDevice, BluetoothGattService bluetoothGattService, BluetoothGattCharacteristic bluetoothGattCharacteristic);

    void uiNewRssiAvailable(BluetoothGatt bluetoothGatt, BluetoothDevice bluetoothDevice, int i);

    void uiNewValueForCharacteristic(BluetoothGattCharacteristic bluetoothGattCharacteristic, byte[] rawValue);

    void uiSuccessfulWrite(BluetoothGatt bluetoothGatt, BluetoothDevice bluetoothDevice, BluetoothGattService bluetoothGattService, BluetoothGattCharacteristic bluetoothGattCharacteristic);

    class Null implements BleWrapperUiCallback {
        public void uiDeviceConnected(BluetoothGatt gatt, BluetoothDevice device) {
        }

        public void uiDeviceDisconnected(BluetoothGatt gatt, BluetoothDevice device) {
        }

        public void uiAvailableServices(BluetoothGatt gatt, BluetoothDevice device, List<BluetoothGattService> list) {
        }

        public void uiCharacteristicForService(BluetoothGatt gatt, BluetoothDevice device, BluetoothGattService service, List<BluetoothGattCharacteristic> list) {
        }

        public void uiNewValueForCharacteristic(BluetoothGattCharacteristic characteristic, byte[] rawValue) {
        }

        public void uiSuccessfulWrite(BluetoothGatt gatt, BluetoothDevice device, BluetoothGattService service, BluetoothGattCharacteristic ch) {
        }

        public void uiFailedWrite(BluetoothGatt gatt, BluetoothDevice device, BluetoothGattService service, BluetoothGattCharacteristic ch) {
        }

        public void uiNewRssiAvailable(BluetoothGatt gatt, BluetoothDevice device, int rssi) {
        }

        public void uiDeviceFound(BluetoothDevice device, int rssi, byte[] record) {
        }
    }
}
