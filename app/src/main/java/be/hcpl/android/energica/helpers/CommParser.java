package be.hcpl.android.energica.helpers;

import android.bluetooth.BluetoothGattCharacteristic;

import be.hcpl.android.energica.interfaces.CommParserCallback;
import be.hcpl.android.energica.model.ble.GpsData;
import be.hcpl.android.energica.model.ble.VehicleStatus;

public class CommParser {

    public static final int ADDRESSMATCHATTEMPTS = 5;
    public static final int NO_CHARACTERISTICS = -1;
    public static final int OK = 1;
    public static final int OUT_OF_BOUNDS = -2;

    private final GpsData gpsData;
    private static int matchAttempt = ADDRESSMATCHATTEMPTS;
    private static boolean deviceRecognized = false;
    private static long lastCPOIUpdate;
    private static int dtcIdx = 0;
    private static int vehicleStatusIdx = 0;
    private static int vehicleStatusIdx2 = 0;
    private static BluetoothGattCharacteristic writeBleGatt;
    private static boolean usrKeySts = false;
    private final BleWrapper bleWrapper;
    private final CommParserCallback parserCallback;
    private final VehicleStatus vehicleStatus;

    public CommParser(BleWrapper CommWrapper, BluetoothGattCharacteristic characteristic, CommParserCallback callback) {
        parserCallback = callback == null ? new CommParserCallback.Null() : callback;
        vehicleStatusIdx = 0;
        bleWrapper = CommWrapper;
        writeBleGatt = characteristic;
        vehicleStatus = new VehicleStatus();
        gpsData = new GpsData();
    }

    public GpsData getGpsData() {
        return gpsData;
    }

    public void setWriteCharacteristic(BluetoothGattCharacteristic writeChar) {
        writeBleGatt = writeChar;
    }

    /**
     * send BLE request for vehicle info
     */
    public void vehicleInfoRequest() {
        byte[] txBuffer = {4, 17, 2, -1, 0, 0, 0, 0, 0, 0};
        if (bleWrapper != null && bleWrapper.isConnected() && writeBleGatt != null) {
            bleWrapper.writeDataToCharacteristic(writeBleGatt, txBuffer);
        }
    }

    /**
     * BLE request for odometer info
     */
    public void odometerInfoRequest() {
        byte[] txBuffer = {4, 17, 4, -1, 0, 0, 0, 0, 0, 0};
        if (bleWrapper != null && bleWrapper.isConnected() && writeBleGatt != null) {
            bleWrapper.writeDataToCharacteristic(writeBleGatt, txBuffer);
        }
    }

    public int setChargePowerLimit(byte power) {
        if (power < 0 || power > 100) {
            return OUT_OF_BOUNDS;
        }
        byte[] txBuffer = {4, 17, 20, -1, power, 0, 0, 0, 0, 0};
        if (bleWrapper == null || !bleWrapper.isConnected() || writeBleGatt == null) {
            return NO_CHARACTERISTICS;
        }
        bleWrapper.writeDataToCharacteristic(writeBleGatt, txBuffer);
        return OK;
    }

    public int getChargePowerLimit() {
        byte[] txBuffer = {4, 17, 21, -1, 0, 0, 0, 0, 0, 0};
        if (bleWrapper == null || !bleWrapper.isConnected() || writeBleGatt == null) {
            return NO_CHARACTERISTICS;
        }
        bleWrapper.writeDataToCharacteristic(writeBleGatt, txBuffer);
        return OK;
    }

    public int setChgTermination() {
        byte[] txBuffer = {4, 17, 22, -1, 0, 0, 0, 0, 0, 0};
        if (bleWrapper == null || !bleWrapper.isConnected() || writeBleGatt == null) {
            return NO_CHARACTERISTICS;
        }
        bleWrapper.writeDataToCharacteristic(writeBleGatt, txBuffer);
        return OK;
    }

    public int setSpeedLimit(int spd) {
        if (spd < 20 || spd > 255) {
            return OUT_OF_BOUNDS;
        }
        byte[] txBuffer = {4, 17, 23, -1, (byte) spd, 0, 0, 0, 0, 0};
        if (bleWrapper == null || !bleWrapper.isConnected() || writeBleGatt == null) {
            return NO_CHARACTERISTICS;
        }
        bleWrapper.writeDataToCharacteristic(writeBleGatt, txBuffer);
        return OK;
    }

    public int setHornPulse() {
        byte[] txBuffer = {4, 17, 24, -1, 1, 0, 0, 0, 0, 0};
        if (bleWrapper == null || !bleWrapper.isConnected() || writeBleGatt == null) {
            return NO_CHARACTERISTICS;
        }
        bleWrapper.writeDataToCharacteristic(writeBleGatt, txBuffer);
        return OK;
    }

    /**
     * BLE request to retrieve diagnostics codes
     */
    public int getDTCList() {
        byte[] txBuffer = {4, 17, 25, -1, 0, 0, 0, 0, 0, 0};
        if (bleWrapper == null || !bleWrapper.isConnected() || writeBleGatt == null) {
            return NO_CHARACTERISTICS;
        }
        bleWrapper.writeDataToCharacteristic(writeBleGatt, txBuffer);
        return OK;
    }

    public int sendFoundStationsNumber(int stationsNumber) {
        byte[] txBuffer = {4, 17, 27, -1, (byte) stationsNumber, 0, 0, 0, 0, 0};
        if (bleWrapper == null || !bleWrapper.isConnected() || writeBleGatt == null) {
            return NO_CHARACTERISTICS;
        }
        bleWrapper.writeDataToCharacteristic(writeBleGatt, txBuffer);
        return OK;
    }

    public int setResetTrip() {
        byte[] txBuffer = {4, 17, 29, -1, 1, 0, 0, 0, 0, 0};
        if (bleWrapper == null || !bleWrapper.isConnected() || writeBleGatt == null) {
            return NO_CHARACTERISTICS;
        }
        bleWrapper.writeDataToCharacteristic(writeBleGatt, txBuffer);
        return OK;
    }

    public void parseBuffer(byte[] rx_buffer) {
        switch (rx_buffer[0]) {
            case 0:
                if (rx_buffer[1] == -1) {
                    parserCallback.onSeedReceived((rx_buffer[5] << 24) & Const.MEASURED_STATE_MASK | ((rx_buffer[4] << 16) & 16711680) | ((rx_buffer[3] << 8) & Const.ACTION_POINTER_INDEX_MASK) | (rx_buffer[2] & 255));
                    return;
                } else {
                    parserCallback.onParseError(rx_buffer);
                    return;
                }
            case 1:
                if (rx_buffer[2] != 0) {
                    deviceRecognized = true;
                    matchAttempt = ADDRESSMATCHATTEMPTS;
                    parserCallback.onConnectionConfirmed();
                } else {
                    deviceRecognized = false;
                    matchAttempt--;
                    if (matchAttempt <= 0) {
                        parserCallback.onConnectionRejected();
                    }
                }
                usrKeySts = rx_buffer[6] != 0;
                vehicleStatus.model = rx_buffer[4];
                //switch (vehicleStatus.model) {
                    // TODO add support for other models also... I'm getting model No 5 with SS9+
                    //case 1: Const.MODEL_EGO);break;
                    //case 2: Const.MODEL_EVA);break;
                //}
                return;
            case 2:
                if (rx_buffer[1] == vehicleStatusIdx) {
                    switch (rx_buffer[1]) {
                        case -2:
                            vehicleStatus.cMAINSV = (short) ((rx_buffer[2] & 255) | ((rx_buffer[3] << 8) & 65280));
                            vehicleStatus.resBatteryEnergy = (short) ((rx_buffer[4] & 255) | ((rx_buffer[5] << 8) & 65280));
                            vehicleStatusIdx = 0;
                            parserCallback.onVehicleStatusReceived(vehicleStatus);
                            return;
                        case 0:
                            vehicleStatus.soc = rx_buffer[2];
                            vehicleStatus.state = rx_buffer[3];
                            vehicleStatus.subState = rx_buffer[4];
                            vehicleStatus.range = (rx_buffer[5] & 255) | ((rx_buffer[6] << 8) & 65280);
                            vehicleStatusIdx++;
                            // added a partial update here
                            parserCallback.onVehicleStatusReceived(vehicleStatus);
                            return;
                        case 1:
                            vehicleStatus.avgConsumption = ((float) ((((rx_buffer[2] & 255) | ((rx_buffer[3] << 8) & 65280)) | ((rx_buffer[4] << 16) & 16711680)) | ((rx_buffer[5] << 24) & -16777216))) / 10.0f;
                            vehicleStatus.instKmKwh = ((float) ((rx_buffer[6] & 255) | ((rx_buffer[7] << 8) & 65280))) / 100.0f;
                            vehicleStatusIdx++;
                            // added a partial update here
                            parserCallback.onVehicleStatusReceived(vehicleStatus);
                            return;
                        case 2:
                            vehicleStatus.instKwh100Km = ((float) ((rx_buffer[2] & 255) | ((rx_buffer[3] << 8) & 65280))) / 100.0f;
                            vehicleStatus.bPackV = ((float) ((rx_buffer[4] & 255) | ((rx_buffer[5] << 8) & 65280))) / 10.0f;
                            vehicleStatus.bPackI = ((float) ((short) ((rx_buffer[6] & 255) | ((rx_buffer[7] << 8) & 65280)))) / 10.0f;
                            vehicleStatusIdx++;
                            // added a partial update here
                            parserCallback.onVehicleStatusReceived(vehicleStatus);
                            return;
                        case 3:
                            vehicleStatus.cDCC = ((float) ((short) ((rx_buffer[2] & 255) | ((rx_buffer[3] << 8) & 65280)))) / 100.0f;
                            vehicleStatus.cDCV = ((float) ((short) ((rx_buffer[4] & 255) | ((rx_buffer[5] << 8) & 65280)))) / 10.0f;
                            vehicleStatus.cMAINSC = ((float) ((short) ((rx_buffer[6] & 255) | ((rx_buffer[7] << 8) & 65280)))) / 10.0f;
                            vehicleStatusIdx = -2;
                            // added a partial update here
                            parserCallback.onVehicleStatusReceived(vehicleStatus);
                            return;
                        default:
                            parserCallback.onParseError(rx_buffer);
                            vehicleStatusIdx = 0;
                            return;
                    }
                } else {
                    parserCallback.onParseError(rx_buffer); // also triggered when ignoring received bytes, not always in error
                    vehicleStatusIdx = 0;
                    return;
                }
            case 3:
                if (rx_buffer[1] == -1) {
                    vehicleStatus.speed = (rx_buffer[2] & 255) | ((rx_buffer[3] << 8) & 65280);
                    vehicleStatus.rpm = (rx_buffer[4] & 255) | ((rx_buffer[5] << 8) & 65280);
                    vehicleStatus.torque = (short) ((rx_buffer[6] & 255) | ((rx_buffer[7] << 8) & 65280));
                    vehicleStatus.power = Math.round(((((float) (vehicleStatus.torque * 2)) * 3.1415927f) * ((float) vehicleStatus.rpm)) / 60000.0f);
                    parserCallback.onSpeedRPMTorqueReceived(vehicleStatus.speed, vehicleStatus.rpm, vehicleStatus.torque, (float) vehicleStatus.power);
                    return;
                }
                parserCallback.onParseError(rx_buffer);
                return;
            case 4:
                if ((rx_buffer[1] & 255) == vehicleStatusIdx2) {
                    switch (rx_buffer[1]) {
                        case -2:
                            vehicleStatus.tripMeter = (float) ((rx_buffer[2] & 255) | (((rx_buffer[3] & 255) << 8) & 65280) | (((rx_buffer[4] & 255) << 16) & 16711680) | (((rx_buffer[5] & 255) << 24) & -16777216));
                            vehicleStatusIdx2 = 0;
                            // added a partial update here
                            parserCallback.onVehicleStatusReceived(vehicleStatus);
                            return;
                        case 0:
                            vehicleStatus.totalOdometer = (float) ((rx_buffer[2] & 255) | ((rx_buffer[3] << 8) & 65280) | ((rx_buffer[4] << 16) & 16711680) | ((rx_buffer[5] << 24) & -16777216));
                            vehicleStatus.avgConsumptionUnit = (char) (rx_buffer[6] & 255);
                            vehicleStatus.distanceUnit = (char) (rx_buffer[7] & 255);
                            vehicleStatusIdx2 = 254;
                            // added a partial update here
                            parserCallback.onVehicleStatusReceived(vehicleStatus);
                            return;
                        default:
                            vehicleStatusIdx2 = 0;
                            return;
                    }
                } else {
                    // invalid data received, reset counter
                    vehicleStatusIdx2 = 0;
                    return;
                }
            case 20:
            case 21:
                // case 20 and 21 were duplicated here, check if we can combine or have error in logic
                if (rx_buffer[1] == -1) {
                    vehicleStatus.chgPowerLimit = rx_buffer[2];
                    parserCallback.onChgPwrLimitReceived(vehicleStatus.chgPowerLimit);
                    return;
                } else {
                    parserCallback.onParseError(rx_buffer);
                    return;
                }
            case 25:
                // diagnostics data received
                if (rx_buffer[1] == dtcIdx) {
                    byte baseIdx = (byte) dtcIdx;
                    int dtc1 = ((rx_buffer[4] & 15) << 16) | (rx_buffer[3] << 8) | rx_buffer[2];
                    int dtc2 = ((rx_buffer[7] & 15) << 16) | (rx_buffer[3] << 6) | rx_buffer[5];
                    dtcIdx++;
                    return;
                } else if (rx_buffer[1] == -1) {
                    byte baseIdx = (byte) -1;
                    int dtc1 = ((rx_buffer[4] & 15) << 16) | (rx_buffer[3] << 8) | rx_buffer[2];
                    int dtc2 = ((rx_buffer[7] & 15) << 16) | (rx_buffer[3] << 6) | rx_buffer[5];
                    dtcIdx = 0;
                    return;
                } else if (rx_buffer[1] == -2) {
                    byte baseIdx = (byte) -2;
                    int dtc1 = ((rx_buffer[4] & 15) << 16) | (rx_buffer[3] << 8) | rx_buffer[2];
                    int dtc2 = ((rx_buffer[7] & 15) << 16) | (rx_buffer[3] << 6) | rx_buffer[5];
                    dtcIdx = 0;
                    return;
                } else {
                    parserCallback.onParseError(rx_buffer);
                    dtcIdx = 0;
                    return;
                }
            case 26:
                // GPS data received from bike
                if (rx_buffer[1] == 0) {
                    gpsData.course = (rx_buffer[2] & 255) | ((rx_buffer[3] & 1) << 8);
                    gpsData.speed = ((rx_buffer[3] >> 1) & Const.KEYCODE_MEDIA_PAUSE) | ((rx_buffer[4] & 3) << 8);
                    gpsData.latitudeDeciMilliminutes = (((rx_buffer[4] & 255) >> 2) & 63) | ((rx_buffer[5] & 255) << 6);
                    gpsData.latitudeMinutes = rx_buffer[6] & 63;
                    gpsData.latSign = ((rx_buffer[7] >> 6) & 1) != 0 ? -1 : 1;
                    gpsData.latitudeDegrees = ((rx_buffer[6] >> 6) & 3) | ((rx_buffer[7] & 63) << 2);
                } else if (rx_buffer[1] == 1) {
                    gpsData.fix = rx_buffer[2] & 3;
                    int altitude = ((rx_buffer[2] >> 2) & 63) | (rx_buffer[3] << 6) | ((rx_buffer[4] & 1) << 14);
                    int altitudeSign = (rx_buffer[4] >> 1) & 1;
                    if (altitudeSign != 0) {
                        altitude = -altitude;
                    }
                    gpsData.altitude = altitude;
                    gpsData.longitudeDeciMilliminutes = (((rx_buffer[4] & 255) >> 2) & 63) | ((rx_buffer[5] & 255) << 6);
                    gpsData.longitudeMinutes = rx_buffer[6] & 63;
                    gpsData.longitudeDegrees = ((rx_buffer[6] >> 6) & 3) | ((rx_buffer[7] & 63) << 2);
                    gpsData.lonSign = ((rx_buffer[7] >> 6) & 1) != 0 ? -1 : 1;
                } else if (rx_buffer[1] == -2) {
                    gpsData.dateMilliseconds = (rx_buffer[2] & 255) | ((rx_buffer[3] & 3) << 8);
                    gpsData.dateSeconds = (rx_buffer[3] >> 2) & 63;
                    gpsData.dateMinutes = rx_buffer[4] & 63;
                    gpsData.dateHour = ((rx_buffer[4] >> 6) & 3) | ((rx_buffer[5] & 7) << 2);
                    gpsData.dateDay = (rx_buffer[5] >> 3) & 31;
                    gpsData.dateMonth = rx_buffer[6] & 15;
                    gpsData.dateYear = ((rx_buffer[6] >> 4) & 15) | ((rx_buffer[7] & 7) << 4);
                    gpsData.connectedSatellites = (rx_buffer[7] >> 3) & 31;
                    gpsData.latitude = gpsData.convertLat();
                    gpsData.longitude = gpsData.convertLon();
                    if (!(gpsData.latitude == 0.0d && gpsData.longitude == 0.0d)) {
                        parserCallback.onGPSDataReceived(gpsData);
                    }
                } else {
                    parserCallback.onParseError(rx_buffer);
                }
                if (System.currentTimeMillis() > lastCPOIUpdate + 15000) {
                    lastCPOIUpdate = System.currentTimeMillis();
                    if (gpsData.convertLat() != 0.0d || gpsData.convertLon() != 0.0d) {
                        parserCallback.onNearbyChargePointsRequested();
                        return;
                    }
                    return;
                }
                return;
            case 28:
                if (rx_buffer[1] == -1) {
                    parserCallback.onChargePointsRequested();
                    return;
                } else {
                    parserCallback.onParseError(rx_buffer);
                    return;
                }
        }
    }
}
