package be.hcpl.android.energica.model.evmonitor

import androidx.annotation.Keep

@Keep
data class LoginOutput(
    val SUCCESS: String?,
    val token: String?,
    val error: String?
    )