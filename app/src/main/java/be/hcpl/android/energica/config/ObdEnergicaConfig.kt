package be.hcpl.android.energica.config

import com.github.pires.obd.commands.ObdCommand
import com.github.pires.obd.commands.protocol.ObdRawCommand

interface ObdConfig {
    val repeatableCommands: List<ObdCommand>
    val singleCommands: List<ObdCommand>
}

object ObdEnergicaConfig : ObdConfig {

    override val repeatableCommands: List<ObdCommand>
        get() {
            return listOf(
                // these are looped commands for updating values

                // charge state
                ObdRawCommand("ATCRA201"),
                Energica201Command(),

                // bms state
                ObdRawCommand("ATCRA200"),
                Energica200Command(),
            )
        }

    override val singleCommands: List<ObdCommand>
        get() {
            return listOf(

                // some startup commands to be executed once
                // overview of all AT commands for this chipset to be found here https://www.sparkfun.com/datasheets/Widgets/ELM327_AT_Commands.pdf
                // ATZ = reset all
                // ATE0 = echo off
                // ATL0 = line feed off
                // ATST__ = set time out

                // Energica boot sequence
                ObdRawCommand("ATWS"), // "warm start" this gives info on the used OBD dongle
                ObdRawCommand("ATE0"), // "echo OFF"
                ObdRawCommand("ATSP6"), // "set protocol"
                ObdRawCommand("ATAT1"), // "adaptive timing"
                // note that most of the default obd commands from lib won't work with headers ON
                ObdRawCommand("ATH1"), // "headers ON", false
                ObdRawCommand("ATL0"), // "linefeed OFF"
                ObdRawCommand("ATS0"), // "set timeout"

                // these are all executed once
                ObdRawCommand("ATCAF1"), // "auto formatting ON"
                ObdRawCommand("ATSH79B"),
                ObdRawCommand("ATFCSH79B"),
                ObdRawCommand("ATFCSD300000"),
                ObdRawCommand("ATFCSM1"),
                ObdRawCommand("ATSH7E7"),
                ObdRawCommand("ATFCSH7E7"),
                ObdRawCommand("ATCRA7EF"),
                //ObdRawCommand("0902"), // "VIN" // VIN

                // Energica specific config
                ObdRawCommand("ATCAF0"), // "auto format OFF", false
                ObdRawCommand("ATSH7DF"), // "SH 7DF", true // FIXME needed?

            )

        }
}
