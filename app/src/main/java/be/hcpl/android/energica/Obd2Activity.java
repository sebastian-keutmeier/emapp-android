package be.hcpl.android.energica;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import be.hcpl.android.energica.config.Energica201Command;
import be.hcpl.android.energica.fragments.ChargeSettingsFragment;
import be.hcpl.android.energica.helpers.Const;
import be.hcpl.android.energica.helpers.ExportData;
import be.hcpl.android.energica.helpers.Help;
import be.hcpl.android.energica.services.ble.BleService;
import be.hcpl.android.energica.services.gps.GpsService;
import be.hcpl.android.energica.services.obd2.Obd2Service;
import be.hcpl.android.energica.services.obd2.ObdCommandJob;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static be.hcpl.android.energica.config.Energica201Command.CHARGE_STATE_AC;
import static be.hcpl.android.energica.config.Energica201Command.CHARGE_STATE_DC;
import static be.hcpl.android.energica.config.Energica201Command.CHARGE_STATE_DC_ALT;
import static be.hcpl.android.energica.config.Energica201Command.CHARGE_STATE_IDLE;
import static be.hcpl.android.energica.fragments.ChargeSettingsFragment.EMPTY_SET;
import static be.hcpl.android.energica.helpers.Const.BROADCAST_OBD2_DATA;
import static be.hcpl.android.energica.helpers.Const.BROADCAST_OBD2_DATA_CHARGE;
import static be.hcpl.android.energica.helpers.Const.BROADCAST_OBD2_GENERIC_DATA;
import static be.hcpl.android.energica.helpers.Const.BROADCAST_OBD2_RAW;
import static be.hcpl.android.energica.helpers.Const.BROADCAST_OBD2_STATE;
import static be.hcpl.android.energica.helpers.Const.OBD2_CHARGE_STATE;
import static be.hcpl.android.energica.helpers.Const.OBD2_CURRENT_AC;
import static be.hcpl.android.energica.helpers.Const.OBD2_CURRENT_DC;
import static be.hcpl.android.energica.helpers.Const.OBD2_JOB_STATE;
import static be.hcpl.android.energica.helpers.Const.OBD2_NAME;
import static be.hcpl.android.energica.helpers.Const.OBD2_RAW_DATA;
import static be.hcpl.android.energica.helpers.Const.OBD2_RESULT;
import static be.hcpl.android.energica.helpers.Const.OBD2_SOC;
import static be.hcpl.android.energica.helpers.Const.OBD2_TEMP1;
import static be.hcpl.android.energica.helpers.Const.OBD2_TEMP2;
import static be.hcpl.android.energica.helpers.Const.OBD2_VOLT;

@SuppressLint("MissingPermission")
public class Obd2Activity extends AppCompatActivity {

    // no more bluetooth and file permission handling duplicated here since it's already done in main view

    private TextView serviceStatusTextView;
    private TextView btStatusTextView;
    private TextView obdStatusTextView;
    private TextView tempView, socView, currentAcView, currentDcView, chargeStateView, voltageView;

    private SharedPreferences prefs;

    // toolbar state icons
    private ImageView gpsStateView, bleStateView, obdStateView;
    // containers
    private ViewGroup energicaContainer, genericContainer;

    // region service connection

    private final ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(
                ComponentName className,
                IBinder binder) {

            startService();
            setStateConnected();
        }

        @Override
        public void onServiceDisconnected(ComponentName className) {
            stopService();
        }
    };

    private void startService() {
        log("Starting OBD2 data");
        setStateConnecting();
        // start background service for GPS location
        final Intent intent = new Intent(Obd2Activity.this, Obd2Service.class);
        // to improve background handling on Android 8 and higher run foreground service instead
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent);
            bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
        } else {
            startService(intent);
        }
    }

    private void stopService() {
        log("Stopping OBD2 data");
        if (serviceConnection != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            try {
                unbindService(serviceConnection);
            } catch (Exception e) { /* ignore */ }
        }
        // stop background service for GPS location
        Intent intent = new Intent(Obd2Activity.this, Obd2Service.class);
        stopService(intent);
        // update status views
        setStateStopped();
    }

    private void log(String message) {
        ExportData.Companion.log(getApplicationContext(), message);
    }

    // endregion

    // region state handling

    private void setStateConnecting() {
        updateServiceStates();
        serviceStatusTextView.setText(getString(R.string.status_service_starting));
        btStatusTextView.setText(getString(R.string.status_bluetooth_connecting));
        obdStatusTextView.setText(getString(R.string.status_obd_no_support));
    }

    private void setStateConnected() {
        updateServiceStates();
        serviceStatusTextView.setText(getString(R.string.status_service_running));
        btStatusTextView.setText(getString(R.string.status_bluetooth_connected));
        obdStatusTextView.setText(getString(R.string.status_obd_ready));
    }

    private void setStateStopped() {
        updateServiceStates();
        serviceStatusTextView.setText(getString(R.string.status_service_stopped));
        btStatusTextView.setText(getString(R.string.status_bluetooth_ok));
        obdStatusTextView.setText(getString(R.string.status_obd_disconnected));
    }

    // endregion

    // region broadcast messages from service

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        public void onReceive(
                Context context,
                Intent intent) {
            String event = intent.getStringExtra(Const.BROADCAST_EVENT);
            if (BROADCAST_OBD2_STATE.equals(event)) {
                onStateUpdate((ObdCommandJob.ObdCommandJobState) intent.getSerializableExtra(OBD2_JOB_STATE));
            } else if (BROADCAST_OBD2_RAW.equals(event)) {
                onRawDataReceived(intent.getStringExtra(OBD2_RAW_DATA));
            } else if (BROADCAST_OBD2_DATA.equals(event)) {
                int soc = intent.getIntExtra(OBD2_SOC, 0);
                int voltage = intent.getIntExtra(OBD2_VOLT, 0);
                int currentAC = intent.getIntExtra(OBD2_CURRENT_AC, 0);
                int currentDC = intent.getIntExtra(OBD2_CURRENT_DC, 0);
                int temp1 = intent.getIntExtra(OBD2_TEMP1, 0);
                int temp2 = intent.getIntExtra(OBD2_TEMP2, 0);
                onDataReceived(soc, voltage, currentAC, currentDC, temp1, temp2);
            } else if( BROADCAST_OBD2_DATA_CHARGE.equals(event)){
                int chargeState = intent.getIntExtra(OBD2_CHARGE_STATE, CHARGE_STATE_IDLE);
                onChargeStateReceived(chargeState);
            } else if (BROADCAST_OBD2_GENERIC_DATA.equals(event)) {
                onGenericDataReceived(intent.getStringExtra(OBD2_NAME), intent.getStringExtra(OBD2_RESULT));
            }
        }
    };

    private void onStateUpdate(final ObdCommandJob.ObdCommandJobState jobState) {
        // update all services state in toolbar
        updateServiceStates();
        // update this specific service state
        serviceStatusTextView.setText(R.string.status_service_running);
        // update bt connection state
        if (ObdCommandJob.ObdCommandJobState.BT_CONNECTED.equals(jobState)) {
            btStatusTextView.setText("connected");
        } else if (ObdCommandJob.ObdCommandJobState.BT_FAILED.equals(jobState)) {
            btStatusTextView.setText("conn. failed");
        }
        // update obd connection state
        if (ObdCommandJob.ObdCommandJobState.EXECUTION_ERROR.equals(jobState)) {
            obdStatusTextView.setText("exec. error");
        } else if (ObdCommandJob.ObdCommandJobState.BROKEN_PIPE.equals(jobState)) {
            obdStatusTextView.setText("conn. broken");
            //stopService(); // FIXME this was removed to fix reconnect improve upon state
        } else if (ObdCommandJob.ObdCommandJobState.NOT_SUPPORTED.equals(jobState)) {
            obdStatusTextView.setText("no support");
        } else {
            obdStatusTextView.setText(R.string.status_obd_ready);
        }
    }

    private void onRawDataReceived(final String rawData) {
        // state update
        obdStatusTextView.setText(getString(R.string.status_obd_data));
        // TODO remove for now rawDataView.setText(rawData);
    }

    private void onDataReceived(int soc, int voltage, int currentAC, int currentDC, int temp1, int temp2) {
        // state update
        obdStatusTextView.setText(getString(R.string.status_obd_data));
        socView.setText(soc + "%");
        currentAcView.setText("AC:" + currentAC + "A");
        currentDcView.setText("DC:" + currentDC + "A");
        tempView.setText(temp1 + "/" + temp2 + "°C");
        voltageView.setText("Volts:"+formatVoltage(voltage));
        // update graph
        updateGraph(soc, currentAC, currentDC, temp1, temp2);
    }

    private float formatVoltage(int voltage){
        return voltage/10;
    }

    private void onChargeStateReceived(int chargeState) {
        // charge state update
        switch (chargeState){
            case CHARGE_STATE_IDLE: {
                chargeStateView.setText("Charging: IDLE");
                currentAcView.setVisibility(GONE);
                currentDcView.setVisibility(GONE);
                break;
            }
            case CHARGE_STATE_AC: {
                chargeStateView.setText("Charging: AC");
                currentAcView.setVisibility(VISIBLE);
                currentDcView.setVisibility(GONE);
                break;
            }
            case CHARGE_STATE_DC:
            case CHARGE_STATE_DC_ALT: {
                chargeStateView.setText("Charging: DC");
                currentAcView.setVisibility(GONE);
                currentDcView.setVisibility(VISIBLE);
                break;
            }
            default: {
                // also print unknown states so we can adapt
                chargeStateView.setText("Charging: "+chargeState);
                currentAcView.setVisibility(GONE);
                currentDcView.setVisibility(GONE);
                break;
            }
        }
    }

    private void onGenericDataReceived(
            final String name,
            final String result) {

        // state update
        obdStatusTextView.setText(getString(R.string.status_obd_data));
        // check if already a view added for this
        TextView dataView = genericContainer.findViewWithTag(name);
        if (dataView == null) {
            dataView = new TextView(this);
            dataView.setTag(name);
            genericContainer.addView(dataView);
        }
        // update values for that specific view
        dataView.setText(name + " : " + result);
    }

    // endregion

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_odb2);

        // get preferences
        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        final Toolbar toolbar = findViewById(R.id.home_topbar);
        setSupportActionBar(toolbar);
        TextView titleView = findViewById(R.id.toolbar_title);
        titleView.setText(R.string.obd2_connection);

        // state indications
        gpsStateView = findViewById(R.id.gps_state);
        gpsStateView.setVisibility(View.VISIBLE);
        gpsStateView.setColorFilter(Help.withColor(getApplicationContext(), R.color.dark_white));
        bleStateView = findViewById(R.id.ble_state);
        bleStateView.setVisibility(View.VISIBLE);
        bleStateView.setColorFilter(Help.withColor(getApplicationContext(), R.color.dark_white));
        obdStateView = findViewById(R.id.obd_state);
        obdStateView.setVisibility(View.VISIBLE);
        obdStateView.setColorFilter(Help.withColor(getApplicationContext(), R.color.dark_white));

        // state views
        serviceStatusTextView = findViewById(R.id.service_status);
        btStatusTextView = findViewById(R.id.BT_STATUS);
        obdStatusTextView = findViewById(R.id.OBD_STATUS);

        // start stop connection
        startView = findViewById(R.id.start);
        startView.setOnClickListener(v -> startService());
        stopView = findViewById(R.id.stop);
        stopView.setOnClickListener(v -> stopService());

        // select OBD device
        spinner = findViewById(R.id.spinner);
        populatePairedBtDevices();

        // data view containers
        genericContainer = findViewById(R.id.container_obd_generic);
        energicaContainer = findViewById(R.id.container_obd_energica);

        // Energica specific views
        socView = findViewById(R.id.soc);
        voltageView = findViewById(R.id.volt);
        tempView = findViewById(R.id.temp);
        currentAcView = findViewById(R.id.currentAc);
        currentDcView = findViewById(R.id.currentDc);
        chargeStateView = findViewById(R.id.chargeState);

        // graph options
        initGraph();

        findViewById(R.id.reset_graph).setOnClickListener(view -> {
            new AlertDialog.Builder(this, R.style.AlertDialogStyle)
                    .setMessage("CLEAR Graphs?")
                    .setPositiveButton(android.R.string.yes, (dialog, whichButton) -> {
                        // just resets both graphs to empty
                        powerACSeries.resetData(EMPTY_SET);
                        powerDCSeries.resetData(EMPTY_SET);
                        tempSeries.resetData(EMPTY_SET);
                        lastSocPlotted = -1;
                        // same for time based graph here
                        temp1Series.resetData(EMPTY_SET);
                        temp2Series.resetData(EMPTY_SET);
                        socSeries.resetData(EMPTY_SET);
                        startTime = System.currentTimeMillis();
                        log("charge graphs cleared");
                    })
                    .setNegativeButton(android.R.string.no, null).show();
        });
        findViewById(R.id.export_data).setOnClickListener(view -> {
            ExportData.Companion.exportData(this, powerACSeries, "charge-power-AC", 0, lastSocPlotted);
            ExportData.Companion.exportData(this, powerDCSeries, "charge-power-DC", 0, lastSocPlotted);
            ExportData.Companion.exportData(this, tempSeries, "charge-temp", 0, lastSocPlotted);
            log("OBD2 charge graph data exported");
            Toast.makeText(this, "data exported", Toast.LENGTH_SHORT).show();
        });
    }

    // region graph

    private GraphView graph1;
    private final LineGraphSeries<DataPoint> powerACSeries = new LineGraphSeries<>(EMPTY_SET);
    private final LineGraphSeries<DataPoint> powerDCSeries = new LineGraphSeries<>(EMPTY_SET);
    private final LineGraphSeries<DataPoint> tempSeries = new LineGraphSeries<>(EMPTY_SET);
    private int lastSocPlotted = -1;
    private int maxValuesSoc = 100;

    private GraphView graph2;
    private final LineGraphSeries<DataPoint> temp1Series = new LineGraphSeries<>(EMPTY_SET);
    private final LineGraphSeries<DataPoint> temp2Series = new LineGraphSeries<>(EMPTY_SET);
    private final LineGraphSeries<DataPoint> socSeries = new LineGraphSeries<>(EMPTY_SET);
    private long startTime = 0L;
    private int maxValuesTime = 60_000;

    private void initGraph() {
        // setup charge graph1 for display of charge current based on SOC
        graph1 = findViewById(R.id.graph1);
        ChargeSettingsFragment.initStyleGraph(getApplicationContext(), graph1);
        graph1.addSeries(powerACSeries); // charge power plotted by SOC
        powerACSeries.setColor(ContextCompat.getColor(this, R.color.fluo_green));
        graph1.addSeries(powerDCSeries); // charge power plotted by SOC
        powerDCSeries.setColor(ContextCompat.getColor(this, R.color.colorPrimary));
        graph1.addSeries(tempSeries); // battery temp plotted by SOC
        tempSeries.setColor(ContextCompat.getColor(this, R.color.colorAccent));
        graph1.getGridLabelRenderer().setHorizontalLabelsVisible(true); // show SOC
        graph1.getViewport().setYAxisBoundsManual(false); // allow to grow while charging

        // setup charge graph2 for display of charge current based on time
        graph2 = findViewById(R.id.graph2);
        graph2.setVisibility(View.VISIBLE);
        graph2.getGridLabelRenderer().setGridColor(ContextCompat.getColor(this, R.color.dark_white));
        graph2.getGridLabelRenderer().setVerticalAxisTitleColor(ContextCompat.getColor(this, R.color.dark_white));
        graph2.getGridLabelRenderer().setVerticalLabelsColor(ContextCompat.getColor(this, R.color.dark_white));
        graph2.getGridLabelRenderer().setHorizontalAxisTitleColor(ContextCompat.getColor(this, R.color.dark_white));
        graph2.getGridLabelRenderer().setHorizontalLabelsColor(ContextCompat.getColor(this, R.color.dark_white));
        graph2.getViewport().setXAxisBoundsManual(false);
        graph2.getViewport().setYAxisBoundsManual(true);
        graph2.getViewport().setMaxY(100); // max 100 SOC value to display

        graph2.addSeries(temp1Series); // battery temp plotted by SOC
        temp1Series.setColor(ContextCompat.getColor(this, R.color.colorAccent));
        graph2.addSeries(temp2Series); // battery temp plotted by SOC
        temp2Series.setColor(ContextCompat.getColor(this, R.color.colorAccent));
        graph2.addSeries(socSeries); // battery temp plotted by SOC
        socSeries.setColor(ContextCompat.getColor(this, R.color.dark_white));

        startTime = System.currentTimeMillis();
        // time based X axis, need to format timestamp here to readable date
        graph2.getGridLabelRenderer().setLabelFormatter(
                new DateAsXAxisLabelFormatter(getApplicationContext(), new SimpleDateFormat("m'm'ss's'")));
    }

    private void updateGraph(
            final int soc,
            final float powerAC,
            // charging current
            final float powerDC,
            // charging current
            final int temp1,
            final int temp2) {

        // for now graph is organised on SOC value while charging, so only on decrease we show an update on graph1
        // TODO check if we can also plot something while riding on this graph? then SOC drops instead of increasing
        if (soc > lastSocPlotted) {
            powerACSeries.appendData(new DataPoint(soc, powerAC), false, maxValuesSoc);
            powerDCSeries.appendData(new DataPoint(soc, powerDC), false, maxValuesSoc);
            // plot highest temp for now
            tempSeries.appendData(new DataPoint(soc, Math.max(temp1, temp2)), false, maxValuesSoc);
            lastSocPlotted = soc;
        }

        // also log temp and soc values in time
        long timestamp = System.currentTimeMillis();
        long time = timestamp-startTime;
        temp1Series.appendData(new DataPoint(time, temp1), false, maxValuesTime);
        temp2Series.appendData(new DataPoint(time, temp2), false, maxValuesTime);
        socSeries.appendData(new DataPoint(time, soc), true, maxValuesTime);
    }

    // endregion

    // region menu

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.close_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // Handle item selection
        if (item.getItemId() == R.id.close) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    // endregion

    // region bluetooth device selection

    private Spinner spinner;
    private List<String> values = new ArrayList<>();
    private List<String> labels = new ArrayList<>();
    private View startView;
    private View stopView;

    private void populatePairedBtDevices() {
        final String lastSelection = prefs.getString(Obd2Service.SELECTED_DEVICE_KEY, null);
        int lastSelectinoPosition = 0;
        final BluetoothAdapter mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();
        if (!pairedDevices.isEmpty()) {
            for (BluetoothDevice device : pairedDevices) {
                labels.add(device.getName() + "\n" + device.getAddress());
                values.add(device.getAddress());
            }
            // if not set already and we have devices set first as default
            if (lastSelection == null) {
                prefs.edit().putString(Obd2Service.SELECTED_DEVICE_KEY, values.get(0)).apply();
            } else {
                lastSelectinoPosition = values.indexOf(lastSelection);
            }
        }
        // populate spinner here
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.spinner_item, labels);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinner.setAdapter(adapter);
        // remember last selection
        if (lastSelectinoPosition > 0 && lastSelectinoPosition < adapter.getCount()) {
            // otherwise use remembered last selection
            spinner.setSelection(lastSelectinoPosition);
        }
        // update in preferences on selection
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(
                    AdapterView<?> parent,
                    View view,
                    int position,
                    long id) {
                prefs.edit().putString(Obd2Service.SELECTED_DEVICE_KEY, values.get(position)).apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    // endregion

    // region lifecycle

    @Override
    protected void onPause() {
        unregisterReceiver(broadcastReceiver);
        super.onPause();
    }

    protected void onResume() {
        super.onResume();
        // register broadcast receiver
        IntentFilter filter = new IntentFilter(Const.BROADCAST_FILTER);
        registerReceiver(broadcastReceiver, filter);

        // update visibility based on prefs
        if (prefs.getBoolean(getString(R.string.key_obd2_use_generic), false)) {
            genericContainer.setVisibility(VISIBLE);
            energicaContainer.setVisibility(GONE);
        } else {
            genericContainer.setVisibility(GONE);
            energicaContainer.setVisibility(VISIBLE);
        }

        // get Bluetooth device state
        final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        if (btAdapter != null && !btAdapter.isEnabled()) {
            btStatusTextView.setText(getString(R.string.status_bluetooth_disabled));
        } else if (btAdapter != null) {
            btStatusTextView.setText(getString(R.string.status_bluetooth_ok));
        }

        // update state of services
        updateServiceStates();
    }

    // endregion

    // region state handling

    private void updateServiceStates() {
        final boolean gpsServiceRunning = Help.isServiceRunning(getApplicationContext(), GpsService.class);
        final boolean bleServiceRunning = Help.isServiceRunning(getApplicationContext(), BleService.class);
        final boolean obd2ServiceRunning = Help.isServiceRunning(getApplicationContext(), Obd2Service.class);
        gpsStateView.setColorFilter(Help.withStateColor(getApplicationContext(), gpsServiceRunning));
        bleStateView.setColorFilter(Help.withStateColor(getApplicationContext(), bleServiceRunning));
        obdStateView.setColorFilter(Help.withStateColor(getApplicationContext(), obd2ServiceRunning));
    }

    // endregion

}
