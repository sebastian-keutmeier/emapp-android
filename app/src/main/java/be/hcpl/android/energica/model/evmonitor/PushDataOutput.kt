package be.hcpl.android.energica.model.evmonitor

import androidx.annotation.Keep

@Keep
data class PushDataOutput(
    val SUCCESS: String?,
    val error: String?
)
