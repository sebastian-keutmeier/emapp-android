package be.hcpl.android.energica.config

import com.github.pires.obd.commands.protocol.ObdRawCommand

class HybridBatteryPackCommand : ObdRawCommand("01 5B") {

    // "Hybrid battery pack remaining life"), // 100*A/255
    // use this for SOC on Zero motorcycles
    // on Energica motorcycles this returns SOH (=correct)

    companion object {

        const val NAME = "Battery Pack Life"

    }

    override fun getName() = NAME

    override fun getFormattedResult(): String {
        return try {
            // ex. 44 5B FF
            "SOC/SOH=${calculateValue()}%"
        } catch (e: Exception) {
            "ERROR ${e.localizedMessage} RAW=$result"
        }
    }

    fun calculatedValue() = try {
        calculateValue()
    } catch (e: Exception) {
        0
    }

    private fun calculateValue() = 100 * int(4, 6) / 255

}