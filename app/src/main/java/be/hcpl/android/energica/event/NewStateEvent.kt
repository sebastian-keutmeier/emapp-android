package be.hcpl.android.energica.event

import java.io.Serializable

data class NewStateEvent(val state: Int) : Serializable
