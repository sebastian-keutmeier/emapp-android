package be.hcpl.android.energica.services.evmonitor

import be.hcpl.android.energica.model.evmonitor.LoginOutput
import be.hcpl.android.energica.model.evmonitor.PushDataInput
import be.hcpl.android.energica.model.evmonitor.PushDataOutput
import be.hcpl.android.energica.model.evmonitor.VehiclesOutput
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.http.Field
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Multipart
import retrofit2.http.Part


interface EvMonitorApiService {

    // see https://ev-monitor.com/web/devs/

    @FormUrlEncoded
    @POST("api/login")
    fun login(@Field("username") username: String, @Field("password") password: String): Call<LoginOutput>

    @FormUrlEncoded
    @POST("api/get_vehicles")
    fun vehicles(@Field("token") token: String): Call<VehiclesOutput>

    @FormUrlEncoded
    @POST("api/push_data")
    fun pushData(@Field("token") token: String, @FieldMap data: Map<String, @JvmSuppressWildcards Any>): Call<PushDataOutput>

}

class EvMonitorApiServiceImpl {

    fun getService(): EvMonitorApiService {
        return getRetrofit().create(EvMonitorApiService::class.java)
    }

    private fun getRetrofit(): Retrofit {
        val interceptor = HttpLoggingInterceptor()
        interceptor.apply { interceptor.level = HttpLoggingInterceptor.Level.BODY }
        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

        val gson = GsonBuilder().setLenient().create()
        return Retrofit.Builder()
            .baseUrl("https://ev-monitor.com/")
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

}
