package be.hcpl.android.energica.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Locale;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import be.hcpl.android.energica.BleEnergicaActivity;
import be.hcpl.android.energica.R;
import be.hcpl.android.energica.event.BleDataEvent;
import be.hcpl.android.energica.helpers.ExportData;
import be.hcpl.android.energica.model.ble.VehicleStatus;
import be.hcpl.android.energica.services.ble.ConnectionManager;

import static be.hcpl.android.energica.fragments.ChargeSettingsFragment.EMPTY_SET;
import static be.hcpl.android.energica.helpers.Const.DISTANCE_UNIT_MI;

public class OdometerFragment extends Fragment {

    private BleEnergicaActivity context;

    private TextView tripMeter;
    private TextView odoMeter;
    private TextView powerValue;
    private TextView rpmValue;
    private TextView speedValue;
    private TextView torqueValue;

    private int lastPlottedValue = 0;
    private final LineGraphSeries<DataPoint> powerSeries = new LineGraphSeries<>(EMPTY_SET);
    private final LineGraphSeries<DataPoint> torqueSeries = new LineGraphSeries<>(EMPTY_SET);
    private final LineGraphSeries<DataPoint> speedSeries = new LineGraphSeries<>(EMPTY_SET);

    private int getColor(int colorResId){
        return ResourcesCompat.getColor(getResources(), colorResId, null);
    }

    @Nullable
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {

        this.context = (BleEnergicaActivity) getActivity();
        final View rootView = inflater.inflate(R.layout.fragment_main_odometer, container, false);

        powerValue = rootView.findViewById(R.id.power_value);
        torqueValue = rootView.findViewById(R.id.torque_value);
        speedValue = rootView.findViewById(R.id.speed_indicator_label);
        rpmValue = rootView.findViewById(R.id.rpm_label);
        odoMeter = rootView.findViewById(R.id.odometer_value);
        tripMeter = rootView.findViewById(R.id.tripmeter_value);

        // have live updating graph for charge power and soc over time
        // have a single graph to display several series (power, torque, speed) all based on tripmeter
        final GraphView graphView = rootView.findViewById(R.id.graph);
        initStyleGraph(graphView);
        graphView.addSeries(powerSeries); // charge power plotted over trip meter distance
        powerSeries.setColor(getColor(R.color.fluo_green));
        powerValue.setTextColor(getColor(R.color.fluo_green));
        graphView.addSeries(torqueSeries); // torque plotted over trip meter distance
        torqueSeries.setColor(getColor(R.color.colorPrimary));
        torqueValue.setTextColor(getColor(R.color.colorPrimary));
        graphView.addSeries(speedSeries); // speed plotted over trip meter distance
        speedSeries.setColor(getColor(R.color.colorAccent));
        speedValue.setTextColor(getColor(R.color.colorAccent));

        rootView.findViewById(R.id.odometer_reset_btn).setOnClickListener(view -> {
            new AlertDialog.Builder(getContext(), R.style.AlertDialogStyle)
                    .setMessage("RESET trip meter?")
                    .setPositiveButton(android.R.string.yes, (dialog, whichButton) -> {
                        ConnectionManager.getInstance().getCurrentStatus().tripMeter = 0.0f;
                        context.setResetTrip();
                        log("reset trip meter requested");
                    })
                    .setNegativeButton(android.R.string.no, null).show();
        });
        rootView.findViewById(R.id.export_data).setOnClickListener(view -> {
            ExportData.Companion.exportData(context, speedSeries, "speed", 0, lastPlottedValue);
            ExportData.Companion.exportData(context, torqueSeries, "torque", 0, lastPlottedValue);
            ExportData.Companion.exportData(context, powerSeries, "power", 0, lastPlottedValue);
            Toast.makeText(context, "data exported", Toast.LENGTH_SHORT).show();
        });
        rootView.findViewById(R.id.clear_data).setOnClickListener(view -> {
            new AlertDialog.Builder(getContext(), R.style.AlertDialogStyle)
                    .setMessage("CLEAR Graph?")
                    .setPositiveButton(android.R.string.yes, (dialog, whichButton) -> {
                        // also resets graph
                        lastPlottedValue = 0;
                        speedSeries.resetData(EMPTY_SET);
                        torqueSeries.resetData(EMPTY_SET);
                        powerSeries.resetData(EMPTY_SET);
                        log("Graph cleared");
                    })
                    .setNegativeButton(android.R.string.no, null).show();
        });

        return rootView;
    }

    private void log(final String message) {
        ExportData.Companion.log(getContext(), message);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private void initStyleGraph(final GraphView graphView) {
        graphView.setVisibility(View.VISIBLE);
        graphView.getGridLabelRenderer().setGridColor(getColor(R.color.dark_white));
        graphView.getGridLabelRenderer().setVerticalAxisTitleColor(getColor(R.color.dark_white));
        graphView.getGridLabelRenderer().setVerticalLabelsColor(getColor(R.color.dark_white));
        graphView.getGridLabelRenderer().setHorizontalAxisTitleColor(getColor(R.color.dark_white));
        graphView.getGridLabelRenderer().setHorizontalLabelsColor(getColor(R.color.dark_white));
        graphView.getGridLabelRenderer().setHorizontalLabelsVisible(false);
        // don't autoscale viewport, speed and most limits are 200
        graphView.getViewport().setXAxisBoundsManual(true);
        graphView.getViewport().setYAxisBoundsManual(true); // should we allow for auto scaling here?
        graphView.getViewport().setMinX(0);
        graphView.getViewport().setMaxX(100);
        graphView.getViewport().setMinY(-50); // negative to show regen
        graphView.getViewport().setMaxY(200);
    }

    private void updateGraph(final VehicleStatus vehicleStatus) {
        if( !isAdded() ) return;
        updateGraph(vehicleStatus.tripMeter, vehicleStatus.power, vehicleStatus.torque, vehicleStatus.speed);
    }

    private void updateGraph(
            float tripMeter,
            int power,
            int torque,
            int speed) {

        if( !isAdded() ) return;
        //if (tripMeter > lastPlottedValue) {
            //lastPlottedValue = Math.round(tripMeter); // if I restore this make sure to do it on every 100m
            lastPlottedValue += 1; // this makes it a real time graph instead of based on distance...
            powerSeries.appendData(new DataPoint(lastPlottedValue, power), true, 100);
            torqueSeries.appendData(new DataPoint(lastPlottedValue, torque), true, 100);
            speedSeries.appendData(new DataPoint(lastPlottedValue, speed), true, 100);
        //}
    }

    @Subscribe
    public void onNewValuesEvent(BleDataEvent event) {
        updateValues(event.getSelectedUnit(), event.getStatus());
    }

    private void updateValues(
            final int selectedUnit,
            final VehicleStatus currentStatus) {

        if( !isAdded() ) return;
        if (selectedUnit == DISTANCE_UNIT_MI) {
            torqueValue.setText(
                    String.format(Locale.getDefault(), "%d %s", currentStatus.torque, getString(R.string.unit_imperial_torque)));
            speedValue.setText(
                    String.format(Locale.getDefault(), "%d %s", currentStatus.speed, getString(R.string.unit_imperial_speed)));
            odoMeter.setText(
                    String.format(Locale.getDefault(), "%.1f %s", currentStatus.totalOdometer, getString(R.string.unit_imperial_distance)));
            tripMeter.setText(
                    String.format(Locale.getDefault(), "%.1f %s",
                                  (((double) currentStatus.tripMeter) * 0.621371192237d) / 10.0d,
                                  getString(R.string.unit_imperial_distance)));
        } else {
            torqueValue.setText(
                    String.format(Locale.getDefault(), "%d %s", currentStatus.torque, getString(R.string.unit_metric_torque)));
            speedValue.setText(
                    String.format(Locale.getDefault(), "%d %s", currentStatus.speed, getString(R.string.unit_metric_speed)));
            odoMeter.setText(
                    String.format(Locale.getDefault(), "%.1f %s", currentStatus.totalOdometer, getString(R.string.unit_metric_distance)));
            tripMeter.setText(
                    String.format(Locale.getDefault(), "%.1f %s",
                                  currentStatus.tripMeter / 10.0f, getString(R.string.unit_metric_distance)));
        }
        powerValue.setText(String.format(Locale.getDefault(), "%d kW", currentStatus.power));
        rpmValue.setText(String.format(Locale.getDefault(), "%d rpm", currentStatus.rpm));
        updateGraph(currentStatus);
    }

}
