package be.hcpl.android.energica.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Locale;

import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import be.hcpl.android.energica.BleEnergicaActivity;
import be.hcpl.android.energica.R;
import be.hcpl.android.energica.event.BleDataEvent;
import be.hcpl.android.energica.event.ChargeLimitEvent;
import be.hcpl.android.energica.helpers.Const;
import be.hcpl.android.energica.helpers.ExportData;
import be.hcpl.android.energica.model.ble.VehicleStatus;
import be.hcpl.android.energica.services.ble.ConnectionManager;

import static be.hcpl.android.energica.helpers.Const.STATE_CHARGE;

public class ChargeSettingsFragment extends Fragment {

    public static final DataPoint[] EMPTY_SET = new DataPoint[]{new DataPoint(0, 0)};

    private BleEnergicaActivity context;

    private TextView currentLimitView;
    private TextView rangeView;
    private TextView consumptionView;
    private TextView chargeLevel;
    private TextView chargePower;
    private TextView packI;
    private TextView packV;
    private TextView resBatteryEnergy;
    private GraphView graphView1;
    private GraphView graphView2;

    private final LineGraphSeries<DataPoint> powerSeries = new LineGraphSeries<>(EMPTY_SET);
    private final LineGraphSeries<DataPoint> socSeries = new LineGraphSeries<>(EMPTY_SET);
    private boolean scaledForAC = false;

    @SuppressLint("StringFormatInvalid")
    public View onCreateView(
            LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState) {

        context = (BleEnergicaActivity) getActivity();
        final View rootView = inflater.inflate(R.layout.fragment_main_charge, container, false);

        chargeLevel = rootView.findViewById(R.id.charge_level_view);
        chargeLevel.setTextColor(getColor(R.color.colorAccent));
        currentLimitView = rootView.findViewById(R.id.set_charge_limit);
        rangeView = rootView.findViewById(R.id.range);
        consumptionView = rootView.findViewById(R.id.avg_consumption);
        chargePower = rootView.findViewById(R.id.power_limit_value);
        packI = rootView.findViewById(R.id.pack_i_value);
        packV = rootView.findViewById(R.id.pack_v_value);
        resBatteryEnergy = rootView.findViewById(R.id.res_battery_energy_value);
        final Button stopChargeView = rootView.findViewById(R.id.shutdown_btn);
        final Button resetGraph = rootView.findViewById(R.id.reset_graph);
        final Button scaleGraph = rootView.findViewById(R.id.scale_graph);

        // have live updating graph for charge power and soc over time
        graphView1 = rootView.findViewById(R.id.graph1);
        graphView2 = rootView.findViewById(R.id.graph2);
        initStyleGraph(getContext(), graphView1);
        initStyleGraph(getContext(), graphView2);
        graphView1.addSeries(powerSeries); // charge power plotted over time
        chargePower.setTextColor(getColor(R.color.fluo_green));
        powerSeries.setColor(getColor(R.color.fluo_green));
        graphView2.addSeries(socSeries); // charge power plotted by SOC
        socSeries.setColor(getColor(R.color.colorAccent));
        graphView2.getGridLabelRenderer().setHorizontalAxisTitleColor(getColor(R.color.colorAccent));
        graphView2.getGridLabelRenderer().setHorizontalLabelsColor(getColor(R.color.colorAccent));
        graphView2.getGridLabelRenderer().setHorizontalLabelsVisible(true); // show SOC

        scaleGraph.setOnClickListener(v -> {
            if (scaledForAC) {
                scaledForAC = false;
                graphView1.getViewport().setMaxY(25);
                graphView2.getViewport().setMaxY(25);
            } else {
                scaledForAC = true;
                graphView1.getViewport().setMaxY(4);
                graphView2.getViewport().setMaxY(4);
            }
        });
        resetGraph.setOnClickListener(v -> {
            new AlertDialog.Builder(getContext(), R.style.AlertDialogStyle)
                    .setMessage("CLEAR Graphs?")
                    .setPositiveButton(android.R.string.yes, (dialog, whichButton) -> {
                        // just resets both graphs to empty
                        powerSeries.resetData(EMPTY_SET);
                        socSeries.resetData(EMPTY_SET);
                        lastSocPlotted = 0;
                        lastTimePlotted = 0;
                        log("charge graphs cleared");
                    })
                    .setNegativeButton(android.R.string.no, null).show();
        });
        rootView.findViewById(R.id.export_data).setOnClickListener(view -> {
            ExportData.Companion.exportData(context, powerSeries, "charge-power", 0, lastTimePlotted);
            ExportData.Companion.exportData(context, socSeries, "charge-soc", 0, lastSocPlotted);
            Toast.makeText(context, "data exported", Toast.LENGTH_SHORT).show();
        });
        // removed input for charge limit config
        stopChargeView.setOnClickListener(v -> {
            if (ConnectionManager.getInstance().getCurrentStatus().state == Const.STATE_CHARGE) {
                new AlertDialog.Builder(getContext(), R.style.AlertDialogStyle)
                        .setMessage(R.string.ChgTermConfirm).setCancelable(true)
                        .setPositiveButton(R.string.optionYes, (dialog, id) -> {
                            // sends stop charging command
                            context.setChgTermination();
                            log("stop charge confirmed and requested");
                            // informs user
                            Toast.makeText(context, getString(R.string.message_disconnect_charger),
                                           Toast.LENGTH_SHORT).show();
                        }).setNegativeButton(R.string.optionNo, (dialog, id) -> dialog.cancel())
                .show();
            } else {
                Toast.makeText(getContext(), "not charging", Toast.LENGTH_SHORT).show();
            }
        });
        return rootView;
    }

    private void log(final String message) {
        ExportData.Companion.log(getContext(), message);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private int getColor(int colorResId){
        return ResourcesCompat.getColor(getResources(), colorResId, null);
    }

    private int lastTimePlotted = 0;
    private int lastSocPlotted = 0;

    public static void initStyleGraph(final Context context, final GraphView graphView) {
        graphView.setVisibility(View.VISIBLE);
        graphView.getGridLabelRenderer().setGridColor(ContextCompat.getColor(context, R.color.dark_white));
        graphView.getGridLabelRenderer().setVerticalAxisTitleColor(ContextCompat.getColor(context, R.color.dark_white));
        graphView.getGridLabelRenderer().setVerticalLabelsColor(ContextCompat.getColor(context, R.color.dark_white));
        graphView.getGridLabelRenderer().setHorizontalAxisTitleColor(ContextCompat.getColor(context, R.color.dark_white));
        graphView.getGridLabelRenderer().setHorizontalLabelsColor(ContextCompat.getColor(context, R.color.dark_white));
        graphView.getGridLabelRenderer().setHorizontalLabelsVisible(false); // hide time counter by default
        // don't autoscale viewport, charge power is max 24 kW
        graphView.getViewport().setXAxisBoundsManual(true);
        graphView.getViewport().setYAxisBoundsManual(true);
        graphView.getViewport().setMinX(0);
        graphView.getViewport().setMaxX(100); // display up to 100% SOC
        graphView.getViewport().setMinY(0);
        graphView.getViewport().setMaxY(25);// max charge power 24 kW on CCS DC
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onChargeLimit(ChargeLimitEvent event) {
        if( !isAdded() ) return;
        // only set charge limit on view when we receive this value as a confirmation
        currentLimitView.setText(String.format(getString(R.string.charge_power_limit_value), event.getLimit()));
    }

    private void updateGraph(
            final int soc,
            final float power) {

        if( !isAdded() ) return;
        if (soc > lastSocPlotted) {
            socSeries.appendData(new DataPoint(soc, power), false, 100); // no auto scrolling
            lastSocPlotted = soc;
        }
        powerSeries.appendData(new DataPoint(lastTimePlotted++, power), true, 100);
    }

    private void updateChargeValues(
            final float chargePower,
            final VehicleStatus vehicleStatus) {

        if( !isAdded() ) return;
        if (vehicleStatus.state == STATE_CHARGE) {
            this.chargePower.setText(String.format(Locale.getDefault(), "Charge power: %.1f kW", chargePower));
            packI.setText(String.format("bPackI: %.1f", vehicleStatus.bPackI));
            packV.setText(String.format("bPackV: %.1f", vehicleStatus.bPackV));
            resBatteryEnergy.setText(String.format("resBattEnergy: %d", vehicleStatus.resBatteryEnergy));
        } else {
            this.chargePower.setText(R.string.no_charge_power);
        }
        chargeLevel.setText(String.format(Locale.getDefault(), "%d", vehicleStatus.soc));
        consumptionView.setText(String.format("Avg. Consumption: %.1f", vehicleStatus.avgConsumption));
        rangeView.setText(String.format("Range: %d", vehicleStatus.range));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleDataEvent(BleDataEvent event) {
        updateValues(event.getStatus());
    }

    private void updateValues(final VehicleStatus currentStatus) {
        if( !isAdded() ) return;
        // removed fixed interval of DTC code requests here
        if (currentStatus.state == STATE_CHARGE) {
            // charge data handling
            int chargePower = 0;
            if (currentStatus.subState == 101) {
                chargePower =
                        (int) (((double) currentStatus.cMAINSV) * 1.0d * ((double) currentStatus.cMAINSC));
            } else if (currentStatus.subState == 104) {
                chargePower = (int) (((double) currentStatus.bPackV) * 0.94d * ((double) currentStatus.bPackI));
            }
            // changed to push all new values and now longer check on previous charge value
            float chargePowerInKwh = ((float) chargePower) / 1000.0f;
            // update charge view here
            updateChargeValues(chargePowerInKwh, currentStatus);
            updateGraph(currentStatus.soc, chargePowerInKwh);
        } else {
            // when user is not charging show some defaults on that view
            updateChargeValues(Const.NOT_CHARGING, currentStatus);
        }
    }

}
