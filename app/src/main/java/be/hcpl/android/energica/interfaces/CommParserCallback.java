package be.hcpl.android.energica.interfaces;

import be.hcpl.android.energica.model.ble.GpsData;
import be.hcpl.android.energica.model.ble.VehicleStatus;

public interface CommParserCallback {
    void onChargePointsRequested();

    void onChgPwrLimitReceived(Integer num);

    void onConnectionConfirmed();

    void onConnectionRejected();

    void onGPSDataReceived(GpsData gpsData);

    void onNearbyChargePointsRequested();

    void onParseError(byte[] bArr);

    void onSeedReceived(Integer num);

    void onSpeedRPMTorqueReceived(int i, int i2, int i3, float f);

    void onVehicleStatusReceived(VehicleStatus vehicleStatus);

    class Null implements CommParserCallback {
        public void onSeedReceived(Integer seed) {
        }

        public void onVehicleStatusReceived(VehicleStatus status) {
        }

        public void onParseError(byte[] record) {
        }

        public void onConnectionConfirmed() {
        }

        public void onConnectionRejected() {
        }

        public void onChgPwrLimitReceived(Integer chgPwrLimit) {
        }

        public void onSpeedRPMTorqueReceived(int speed, int rpm, int torque, float power) {
        }

        public void onGPSDataReceived(GpsData gpsData) {
        }

        public void onNearbyChargePointsRequested() {
        }

        public void onChargePointsRequested() {
        }
    }
}
