package be.hcpl.android.energica.config

import com.github.pires.obd.commands.protocol.ObdRawCommand
import kotlin.math.pow

class OdoMeterCommand : ObdRawCommand("01 A6") {

    // ObdRawCommand("01 A6"), // "odometer"), // formula is (A2^24 + B2^16 + C*2^8 + D) / 10

    override fun getName() = "Odometer"

    override fun getFormattedResult(): String {
        return try {
            // ex. 41 A6 00 15 19 F0
            "${calculateValue()}"
        } catch(e: Exception) {
            "ERROR ${e.localizedMessage} RAW=$result"
        }
    }

    fun calculatedValue() : Int = try { calculateValue().toInt() } catch(e: Exception) { 0 }

    private fun calculateValue() = (int(4,6)*2.0.pow(24) + int(6,8)*2.0.pow(16) + int(8,10)*2.0.pow(8)) / 100
}