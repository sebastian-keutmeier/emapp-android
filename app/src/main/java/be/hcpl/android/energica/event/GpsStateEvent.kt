package be.hcpl.android.energica.event

data class GpsStateEvent(val checked: Boolean)
