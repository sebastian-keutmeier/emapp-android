package be.hcpl.android.energica.event;

import java.io.Serializable

data class ChargeLimitEvent(val limit: Int) : Serializable
