package be.hcpl.android.energica.model.ocm

import androidx.annotation.Keep

@Keep
class AddressInfo(
    val AddressLine1: String?,
    val AddressLine2: String?,
    val ContactTelephone1: String?,
    val ContactTelephone2: String?,
    val Distance: Float = 0f,
    val DistanceUnit: Int = 0,
    val ID: Int = 0,
    val Latitude: Double = 0.0,
    val Longitude: Double = 0.0,
    val Title: String?,
    val Town: String?
)
