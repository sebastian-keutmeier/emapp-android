package be.hcpl.android.energica.model.evmonitor

import androidx.annotation.Keep

@Keep
data class Vehicle(
    val id: String,
    val user_id: String?,
    val make: String?,
    val model: String?,
    val nickname: String?,
    val plan: String?,
    val plan_active: String?,
    val plan_active_until: String?, // probably a date
    val selected: Boolean = false
)