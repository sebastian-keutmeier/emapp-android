package be.hcpl.android.energica.event

import be.hcpl.android.energica.model.ble.VehicleStatus
import java.io.Serializable

data class BleDataEvent(val selectedUnit: Int, val status: VehicleStatus) : Serializable
