package be.hcpl.android.energica.helpers

import android.content.Context
import android.os.Environment
import android.util.Log
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import java.io.File
import java.io.FileWriter
import java.lang.System.currentTimeMillis
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class ExportData {

    companion object {

        private const val TAG = "ExportData"
        val timeFormatFile: DateFormat = SimpleDateFormat("yyyy-MM-dd-HHmmss")
        val timeFormatLogStatement: DateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")
        val dateformatGpx: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")

        const val header = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?><gpx xmlns=\"http://www.topografix.com/GPX/1/1\" " +
                "creator=\"MapSource 6.15.5\" version=\"1.1\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"  " +
                "xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd\">\n"

        private var logFileTimeStamp: String? = null
        private var appLogFile: File? = null
        private val appLogDateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd-HH")

        fun getCurrentAppLogFile(context: Context): File? {
            val currentTimestamp = appLogDateFormat.format(Date())
            if (appLogFile == null || logFileTimeStamp != currentTimestamp) {
                // make a new log file every hour (at least)
                logFileTimeStamp = currentTimestamp
                appLogFile = File(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
                    "${logFileTimeStamp}-${Const.APP_LOGS_FILE}")
            }
            return appLogFile
        }

        fun log(context: Context, message: String) {
            Log.d(TAG, message)
            getCurrentAppLogFile(context)
            if(appLogFile?.exists() == false){
                appLogFile?.createNewFile()
            }
            appLogFile?.appendText("${timeFormatLogStatement.format(currentTimeMillis())} ${message}\n")
        }

        fun exportData(context: Context, series: LineGraphSeries<DataPoint>, name: String, rangeStart: Double, rangeEnd: Double) {
            val speedValues = series.getValues(rangeStart, rangeEnd)
            val data = StringBuilder()
            while (speedValues.hasNext()) {
                val dataPoint = speedValues.next()
                data.append(dataPoint.x).append(";").append(dataPoint.y).append(System.lineSeparator())
            }
            writeToNewFile(
                "export-$name-${SimpleDateFormat("yyyy-MM-dd-HHmmss").format(currentTimeMillis())}.txt", data.toString(),
                context
            )
        }

        fun writeToNewFile(fileName: String, data: String, context: Context) {
            try {
                val filePath = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
                val fileWriter = FileWriter(File(filePath, fileName))
                fileWriter.write(data)
                fileWriter.flush()
                fileWriter.close()
            } catch (e: Exception) {
                log(context, "File write failed: $e")
            }
        }

        fun appendToFile(fileName: String, data: String, context: Context) {
            try {
                val filePath = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
                val fileWriter = FileWriter(File(filePath, fileName), true)
                fileWriter.write(data + System.lineSeparator())
                fileWriter.flush()
                fileWriter.close()
            } catch (e: Exception) {
                log(context, "File write failed: $e")
            }
        }

    }
}
