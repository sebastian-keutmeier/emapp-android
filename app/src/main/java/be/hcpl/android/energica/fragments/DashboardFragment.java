package be.hcpl.android.energica.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import be.hcpl.android.energica.MainActivity;
import be.hcpl.android.energica.R;
import be.hcpl.android.energica.config.HybridBatteryPackCommand;
import be.hcpl.android.energica.event.BleDataEvent;
import be.hcpl.android.energica.event.LastLocationEvent;
import be.hcpl.android.energica.event.LocationEvent;
import be.hcpl.android.energica.helpers.Const;
import be.hcpl.android.energica.helpers.ExportData;
import be.hcpl.android.energica.helpers.Help;
import be.hcpl.android.energica.model.ble.VehicleStatus;
import be.hcpl.android.energica.model.ocm.ChargeLocation;
import be.hcpl.android.energica.services.chargemap.ChargeMapRepo;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static be.hcpl.android.energica.helpers.Const.BLE_EVENT;
import static be.hcpl.android.energica.helpers.Const.BROADCAST_BLE_EVENT;
import static be.hcpl.android.energica.helpers.Const.BROADCAST_LOCATION;
import static be.hcpl.android.energica.helpers.Const.BROADCAST_OBD2_DATA;
import static be.hcpl.android.energica.helpers.Const.BROADCAST_OBD2_GENERIC_DATA;
import static be.hcpl.android.energica.helpers.Const.DISTANCE_UNIT_KM;
import static be.hcpl.android.energica.helpers.Const.DISTANCE_UNIT_MI;
import static be.hcpl.android.energica.helpers.Const.KM_TO_MI;
import static be.hcpl.android.energica.helpers.Const.LOCATION_LAT;
import static be.hcpl.android.energica.helpers.Const.LOCATION_LON;
import static be.hcpl.android.energica.helpers.Const.LOCATION_PRECISION;
import static be.hcpl.android.energica.helpers.Const.LOCATION_SPEED;
import static be.hcpl.android.energica.helpers.Const.MPS_TO_KPH;
import static be.hcpl.android.energica.helpers.Const.OBD2_NAME;
import static be.hcpl.android.energica.helpers.Const.OBD2_RESULT;
import static be.hcpl.android.energica.helpers.Const.OBD2_SOC;
import static be.hcpl.android.energica.helpers.Const.OBD2_TEMP1;
import static be.hcpl.android.energica.helpers.Const.OBD2_TEMP2;

public class DashboardFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private final List<Location> mapPoints = new ArrayList<>(); // collects even when in background
    private final List<Polyline> plotted = new ArrayList<>(); // plotted route
    private final List<Marker> markers = new ArrayList<>(); // charge stops
    private final List<Marker> nearbyChargers = new ArrayList<>(); // chargers close to current location
    private final List<Long> chargeStopLengths = new ArrayList<>();
    private MainActivity context;
    private SharedPreferences prefs;
    private TextView consumptionView;
    private TextView socView;
    private TextView rangeView;
    private TextView statsView;
    private Button logChargeView;
    private View bleDataView;
    private TextView massiveSpeedView;
    private View obd2DataView;
    private TextView obd2SocView;
    private TextView obd2TempView;

    private TextView reserveView;
    private TextView calculatedConsumptionView;
    private Location lastLocation;
    // service for receiving nearby chargers
    private ChargeMapRepo chargeMapRepo;
    private GoogleMap mapView;
    private Marker lastPositionMarker = null; // current position (end of route)

    private Marker startPosition = null; // initial position on map
    private boolean isReceivingGpsData = false;
    private Location lastLocationFetchedChargers = null;
    private VehicleStatus lastStatus;
    private boolean charging = false;
    private long lastChargeStopTimestamp = 0;
    private int topSpeed = 0;
    private double calculatedDistance = 0;
    private float initialOdoValue = 0;
    private int initialBatteryValue = 0;
    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        public void onReceive(
                Context context,
                Intent intent) {
            String event = intent.getStringExtra(Const.BROADCAST_EVENT);
            if (BROADCAST_LOCATION.equals(event)) {
                double latitude = intent.getDoubleExtra(LOCATION_LAT, 0d);
                double longitude = intent.getDoubleExtra(LOCATION_LON, 0d);
                float speed = intent.getFloatExtra(LOCATION_SPEED, 0f);
                float precision = intent.getFloatExtra(LOCATION_PRECISION, 0f);
                Location location = new Location("gps"); // received location from background service
                location.setLatitude(latitude);
                location.setLongitude(longitude);
                location.setSpeed(speed);
                location.setAccuracy(precision);
                onLocationChanged(new LocationEvent(location));
                return;
            } else if (BROADCAST_BLE_EVENT.equals(event)) {
                // receive BLE updates
                final BleDataEvent data = (BleDataEvent)intent.getSerializableExtra(BLE_EVENT);
                onBleDataEvent(data);
            } else if (BROADCAST_OBD2_DATA.equals(event)) {
                int soc = intent.getIntExtra(OBD2_SOC, 0);
                int temp1 = intent.getIntExtra(OBD2_TEMP1, 0);
                int temp2 = intent.getIntExtra(OBD2_TEMP2, 0);
                updateObdData(soc, temp1, temp2);
            } else if (BROADCAST_OBD2_GENERIC_DATA.equals(event)) {
                // also check for name so we can display SOC from generic OBD data
                String name = intent.getStringExtra(OBD2_NAME);
                String value = intent.getStringExtra(OBD2_RESULT);
                if( HybridBatteryPackCommand.NAME.equals(name) ){
                    updateObdData(value);
                }
            }
        }
    };

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        chargeMapRepo = new ChargeMapRepo(getContext());
        metricUnitValue = getString(R.string.value_metric);
        imperialUnitValue = getString(R.string.value_imperial);
    }

    private String metricUnitValue = "metric", imperialUnitValue = "imperial";

    @Nullable
    public View onCreateView(
            LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState) {

        context = (MainActivity) getActivity();
        prefs = Help.getSharedPrefs(context);

        IntentFilter filter = new IntentFilter(Const.BROADCAST_FILTER);
        getContext().registerReceiver(broadcastReceiver, filter);

        final View rootView = inflater.inflate(R.layout.fragment_main_dashboard, container, false);
        socView = rootView.findViewById(R.id.soc_value);
        rangeView = rootView.findViewById(R.id.range_value);
        consumptionView = rootView.findViewById(R.id.consumption_value);
        statsView = rootView.findViewById(R.id.stats);
        reserveView = rootView.findViewById(R.id.reserve_value);
        calculatedConsumptionView = rootView.findViewById(R.id.calculated_consumption);
        bleDataView = rootView.findViewById(R.id.ble_data);
        massiveSpeedView = rootView.findViewById(R.id.massive_speed);
        obd2DataView = rootView.findViewById(R.id.obd_data);
        obd2SocView = rootView.findViewById(R.id.obd_soc_value);
        obd2TempView = rootView.findViewById(R.id.obd_temp_value);

        // some more features added
        rootView.findViewById(R.id.export_gpx).setOnClickListener(v -> {
            exportRouteToGpx();
            Toast.makeText(context, "data GPX export done", Toast.LENGTH_SHORT).show();
        });
        // allow user to manually start and stop charge logs
        logChargeView = rootView.findViewById(R.id.log_charge);
        logChargeView.setOnClickListener(v -> {
            handleManualChargeStop();
        });
        rootView.findViewById(R.id.reset_trip).setOnClickListener(v -> {
            new AlertDialog.Builder(getContext(), R.style.AlertDialogStyle)
                    .setMessage("CLEAR current Route (export first)?")
                    .setPositiveButton(android.R.string.yes, (dialog, whichButton) -> {
                        // context.getParser().setResetTrip();
                        resetGpsData();
                        resetStats();
                        log("route removed from map");
                    })
                    .setNegativeButton(android.R.string.no, null).show();
        });

        // enables google maps
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    // endregion

    @Override
    public void onDestroy() {
        getContext().unregisterReceiver(broadcastReceiver);
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();

        // update from prefs
        isReceivingGpsData = prefs.getBoolean(getString(R.string.key_use_device_gps), false);
        // enable or disable massive speed view visibility based on user prefs
        massiveSpeedView.setVisibility(prefs.getBoolean(getString(R.string.key_show_massive_speed), false) ? VISIBLE : GONE);

        clearRouteFromMap();
        plotCompleteRoute();
        updateStats();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapView = googleMap;
        plotCompleteRoute();
        mapView.setOnMarkerClickListener(this);
    }

    @Override
    public boolean onMarkerClick(@NonNull @NotNull final Marker marker) {
        // don't provide navigation to current position marker
        if( lastPositionMarker.equals(marker) ){
            return false;
        }
        new AlertDialog.Builder(requireContext(), R.style.AlertDialogStyle)
                .setMessage("Navigate to this place? \""+marker.getTitle()+"\"")
                .setPositiveButton(android.R.string.yes, (dialog, whichButton) -> {
                    log("triggered navigation to selected marker");
                    double latitude = marker.getPosition().latitude;
                    double longitude = marker.getPosition().longitude;

                    String uri = "geo:"+latitude+","+longitude+"?q="+latitude+","+longitude+"";
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(uri)));
                })
                .setNegativeButton(android.R.string.no, null).show();
        return true;
    }

    public void resetGpsData() {
        clearRouteFromMap();
        mapPoints.clear();
        plotted.clear();
        markers.clear();
        nearbyChargers.clear();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLocationChanged(LocationEvent event) {
        if (!isReceivingGpsData && "gps".equals(event.getLocation().getProvider())) {
            // auto correct for receiving GPS data
            isReceivingGpsData = true;
        }
        if (isReceivingGpsData && !"gps".equals(event.getLocation().getProvider())) {
            // filter on gps data if gps data is enabled
            return;
        }
        if (lastLocation != null) {
            // update (unit is meters) FIXME not very accurate, prefer ODO values instead if available
            calculatedDistance += lastLocation.distanceTo(event.getLocation());
        }
        lastLocation = event.getLocation();
        updateChargers();
        updateStats();
        mapPoints.add(event.getLocation());
        if (isAdded()) {
            // when visible also update map
            addLastPointToRoute();
            // easy fix is to keep track of points added or not is to just rebuild the complete route on resume
        }
    }

    private void updateChargers() {
        // no need to without location
        if (lastLocation == null) {
            return;
        }
        // don't check when feature is disabled
        if( !prefs.getBoolean(getString(R.string.key_fetch_chargers), true)){
            return;
        }
        // based on new lastLocation first check if we have covered another (configurable distance)
        int distanceBetween = Integer.parseInt(prefs.getString(getString(R.string.key_distance_between_chargers), "5")) * 1_000;
        if (lastLocationFetchedChargers == null || lastLocationFetchedChargers.distanceTo(lastLocation) > distanceBetween) {

            // go fetch new nearby chargers
            chargeMapRepo.chargeLocations(
                    lastLocation.getLatitude(),
                    lastLocation.getLongitude(),
                    () -> {
                        int searchRadiusInKm = Integer.parseInt(prefs.getString(getString(R.string.key_radius_chargers), "20"));
                        int numberOfResults = Integer.parseInt(prefs.getString(getString(R.string.key_max_number_chargers), "10"));
                        String typeOfChargers = prefs.getString(getString(R.string.key_charger_types), "33,32");
                        String distanceUnit = "KM";
                        if( imperialUnitValue.equals(prefs.getString(getString(R.string.key_unit_system), metricUnitValue)) )
                            distanceUnit = "Miles";
                        return new ChargeMapRepo.ChargeConfig(
                                searchRadiusInKm,
                                numberOfResults,
                                typeOfChargers,
                                distanceUnit
                        );
                    },
                    (chargers) -> {
                        log("received nearby chargers for current location");
                        handleNewNearbyChargers(chargers);
                        return null;
                        },
                    () -> {
                        log("failed to get nearby chargers from api");
                        return null;
                    },
                    () -> {
                        // fetching chargers was queued already so this request was ignored
                        return null;
                    });
        }
    }

    private void handleNewNearbyChargers(List<ChargeLocation> chargeLocations) {
        lastLocationFetchedChargers = lastLocation; // mark that we just fetched chargers
        // remove any existing chargers (if present)
        for (Marker chargerMarker : nearbyChargers) {
            chargerMarker.remove();
        }
        nearbyChargers.clear();
        if (mapView == null) {
            return;
        }
        // and plot the new ones on the map instead
        for (ChargeLocation charger : chargeLocations) {
            LatLng position = new LatLng(charger.getAddressInfo().getLatitude(), charger.getAddressInfo().getLongitude());
            String title = charger.getAddressInfo().getTitle();
            nearbyChargers.add(
                    mapView.addMarker(new MarkerOptions()
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                            .position(position)
                            .title(title)));
        }
    }

    // endregion

    // region route plotting on map

    private void log(String message) {
        ExportData.Companion.log(getContext(), message);
    }

    private void clearRouteFromMap() {
        // doesn't remove any collected data, only visually removed from map
        for (Polyline line : plotted) {
            line.remove(); // complete route plotted on map
        }
        for (Marker marker : markers) {
            marker.remove(); // includes start and end position
        }
        for (Marker marker : nearbyChargers) {
            marker.remove(); // removes all chargers on the map
        }
    }

    private void exportRouteToGpx() {
        String fileName = "route-" + ExportData.Companion.getTimeFormatLogStatement().format(System.currentTimeMillis()) + ".gpx";
        StringBuilder segments = new StringBuilder("<trk><name>" + fileName + "</name>");
        segments.append("<trkseg>\n");
        for (Location location : mapPoints) {
            segments.append("<trkpt lat=\"").append(location.getLatitude()).append("\" lon=\"").append(location.getLongitude()).append(
                    "\"><time>").append(ExportData.Companion.getDateformatGpx().format(
                    new Date(location.getTime()))).append("</time></trkpt>\n");
        }
        segments.append("</trkseg></trk>");
        // also append all chargestops as waypoints
        StringBuilder waypoints = new StringBuilder();
        for (Marker marker : markers) {
            waypoints.append("<wpt lat=\"").append(marker.getPosition().latitude).append("\" lon=\"").append(marker.getPosition().longitude).append(
                    "\">").append(
                    // TODO we should store time for chargestops also
                    //"<time>" + gpxDateFormat.format(new Date(marker.getPosition().location.getTime())) + "</time>" +
                    "<name>").append(marker.getTitle()).append("</name></wpt>");
        }
        String footer = "</gpx>";
        ExportData.Companion.writeToNewFile(fileName, ExportData.header + segments.toString() + waypoints.toString() + footer, requireContext());
    }

    private void plotCompleteRoute() {
        // used to recover from app being in background while service was running, recreate the route from collected data
        if (mapView == null) {
            return;
        }

        // add all markers again to map
        List<Marker> newMarkers = new ArrayList<>();
        for (Marker marker : markers) {
            // also need to keep track of the lastPositionMarker so it can be updated once we continue plotting
            lastPositionMarker = mapView.addMarker(new MarkerOptions().position(marker.getPosition()).title(marker.getTitle()));
            newMarkers.add(lastPositionMarker);
        }
        // need to reset the references to these markers as they are now added again
        markers.clear();
        markers.addAll(newMarkers);

        // add all nearby chargers to map
        List<Marker> newChargerMarkers = new ArrayList<>();
        for (Marker charger : nearbyChargers) {
            newChargerMarkers.add(mapView.addMarker(
                    new MarkerOptions()
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                            .position(charger.getPosition())
                            .title(charger.getTitle())));
        }
        nearbyChargers.clear();
        nearbyChargers.addAll(newChargerMarkers);

        // lines we can just clear as they are build from Locations
        plotted.clear();
        // renders a line on map for all points retrieved so far
        PolylineOptions polyLineOptions = new PolylineOptions();//.clickable(true);
        for (Location location : mapPoints) {
            if (location.getLatitude() != 0 && location.getLongitude() != 0) {
                polyLineOptions.add(new LatLng(location.getLatitude(), location.getLongitude()));
            }
        }
        Polyline polyline = mapView.addPolyline(polyLineOptions);
        // Store a data object with the polyline, used here to indicate an arbitrary type.
        //polyline.tag = date // uses date as a tag on the lines
        polyline.setWidth(getResources().getDimension(R.dimen.map_stroke_width));
        polyline.setColor(ResourcesCompat.getColor(getResources(), R.color.map_stroke_color, null));
        polyline.setJointType(JointType.ROUND);
        // keep reference so we can remove lines from map when needed
        plotted.add(polyline);
        // move to last position
        centerMapUpdatingLastPoint(false); // last position is already there as a marker so don't add it again
    }

    private void addLastPointToRoute() {

        // plot start position if not already done
        if (startPosition == null && !mapPoints.isEmpty()) {
            Location location = mapPoints.get(0);
            initialMapLocation(new LastLocationEvent(location.getLatitude(), location.getLongitude()));
            return;
        }

        // TODO documentation needed on the POI colors used
        // red = start and end point markers
        // green = charge stops
        // blue = chargers nearby

        Location lastPoint = mapPoints.get(mapPoints.size() - 1);
        Location previousPoint = mapPoints.get(mapPoints.size() - 2);

        PolylineOptions polyLineOptions = new PolylineOptions();
        polyLineOptions.add(new LatLng(previousPoint.getLatitude(), previousPoint.getLongitude()));
        polyLineOptions.add(new LatLng(lastPoint.getLatitude(), lastPoint.getLongitude()));
        Polyline polyline = mapView.addPolyline(polyLineOptions);
        // Store a data object with the polyline, used here to indicate an arbitrary type.
        polyline.setWidth(getResources().getDimension(R.dimen.map_stroke_width));
        polyline.setColor(ResourcesCompat.getColor(getResources(), R.color.map_stroke_color, null));
        polyline.setJointType(JointType.ROUND);
        // keep reference so we can remove lines from map when needed
        plotted.add(polyline);
        centerMapUpdatingLastPoint(true); // also updates last position marker
    }

    @Subscribe
    public void initialMapLocation(LastLocationEvent event) {
        if (startPosition != null || mapView == null) {
            return; // ignore if we already have a start location plotted on map or if map isn't ready
        }
        // mark start location on map and change zoom level
        LatLng location = new LatLng(event.getLatitude(), event.getLongitude());
        startPosition = mapView.addMarker(new MarkerOptions().position(location).title("Start"));
        mapView.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 10));
        markers.add(startPosition);
    }

    private void centerMapUpdatingLastPoint(boolean addLastPosition) {
        // center map on last location (no change in zoom level)
        if (!mapPoints.isEmpty()) {
            Location location = mapPoints.get(mapPoints.size() - 1);
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            if (addLastPosition) {
                if (lastPositionMarker != null) {
                    lastPositionMarker.remove();
                    markers.remove(lastPositionMarker);
                }
                lastPositionMarker = mapView.addMarker(new MarkerOptions().position(latLng).title("You"));
                markers.add(lastPositionMarker);
            }
            mapView.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        }
    }

    private void handleManualChargeStop() {
        // toggles charging state manually for users that don't have a BLE connection
        if (charging) {
            stopCharging();
        } else {
            startCharging();
        }
    }

    // endregion

    // region charge stop handling

    private void startCharging() {
        charging = true;
        logChargeView.setText("Stop Charge");
        showChargeStopOnMap();
        lastChargeStopTimestamp = System.currentTimeMillis();
        log("START charging, total charge sessions " + chargeStopLengths.size());
        Toast.makeText(context, "Charge Session STARTED", Toast.LENGTH_SHORT).show();
    }

    private void stopCharging() {
        charging = false;
        logChargeView.setText("Start Charge");
        long totalChargingTimeInMs = System.currentTimeMillis() - lastChargeStopTimestamp;
        chargeStopLengths.add(totalChargingTimeInMs);
        log("STOP charging session of " + (totalChargingTimeInMs / 1000 / 60) + " min, total charge sessions " + chargeStopLengths.size());
        Toast.makeText(context, "Charge Session STOPPED", Toast.LENGTH_SHORT).show();
        // triggers update of stats on data screen
        updateStats();
    }

    // attempt for automatic charge stop marking from state change events
    private void checkForChargeStops(final BleDataEvent event) {
        // FIXME auto charge stop creation doesn't work as expected
        // checks if state changed to charging to log a charge stop
        VehicleStatus newStatus = event.getStatus();
        if (newStatus.isCharging() && lastStatus != null && !lastStatus.isCharging()) {
            // user started charging
            lastStatus = newStatus;
            startCharging();
        } else if (!newStatus.isCharging() && lastStatus != null && lastStatus.isCharging()) {
            // user stopped charging
            stopCharging();
        }
    }

    private void showChargeStopOnMap() {
        // just a single charge stop marker on the map using current location
        if (lastLocation != null) {
            LatLng latLng = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
            Marker marker = mapView.addMarker(
                    new MarkerOptions()
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                            .position(latLng)
                            .title("Charging from " + (lastStatus != null ? lastStatus.soc : "?") + "%"));
            markers.add(marker);
        }
    }

    // endregion

    // region collecting stats

    private void updateObdData(final int soc, final int temp1, final int temp2) {
        obd2SocView.setText(getString(R.string.battery_soc_obd2_value).replace("--", String.valueOf(soc)));
        obd2TempView.setText(getString(R.string.battery_temp_obd2_value).replace("--/--", temp1+"/"+temp2));
    }

    private void updateObdData(final String soc) {
        obd2SocView.setText(getString(R.string.battery_soc_obd2_value).replace("--", soc));
        obd2TempView.setText(getString(R.string.battery_temp_obd2_value));
    }

    public void onBleDataEvent(BleDataEvent event) {
        checkForChargeStops(event);
        lastStatus = event.getStatus();
        updateStats();
        updateValues(event.getSelectedUnit(), event.getStatus());
    }

    private void resetStats() {
        topSpeed = 0;
        chargeStopLengths.clear();
        lastChargeStopTimestamp = 0;
        charging = false;
        logChargeView.setText("Start Charge");
        calculatedDistance = 0;
        initialOdoValue = 0;
        initialBatteryValue = 0;
    }

    private void updateStats() {

        // show or hide obd2 data based on prefs
        boolean showObd2Data = prefs.getBoolean(getString(R.string.key_obd2_show_on_map), true);
        obd2DataView.setVisibility( showObd2Data ? VISIBLE : GONE);

        // BLE details
        boolean showBleData = prefs.getBoolean(getString(R.string.key_show_ble_data_map), true);
        bleDataView.setVisibility( showBleData ? VISIBLE : GONE);

        // bike specific details
        int lastBikeSpeed = lastStatus != null ? lastStatus.speed : 0;
        int currentUnitBike = lastStatus != null ? lastStatus.distanceUnit : DISTANCE_UNIT_KM;
        String unitValueBike = currentUnitBike == DISTANCE_UNIT_MI ? "mi" : "km";

        // calc consumption from BLE
        boolean showCalculatedConsumption = prefs.getBoolean(getString(R.string.key_show_calculated_consumption_map), true);
        if( showCalculatedConsumption)
            handleCalculatedConsumption();
        else
            hideCalculatedConsumption();

        int lastGpsSpeed = lastLocation != null ? Math.round(lastLocation.getSpeed() * MPS_TO_KPH) : 0;
        String valueMetric = getString(R.string.value_metric);
        int unitSystem = valueMetric.equals(prefs.getString(getString(R.string.key_unit_system), valueMetric)) ? DISTANCE_UNIT_KM : DISTANCE_UNIT_MI;
        String unitValueSystem = unitSystem == DISTANCE_UNIT_MI ? "mi" : "km";
        int averageSpeed = Math.round(calculateAverageSpeed() * MPS_TO_KPH);
        // register top speed
        if (lastGpsSpeed > topSpeed) {
            topSpeed = lastGpsSpeed;
        }
        if (unitSystem == DISTANCE_UNIT_MI) {
            // correct speeds for unit
            lastGpsSpeed *= KM_TO_MI;
            averageSpeed *= KM_TO_MI;
            topSpeed *= KM_TO_MI;
        }

        // GPS details
        boolean showGpsDetails = prefs.getBoolean(getString(R.string.key_show_gps_details), true);
        if( showGpsDetails ) {
            statsView.setText("Speed GPS: " + lastGpsSpeed + ", Avg: "
                            + averageSpeed + " (" + unitValueSystem + "), Bike: "
                                      + lastBikeSpeed + " (" + unitValueBike + ")");
            statsView.setVisibility(VISIBLE);
        } else {
            statsView.setVisibility(View.GONE);
        }
        if( massiveSpeedView != null )
            massiveSpeedView.setText(String.valueOf(lastGpsSpeed));

        // FIXME this is broken? triggers update of stats on data screen but no more subscribers active
        //EventBus.getDefault().post(new StatsEvent(currentUnitBike, unitSystem,
        //        lastGpsSpeed, lastBikeSpeed, averageSpeed, topSpeed, chargeStopLengths.size(),
        //        calculateTotalChargeTimeInMs(), calculatedDistance));
    }

    private void hideCalculatedConsumption() {
        calculatedConsumptionView.setVisibility(GONE);
    }

    private void handleCalculatedConsumption() {
        // bike specific details
        int currentUnitBike = lastStatus != null ? lastStatus.distanceUnit : DISTANCE_UNIT_KM;
        String unitValueBike = currentUnitBike == DISTANCE_UNIT_MI ? "mi" : "km";

        // average consumption from ODO and resBatteryEnergy values
        float averageConsumption = 0, coveredDistance = 0, consumedKwh = 0, rangeLeft = 0;
        if (lastStatus != null) {
            final float lastOdoValue = lastStatus.totalOdometer;
            final int lastBatteryValue = lastStatus.resBatteryEnergy;
            if (initialOdoValue == 0) initialOdoValue = lastOdoValue;
            if (initialBatteryValue == 0) initialBatteryValue = lastBatteryValue;
            // calculate how much kWh were used over what distance
            coveredDistance = lastOdoValue - initialOdoValue;
            consumedKwh = initialBatteryValue - lastBatteryValue;
            if (consumedKwh != 0 && coveredDistance != 0) {
                averageConsumption = consumedKwh / coveredDistance;
            }
            if( averageConsumption != 0 && lastBatteryValue != 0 ){
                rangeLeft = lastBatteryValue / averageConsumption;
            }
        }
        // display these values
        if (averageConsumption != 0 && averageConsumption != 0) {
            calculatedConsumptionView.setText(getString(R.string.calculated_consumption, coveredDistance, unitValueBike, consumedKwh,
                                                        averageConsumption, unitValueBike, rangeLeft, unitValueBike));
            calculatedConsumptionView.setVisibility(VISIBLE);
        } else {
            hideCalculatedConsumption();
        }
    }

    private long calculateTotalChargeTimeInMs() {
        long totalChargeTime = 0;
        for (long chargeSessionTime : chargeStopLengths) {
            totalChargeTime += chargeSessionTime;
        }
        return totalChargeTime;
    }

    private int calculateAverageSpeed() {
        // alternative option is to calculate average speed with a moving average
        // new_average = (n * old_average - x_forget + x_new) / n
        // Here, n is the number of data points, x_forget is the value you are "forgetting" and x_new is the latest value
        int totalSpeed = 0;
        int dataPoints = 0;
        for (Location location : mapPoints) {
            if (location.hasSpeed() && "gps".equals(location.getProvider())) {
                // only takes values from gps provider into account, values from bike are ignored for speed calculations
                totalSpeed += location.getSpeed();
                dataPoints++;
            }
        }
        return totalSpeed > 0 && dataPoints > 0 ? totalSpeed / dataPoints : 0;
    }

    private void updateValues(
            final int selectedUnit,
            final VehicleStatus currentStatus) {

        if (!isAdded()) {
            return;
        }

        // uses metric system from the bike itself
        reserveView.setText(String.format(Locale.getDefault(), "%,d Wh", currentStatus.resBatteryEnergy));
        consumptionView.setText(String.format(Locale.getDefault(), "%.1f %s", currentStatus.avgConsumption,
                Help.getConsumptionUnit(currentStatus.avgConsumptionUnit)));
        socView.setText(String.format(Locale.getDefault(), "%d%%", currentStatus.soc));
        if (selectedUnit == DISTANCE_UNIT_MI) {
            rangeView.setText(String.format(Locale.getDefault(), "%d %s",
                    Math.round(((double) currentStatus.range) * KM_TO_MI),
                    getString(R.string.unit_imperial_distance)));
        } else {
            rangeView.setText(
                    String.format(Locale.getDefault(), "%d %s", currentStatus.range,
                            getString(R.string.unit_metric_distance)));
        }
    }

    // endregion

}
