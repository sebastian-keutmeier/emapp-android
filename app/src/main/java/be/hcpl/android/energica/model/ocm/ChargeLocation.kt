package be.hcpl.android.energica.model.ocm

import androidx.annotation.Keep

@Keep
class ChargeLocation(
    val AddressInfo: AddressInfo,
    val Connections: List<Connection>
)
