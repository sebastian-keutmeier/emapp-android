package be.hcpl.android.energica.ui.webview

import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import be.hcpl.android.energica.R

class WebViewFragment : Fragment() {

    private lateinit var dashboardViewModel: WebViewViewModel

    private lateinit var progressBarView: ProgressBar
    private lateinit var webView: WebView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        dashboardViewModel =
            ViewModelProvider(this).get(WebViewViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_webview, container, false)

        progressBarView = root.findViewById(R.id.progress)
        webView = root.findViewById(R.id.webview)

        // TODO use viewModel instead for passing info
        loadContent(arguments?.getString(KEY_CONTENT)?: "https://fotoleer.wordpress.com/electric-motorcycles")

        dashboardViewModel.text.observe(viewLifecycleOwner, Observer {
            //textView.text = it
        })
        return root
    }

    private fun loadContent(contentUrl: String){
        webView.webViewClient = object : WebViewClient() {

            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                view.visibility = View.INVISIBLE
                progressBarView.visibility = View.VISIBLE
            }

            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                view.visibility = View.VISIBLE
                progressBarView.visibility = View.INVISIBLE
            }

        }
        webView.settings.javaScriptEnabled = true
        val settings = webView.settings
        settings.domStorageEnabled = true

        webView.loadUrl(contentUrl)

    }

    companion object {

        const val KEY_CONTENT = "contentReference"

        private const val VALUE_BLOG = "https://fotoleer.wordpress.com/electric-motorcycles"
        private const val VALUE_VIDEO = "https://www.youtube.com/channel/UCMB-LNAuNJTAsFZgakBkpxQ/Videos"

        fun argsForBlog() = Bundle().apply { putString(KEY_CONTENT, VALUE_BLOG) }
        fun argsForVideo() = Bundle().apply { putString(KEY_CONTENT, VALUE_VIDEO) }

        fun fragmentForBlog() = WebViewFragment().apply { arguments = argsForBlog() }
        fun fragmentForVideo() = WebViewFragment().apply { arguments = argsForVideo() }

    }

}
