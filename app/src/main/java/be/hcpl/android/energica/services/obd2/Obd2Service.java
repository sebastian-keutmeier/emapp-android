package be.hcpl.android.energica.services.obd2;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.github.pires.obd.commands.ObdCommand;
import com.github.pires.obd.commands.protocol.ObdResetCommand;
import com.github.pires.obd.exceptions.UnsupportedCommandException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import be.hcpl.android.energica.Obd2Activity;
import be.hcpl.android.energica.R;
import be.hcpl.android.energica.config.Energica200Command;
import be.hcpl.android.energica.config.Energica201Command;
import be.hcpl.android.energica.config.ObdConfig;
import be.hcpl.android.energica.config.ObdEnergicaConfig;
import be.hcpl.android.energica.config.ObdGenericEVConfig;
import be.hcpl.android.energica.helpers.ExportData;
import be.hcpl.android.energica.services.evmonitor.EvMonitorRepo;
import be.hcpl.android.energica.services.obd2.ObdCommandJob.ObdCommandJobState;

import static be.hcpl.android.energica.helpers.Const.BROADCAST_EVENT;
import static be.hcpl.android.energica.helpers.Const.BROADCAST_FILTER;
import static be.hcpl.android.energica.helpers.Const.BROADCAST_OBD2_DATA;
import static be.hcpl.android.energica.helpers.Const.BROADCAST_OBD2_DATA_CHARGE;
import static be.hcpl.android.energica.helpers.Const.BROADCAST_OBD2_GENERIC_DATA;
import static be.hcpl.android.energica.helpers.Const.BROADCAST_OBD2_RAW;
import static be.hcpl.android.energica.helpers.Const.BROADCAST_OBD2_STATE;
import static be.hcpl.android.energica.helpers.Const.OBD2_CHARGE_STATE;
import static be.hcpl.android.energica.helpers.Const.OBD2_CURRENT_AC;
import static be.hcpl.android.energica.helpers.Const.OBD2_CURRENT_DC;
import static be.hcpl.android.energica.helpers.Const.OBD2_JOB_STATE;
import static be.hcpl.android.energica.helpers.Const.OBD2_NAME;
import static be.hcpl.android.energica.helpers.Const.OBD2_RAW_DATA;
import static be.hcpl.android.energica.helpers.Const.OBD2_RESULT;
import static be.hcpl.android.energica.helpers.Const.OBD2_SOC;
import static be.hcpl.android.energica.helpers.Const.OBD2_TEMP1;
import static be.hcpl.android.energica.helpers.Const.OBD2_TEMP2;
import static be.hcpl.android.energica.helpers.Const.OBD2_VOLT;
import static java.lang.System.currentTimeMillis;

/**
 * This service is primarily responsible for establishing and maintaining a
 * permanent connection between the device where the application runs and a more
 * OBD Bluetooth interface.
 * <p/>
 * Secondarily, it will serve as a repository of ObdCommandJobs and at the same
 * time the application state-machine.
 */
public class Obd2Service extends Service {

    public static final String SELECTED_DEVICE_KEY = "selectedDevice";
    public static final int NOTIFICATION_ID = 123;
    public static final int DELAY_MILLIS = 4_000; // TODO move update interval to config
    public static final int RECONNECT_DELAY_MILLIS = 10_000; // TODO move update interval to config
    public static final String NOTIFICATION_CHANNEL_ID = "obd2-channel";
    public static final String NOTIFICATION_CHANNEL_NAME = "OBD2 Service";
    public static final String NOTIFICATION_TEXT = "OBD2 Service running";

    private SharedPreferences prefs;
    private EvMonitorRepo evMonitorRepo;

    private BluetoothDevice device = null;
    private BluetoothSocket connectionSocket = null;

    // queue definition
    private Long queueCounter = 0L;
    private BlockingQueue<ObdCommandJob> jobsQueue = new LinkedBlockingQueue<>();

    // connection and service state
    private boolean running = false;
    private boolean inDebugMode = false;
    private boolean shouldReconnect = true;

    // region broadcast

    private void sendStateUpdate(final ObdCommandJobState jobState){
        final Bundle bundle = new Bundle();
        bundle.putSerializable(OBD2_JOB_STATE, jobState);
        sendBroadcast(BROADCAST_OBD2_STATE, bundle);
    }

    private void sendRawData(final String rawData){
        evMonitorRepo.pushEnergicaRawData(rawData);
        final Bundle bundle = new Bundle();
        bundle.putString(OBD2_RAW_DATA, rawData);
        sendBroadcast(BROADCAST_OBD2_RAW, bundle);
    }

    private void sendEnergicaData(int soh, int soc, int voltage, int currentAc, int currentDc, int temp1, int temp2){
        evMonitorRepo.pushEnergicaObd2Data(temp1, temp2, soh, soc, currentAc, currentDc);
        final Bundle bundle = new Bundle();
        bundle.putInt(OBD2_SOC, soc);
        bundle.putInt(OBD2_VOLT, voltage);
        bundle.putInt(OBD2_CURRENT_AC, currentAc);
        bundle.putInt(OBD2_CURRENT_DC, currentDc);
        bundle.putInt(OBD2_TEMP1, temp1);
        bundle.putInt(OBD2_TEMP2, temp2);
        sendBroadcast(BROADCAST_OBD2_DATA, bundle);
        checkNotificationUpdate(soc, temp1, temp2, currentAc, currentDc);
    }

    private void sendEnergicaChargeState(int chargeState){
        final Bundle bundle = new Bundle();
        bundle.putInt(OBD2_CHARGE_STATE, chargeState);
        sendBroadcast(BROADCAST_OBD2_DATA_CHARGE, bundle);
        // TODO evMonitorRepo.pushEnergicaObd2Data(temp1, temp2, soh, soc, currentAc, currentDc);
        // TODO checkNotificationUpdate(soc, temp1, temp2, currentAc, currentDc);
    }

    private void sendGenericData(final ObdCommand command){
        evMonitorRepo.pushGenericObd2Data(command);
        final Bundle bundle = new Bundle();
        bundle.putString(OBD2_NAME, command.getName());
        bundle.putString(OBD2_RESULT, command.getFormattedResult());
        sendBroadcast(BROADCAST_OBD2_GENERIC_DATA, bundle);
    }

    private void sendBroadcast(String event, Bundle bundle) {
        Intent intent = new Intent(BROADCAST_FILTER);
        intent.putExtra(BROADCAST_EVENT, event);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        sendBroadcast(intent);
    }

    // endregion 

    // region logging

    private boolean logging = false;
    private String fileName;

    private void log(final String message){
        ExportData.Companion.log(getApplicationContext(), message); // logs to system file for in app use
    }

    public void startLogging(){
        if( !logging && prefs.getBoolean(getString(R.string.key_log_to_file), false) ){
            fileName = "obd-data-"+ExportData.Companion.getTimeFormatFile().format(currentTimeMillis())+".txt";
            logging = true;
        }
    }

    public void stopLogging(){
        logging = false;
    }

    private void appendToFile(String data) {
        try {
            String timestamp = ExportData.Companion.getTimeFormatLogStatement().format(currentTimeMillis());
            File filePath = getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
            FileWriter fileWriter = new FileWriter(new File(filePath, fileName), true);
            fileWriter.write(timestamp + " " + data + System.lineSeparator());
            fileWriter.flush();
            fileWriter.close();
        } catch (Exception e) {
            log("OBD2 - File write failed: $e");
        }
    }

    // endregion

    // region keep service running in background mode with notification to inform user

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        log("OBD2 - onStartCommand");
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    private Notification.Builder builder;
    private NotificationCompat.Builder builderCompat;
    private NotificationManager notificationManager;

    @RequiresApi(api = Build.VERSION_CODES.O)
    private Notification getNotification() {
        final PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, new Intent(getApplicationContext(), Obd2Activity.class), 0);
        NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, NOTIFICATION_CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
        if (notificationManager == null)
             notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.createNotificationChannel(channel);
        if (builder == null) {
            builder = new Notification.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID);
        }
        builder.setSmallIcon(R.drawable.ic_action_settings_input_hdmi)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(NOTIFICATION_TEXT)
                .setContentIntent(contentIntent);
        return builder.build();
    }

    private Notification getCompatNotification() {
        final PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, new Intent(getApplicationContext(), Obd2Activity.class), 0);
        if (notificationManager == null)
            notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (builderCompat == null)
            builderCompat = new NotificationCompat.Builder(getApplicationContext());
        builderCompat.setSmallIcon(R.drawable.ic_action_settings_input_hdmi)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(NOTIFICATION_TEXT)
                .setContentIntent(contentIntent)
                .setAutoCancel(false)
                .setOngoing(true);
        return builderCompat.build();
    }

    private int lastTemp1, lastTemp2, lastCurrentAc, lastCurrentDc, lastSoc;

    private void checkNotificationUpdate(int soc, int temp1, int temp2, int currentAC, int currentDC){
        // preferences
        boolean showTempNotification = prefs.getBoolean(getString(R.string.key_obd2_show_temp_notification), true);
        boolean showCurrentNotification = prefs.getBoolean(getString(R.string.key_obd2_show_current_notification), false);
        boolean showSocNotification = prefs.getBoolean(getString(R.string.key_obd2_show_soc_notification), false);

        // always provide text within notification for all values
        String text = "Battery Temp: " + temp1 + "/" + temp2 + "°C";
        text += " | Charge Current: " + currentAC + "/" + currentDC + "A";
        text += " | SOC: "+soc+"%";

        // only triggering new notification takes chekcs config toggles
        boolean updateNotification = false;
        // only update when temps have changed
        updateNotification = showTempNotification && (updateNotification || temp1 != lastTemp1 || temp2 != lastTemp2);
        // charge current values
        updateNotification = showCurrentNotification && (updateNotification || currentAC != lastCurrentAc || currentDC != lastCurrentDc);
        // check for changed SOC
        updateNotification = showSocNotification && (updateNotification || Math.abs(soc-lastSoc) >= 5);

        // show if needed
        if (updateNotification)
            updateNotification(text);

        // update last values for next checks
        lastTemp1 = temp1;
        lastTemp2 = temp2;
        lastCurrentAc = currentAC;
        lastCurrentDc = currentDC;
        lastSoc = soc;
    }

    private void updateNotification(String newText) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setContentText(newText);
            notificationManager.notify(NOTIFICATION_ID, builder.build());
        } else {
            builderCompat.setContentText(newText);
            notificationManager.notify(NOTIFICATION_ID, builderCompat.build());
        }
    }

    private void cancelNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            stopForeground(true);
        } else {
            notificationManager.cancel(NOTIFICATION_ID);
        }
    }

    // endregion

    // region lifecycle

    @Override
    public void onCreate() {
        super.onCreate();
        log("OBD2 - Creating service..");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForeground(NOTIFICATION_ID, getNotification());
        } else {
            notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.notify(NOTIFICATION_ID, getCompatNotification());
        }
        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        evMonitorRepo = new EvMonitorRepo(getApplicationContext(), prefs);
        shouldReconnect = prefs.getBoolean(getString(R.string.key_obd2_autoconnect), true);
        startLogging();
        startService();
        log("OBD2 - Service created.");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        log("OBD2 - Destroying service...");
        evMonitorRepo.flushData();
        stopService();
        stopLogging();
        cancelNotification();
        log("OBD2 - Service destroyed.");
    }

    @SuppressLint("MissingPermission")
    private void startService() {
        log("OBD2 - Starting service..");
        running = true;
        // in debug mode just continue
        inDebugMode = prefs.getBoolean(getApplicationContext().getString(R.string.key_general_debug_data), false);
        if( inDebugMode ){
            log("OBD2 - running OBD2 service in debug mode");
            try { startObdConnection(); } catch(Exception e) { /* ignore exceptions */ }
            return;
        }
        // get the remote Bluetooth device
        final String remoteDevice = prefs.getString(SELECTED_DEVICE_KEY, null);
        if (remoteDevice == null || "".equals(remoteDevice)) {
            Toast.makeText(getApplicationContext(), getString(R.string.text_bluetooth_nodevice), Toast.LENGTH_LONG).show();
            // log error
            log("OBD2 - No Bluetooth device has been selected.");
            // kill this service gracefully
            sendStateUpdate(ObdCommandJobState.BT_FAILED);
            stopService();
        } else {
            final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
            device = btAdapter.getRemoteDevice(remoteDevice);
            log("OBD2 - Stopping Bluetooth discovery.");
            btAdapter.cancelDiscovery();
            try {
                startObdConnection();
            } catch (Exception e) {
                log("OBD2 - There was an error while establishing connection. -> " + e.getMessage());
                // in case of failure, stop this service.
                sendStateUpdate(ObdCommandJobState.BT_FAILED);
                if( shouldReconnect ) {
                    new Handler().postDelayed(runnable, RECONNECT_DELAY_MILLIS);
                } else {
                    stopService();
                }
            }
        }
    }

    private void startObdConnection() {
        if (inDebugMode) {
            log("OBD2 - running in debug mode continued, schedule fake jobs from thread");
            new Handler().post(runnable);
            return;
        }
        // start a connection in another thread here
        new Thread(() -> {
            try {
                log("OBD2 - Starting OBD bluetooth connection..");
                // FIXED this was causing thread locks on UI
                // https://developer.android.com/guide/background/threading#using-handlers
                connectionSocket = BluetoothManager.connect(device);
                sendStateUpdate(ObdCommandJobState.BT_CONNECTED);
                new Handler(Looper.getMainLooper()).post(this::continueWithConnection);
            } catch (Exception e2) {
                log("OBD2 - There was an error while establishing Bluetooth connection -> "+ e2.getMessage());
                sendStateUpdate(ObdCommandJobState.BT_FAILED);
                if( !shouldReconnect ) {
                    new Handler(Looper.getMainLooper()).post(this::stopService); // removed for auto reconnect, otherwise we can't recover
                } else {
                    new Handler(Looper.getMainLooper()).postDelayed(runnable, RECONNECT_DELAY_MILLIS);
                }
            }
        }).start();
    }

    private void continueWithConnection(){
        // Let's configure the connection.
        log("OBD2 - Queueing jobs for connection configuration..");
        queueJob(new ObdCommandJob(new ObdResetCommand()));

        // Below is to give the adapter enough time to reset before sending the commands,
        // otherwise the first startup commands could be ignored.
        try { Thread.sleep(500); } catch (InterruptedException e) { e.printStackTrace(); }

        // moved default commands to ObdConfig also (for single execution)
        queueCounter = 0L;
        log("OBD2 - Initialization jobs queued.");

        // queue jobs
        for (ObdCommand Command : getObdConfig().getSingleCommands()) {
            queueJob(new ObdCommandJob(Command));
        }
        //new Handler().post(runnable);
        new Handler().postDelayed(runnable, RECONNECT_DELAY_MILLIS);
    }

    public void stopService() {
        log("OBD2 - Stopping service..");
        running = false;
        sendStateUpdate(ObdCommandJobState.BROKEN_PIPE);
        try {
            cancelNotification();
            jobsQueue.clear();
            if( !inDebugMode && connectionSocket != null ) connectionSocket.close(); // close socket
        } catch (Exception e) {
            log("OBD2 - "+e.getMessage());
        }
        stopSelf(); // kill service
    }

    // endregion

    // region queue handling

    public boolean queueEmpty() {
        return jobsQueue.isEmpty();
    }

    private final Runnable runnable = new Runnable() {
        public void run() {
            // check for connection so we can reconnect if enabled
            if( running && !inDebugMode && (connectionSocket == null || !connectionSocket.isConnected()) ){
                sendStateUpdate(ObdCommandJobState.BT_FAILED); // notify connection is gone
                jobsQueue.clear(); // remove pending jobs
                connectionSocket = null;
                if( shouldReconnect ){
                    // attempt to reconnect on each iteration, the reconnect will trigger this again
                    startObdConnection(); // no need to refresh config, only restart OBD and BT conn.
                } else {
                    stopService(); // no need to continue, stop service completely
                }
            } else {
                // connection was still fine at this pointso handle commands
                if (queueEmpty()) queueCommands(); // re-populate queue when needed
                executeQueue();
                // run again in period defined in preferences as long as service is running
                if( running ) new Handler().postDelayed(runnable, DELAY_MILLIS);
            }
        }
    };

    private void queueCommands() {
        for (ObdCommand Command : getObdConfig().getRepeatableCommands()) {
            queueJob(new ObdCommandJob(Command));
        }
    }

    private ObdConfig getObdConfig() {
        if( prefs.getBoolean(getString(R.string.key_obd2_use_generic), false)) {
            return ObdGenericEVConfig.INSTANCE;
        } else {
            return ObdEnergicaConfig.INSTANCE;
        }
    }

    public void queueJob(ObdCommandJob job) {
        // This is a good place to enforce the imperial units option
        job.getCommand().useImperialUnits(false);
        job.setId(++queueCounter); // generate a seq ID for each job
        try {
            jobsQueue.put(job);
        } catch (InterruptedException e) {
            job.setState(ObdCommandJobState.QUEUE_ERROR);
            log("OBD2 - Failed to queue job.");
        }
    }

    protected void executeQueue() {
        while (!jobsQueue.isEmpty() && running) {
            ObdCommandJob job = null;
            try {
                job = jobsQueue.take();
                log("OBD2 - Taking job[" + job.getId() + "]"); // log job
                if (job.getState().equals(ObdCommandJobState.NEW)) {
                    //log("OBD2 - Job state is NEW. Run it..");
                    job.setState(ObdCommandJobState.RUNNING);
                    if( inDebugMode ){
                        // generates some dummy data for testing
                        if( job.getCommand() instanceof Energica200Command)
                            ((Energica200Command)job.getCommand()).generateDummyData();
                        job.setState(ObdCommandJobState.FINISHED);
                    } else if (connectionSocket.isConnected()) {
                        // this is where the job is executed
                        job.getCommand().run(connectionSocket.getInputStream(), connectionSocket.getOutputStream());
                    } else {
                        job.setState(ObdCommandJobState.EXECUTION_ERROR);
                        log("OBD2 - Can't run command on a closed socket.");
                    }
                } else {
                    // log not new job
                    job.setState(ObdCommandJobState.FINISHED);
                    log("OBD2 - Job state was not new, so it shouldn't be in queue. BUG ALERT!");
                }
            } catch (InterruptedException i) {
                log("OBD2 - interrupted exception found"); // keeps service running
            } catch (UnsupportedCommandException u) {
                if (job != null) {
                    job.setState(ObdCommandJobState.NOT_SUPPORTED);
                }
                log("OBD2 - Command not supported. -> " + u.getMessage());
            } catch (IOException io) {
                if(io.getMessage().contains("Broken pipe"))
                    job.setState(ObdCommandJobState.BROKEN_PIPE);
                else
                    job.setState(ObdCommandJobState.EXECUTION_ERROR);
                connectionSocket = null; // added for auto reconnect feature
                log("OBD2 - IO error. -> " + io.getMessage());
            } catch (Exception e) {
                if (job != null) {
                    job.setState(ObdCommandJobState.EXECUTION_ERROR);
                }
                log("OBD2 - Failed to run command. -> " + e.getMessage());
            }

            if (job != null) {
                // log
                String cmdResult = job.getCommand().getFormattedResult();
                appendToFile(job.getCommand().getName()+ " "+cmdResult);

                // publish state update
                sendStateUpdate(job.getState());

                // publish data to views and remote
                final ObdCommand command = job.getCommand();
                if( command instanceof Energica200Command) {
                    final Energica200Command eCommand = (Energica200Command) command;
                    try {
                        // prevent crashes here
                        sendEnergicaData(eCommand.getSOH(), eCommand.getSOC(), eCommand.getVoltage(), eCommand.getAcCurrent(), eCommand.getDcCurrent(), eCommand.getTemp1(),
                                eCommand.getTemp2());
                    } catch (Exception e) {
                        log("OBD2 - failed to parse OBDII data " + e.getMessage());
                    }
                    // for now always show raw data on screen also
                    sendRawData(eCommand.getRawResult());
                } else if ( command instanceof Energica201Command){
                    final Energica201Command eCommand = (Energica201Command) command;
                    try {
                        // prevent crashes here
                        sendEnergicaChargeState(eCommand.getChargeState());
                    } catch (Exception e) {
                        log("OBD2 - failed to parse OBDII data " + e.getMessage());
                    }
                    // for now always show raw data on screen also
                    sendRawData(eCommand.getRawResult());
                } else {
                    sendGenericData(command);
                }

            }
        }
    }

    // endregion

}
