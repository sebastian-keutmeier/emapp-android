package be.hcpl.android.energica

import android.app.AlertDialog
import android.content.*
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.res.ResourcesCompat
import androidx.viewpager.widget.ViewPager
import be.hcpl.android.energica.event.ConnectionRejectedEvent
import be.hcpl.android.energica.event.ResetConnectionEvent
import be.hcpl.android.energica.helpers.Const
import be.hcpl.android.energica.helpers.Const.BLE_EVENT
import be.hcpl.android.energica.helpers.Const.BLE_GENERIC_EVENT
import be.hcpl.android.energica.helpers.Const.BLE_STATE
import be.hcpl.android.energica.helpers.Const.BROADCAST_BLE_EVENT
import be.hcpl.android.energica.helpers.Const.BROADCAST_BLE_GENERIC_EVENT
import be.hcpl.android.energica.helpers.Const.BROADCAST_BLE_STATE
import be.hcpl.android.energica.helpers.Const.SETTINGS_DEVICE
import be.hcpl.android.energica.helpers.Const.STATE_NOT_CONNECTED
import be.hcpl.android.energica.helpers.ExportData.Companion.log
import be.hcpl.android.energica.helpers.Help.getSharedPrefs
import be.hcpl.android.energica.helpers.Help.isServiceRunning
import be.hcpl.android.energica.helpers.Help.withColor
import be.hcpl.android.energica.helpers.Help.withStateColor
import be.hcpl.android.energica.pagers.ViewPagerAdapterMain
import be.hcpl.android.energica.services.ble.BleService
import be.hcpl.android.energica.services.ble.ConnectionManager
import be.hcpl.android.energica.services.gps.GpsService
import be.hcpl.android.energica.services.obd2.Obd2Service
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

class BleEnergicaActivity : AppCompatActivity(R.layout.activity_ble) {

    private lateinit var prefs: SharedPreferences

    // toolbar state icons
    private lateinit var gpsStateView: ImageView
    private lateinit var bleStateView: ImageView
    private lateinit var obdStateView: ImageView

    private lateinit var connectView: TextView

    // region service connection

    private val serviceConnection: ServiceConnection = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName, binder: IBinder) {
            startService()
            setStateRunning()
        }

        override fun onServiceDisconnected(className: ComponentName) {
            stopService()
        }
    }

    private fun startService() {
        val intent = Intent(this@BleEnergicaActivity, BleService::class.java)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent)
            bindService(intent, serviceConnection, BIND_AUTO_CREATE)
        } else {
            startService(intent)
        }
    }

    private fun stopService() {
        if (serviceConnection != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            try {
                unbindService(serviceConnection)
            } catch (e: Exception) { /* ignore */
            }
        }
        // stop background service for GPS location
        val intent = Intent(this@BleEnergicaActivity, BleService::class.java)
        stopService(intent)
        setStateStopped()
    }

    private fun setStateRunning() {
        // allows for stopping service
        connectView.text = "stop"
        connectView.setOnClickListener { stopService() }
    }

    private fun setStateStopped() {
        // allows for starting service
        connectView.text = "start"
        connectView.setOnClickListener { startService() }
    }

    // endregion

    // region broadcast messages from service

    private val broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            val event = intent.getStringExtra(Const.BROADCAST_EVENT)
            if (BROADCAST_BLE_STATE == event) {
                // update state on top of view and in toolbar
                updateState(intent.getIntExtra(BLE_STATE, STATE_NOT_CONNECTED))
                updateServiceStates()
            } else if (BROADCAST_BLE_EVENT == event) {
                // forwards this data to the fragments using event bus for now
                intent.getSerializableExtra(BLE_EVENT)?.let{ EventBus.getDefault().post(it) }
            } else if (BROADCAST_BLE_GENERIC_EVENT == event) {
                // temp solution posting generic events like resetConnection and more
                intent.getSerializableExtra(BLE_GENERIC_EVENT)?.let{ EventBus.getDefault().post(it) }
            }
        }
    }

    // endregion

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        prefs = getSharedPrefs(this)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        val titleView = findViewById<TextView>(R.id.toolbar_title)
        titleView.text = getString(R.string.ble_connection)

        statusView = findViewById(R.id.status_value)
        statusView.visibility = View.VISIBLE

        // show configured device
        findViewById<TextView>(R.id.selected_device).text = prefs.getString(SETTINGS_DEVICE, "N/A")

        // some state indications
        gpsStateView = findViewById(R.id.gps_state)
        gpsStateView.visibility = View.VISIBLE
        gpsStateView.setColorFilter(ResourcesCompat.getColor(resources, R.color.dark_white, null))
        bleStateView = findViewById(R.id.ble_state)
        bleStateView.visibility = View.VISIBLE
        bleStateView.setColorFilter(ResourcesCompat.getColor(resources, R.color.dark_white, null))
        obdStateView = findViewById(R.id.obd_state)
        obdStateView.visibility = View.VISIBLE
        obdStateView.setColorFilter(ResourcesCompat.getColor(resources, R.color.dark_white, null))

        connectView = findViewById(R.id.button_connect)
        connectView.visibility = View.VISIBLE

        connectView.setOnClickListener { startService(); }

        // fragment pager setup
        val adapter = ViewPagerAdapterMain(supportFragmentManager)
        val viewPager = findViewById<ViewPager>(R.id.home_viewpager);
        viewPager.adapter = adapter;
    }

    // region menu

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.close_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.close -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    // endregion

    // region lifecycle handling

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onResume() {
        super.onResume()
        // register broadcast receiver
        val filter = IntentFilter(Const.BROADCAST_FILTER)
        registerReceiver(broadcastReceiver, filter)
        updateServiceStates()
    }

    override fun onPause() {
        unregisterReceiver(broadcastReceiver)
        super.onPause()
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    // endregion

    // region more connection handling

    @Subscribe
    fun onResetConnection(event: ResetConnectionEvent?) {
        // when this was reset from the settings we should also stop the service
        log("BLE service stopped since connection was reset on phone")
        stopService()
    }

    @Subscribe
    fun connectionRejected(event: ConnectionRejectedEvent) {
        log("connection rejected by bike")
        val builder = AlertDialog.Builder(this, R.style.AlertDialogStyle)
        builder.setMessage(R.string.ConnRejected)
            .setCancelable(false)
        builder.create().show()
    }

    // endregion

    fun setChgTermination() {
        // only works because it's a singleton
        ConnectionManager.getInstance().setChargeTermination()
    }

    fun setResetTrip() {
        // only works because it's a singleton
        ConnectionManager.getInstance().setResetTrip()
    }

    private fun log(message: String) {
        log(applicationContext, message)
    }

    // region show connection state

    private lateinit var statusView: TextView

    private fun updateServiceStates() {
        val gpsServiceRunning = isServiceRunning(applicationContext, GpsService::class.java)
        val bleServiceRunning = isServiceRunning(applicationContext, BleService::class.java)
        val obd2ServiceRunning = isServiceRunning(applicationContext, Obd2Service::class.java)
        gpsStateView.setColorFilter(withStateColor(applicationContext, gpsServiceRunning))
        bleStateView.setColorFilter(withStateColor(applicationContext, bleServiceRunning))
        obdStateView.setColorFilter(withStateColor(applicationContext, obd2ServiceRunning))
        if( bleServiceRunning ) setStateRunning() else setStateStopped()
    }

    private fun updateState(state: Int) {
        // overrule when service state is not running
        if( !isServiceRunning(applicationContext, BleService::class.java) ){
            statusView.setText(R.string.NotRunningState)
            statusView.setTextColor(withColor(applicationContext, R.color.colorAccent))
        } else if (state == Const.STATE_SEARCHING) {
            statusView.setText(R.string.searchingState)
            statusView.setTextColor(withColor(applicationContext, R.color.colorPrimary))
        } else if (state == STATE_NOT_CONNECTED) {
            statusView.setText(R.string.NotConnectedState)
            statusView.setTextColor(withColor(applicationContext, R.color.colorAccent))
        } else if (state == Const.STATE_IDLE) {
            statusView.setText(R.string.IdleState)
            statusView.setTextColor(withColor(applicationContext, R.color.fluo_green))
        } else if (state == Const.STATE_CHARGE) {
            statusView.setText(R.string.ChargeState)
            statusView.setTextColor(withColor(applicationContext, R.color.fluo_green))
        } else if (state == Const.STATE_RUN) {
            statusView.setText(R.string.RunState)
            statusView.setTextColor(withColor(applicationContext, R.color.fluo_green))
        } else if (state == Const.STATE_ERROR) {
            statusView.setText(R.string.FaultState)
            statusView.setTextColor(withColor(applicationContext, R.color.colorAccent))
        }
    }

    // endregion

}