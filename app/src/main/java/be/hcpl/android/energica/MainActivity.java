package be.hcpl.android.energica;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.jetbrains.annotations.NotNull;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import be.hcpl.android.energica.event.GpsStateEvent;
import be.hcpl.android.energica.fragments.DashboardFragment;
import be.hcpl.android.energica.helpers.Const;
import be.hcpl.android.energica.helpers.ExportData;
import be.hcpl.android.energica.helpers.Help;
import be.hcpl.android.energica.services.ble.BleService;
import be.hcpl.android.energica.services.gps.GpsService;
import be.hcpl.android.energica.services.obd2.Obd2Service;
import be.hcpl.android.energica.ui.range.RangeFragment;
import be.hcpl.android.energica.ui.webview.WebViewFragment;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class MainActivity extends AppCompatActivity {

    // handle all the app permission requrests here in one place
    private final static int REQUEST_ENABLE_BT = 101;
    private static final int PERMISSION_REQUEST_LOCATION = 102;
    private static final int PERMISSION_REQUEST_STORAGE = 103;

    private ImageView gpsStateView;
    private ImageView bleStateView;
    private ImageView obdStateView;

    private void log(String message) {
        ExportData.Companion.log(getApplicationContext(), message);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);

        final Toolbar toolbar = findViewById(R.id.home_topbar);
        setSupportActionBar(toolbar);

        // this initial app view is responsible for checking all permissions
        checkStoragePermissions();
        checkBluetoothAndLocation();

        // state indications
        gpsStateView = findViewById(R.id.gps_state);
        gpsStateView.setVisibility(View.VISIBLE);
        gpsStateView.setColorFilter(Help.withColor(getApplicationContext(), R.color.dark_white));
        gpsStateView.setOnClickListener(view -> toggleService(GpsService.class) );

        bleStateView = findViewById(R.id.ble_state);
        bleStateView.setVisibility(View.VISIBLE);
        bleStateView.setColorFilter(Help.withColor(getApplicationContext(), R.color.dark_white));
        bleStateView.setOnClickListener(view -> toggleService(BleService.class) );

        obdStateView = findViewById(R.id.obd_state);
        obdStateView.setVisibility(View.VISIBLE);
        obdStateView.setColorFilter(Help.withColor(getApplicationContext(), R.color.dark_white));
        obdStateView.setOnClickListener(view -> toggleService(Obd2Service.class) );

        // load map vew for gps functionality
        switchFragment(dashboardFragment);

        // handle navigation here
        findViewById(R.id.nav_dashboard).setOnClickListener(v -> switchFragment(dashboardFragment));
        findViewById(R.id.nav_range).setOnClickListener(v -> switchFragment(rangeFragment));
        findViewById(R.id.nav_blog).setOnClickListener(v -> switchFragment(blogFragment));
        findViewById(R.id.nav_video).setOnClickListener(v -> switchFragment(videoFragment));
    }

    private Fragment dashboardFragment = new DashboardFragment();
    private Fragment rangeFragment = new RangeFragment();
    private Fragment blogFragment = WebViewFragment.Companion.fragmentForBlog();
    private Fragment videoFragment = WebViewFragment.Companion.fragmentForVideo();

    private void switchFragment(Fragment fragment){
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
    }

    // region broadcast messages from service for state handling

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            updateServiceStates();
        }
    };

    private boolean gpsServiceRunning;
    private boolean bleServiceRunning;
    private boolean obd2ServiceRunning;

    private void updateServiceStates() {
        gpsServiceRunning = Help.isServiceRunning(getApplicationContext(), GpsService.class);
        bleServiceRunning = Help.isServiceRunning(getApplicationContext(), BleService.class);
        obd2ServiceRunning = Help.isServiceRunning(getApplicationContext(), Obd2Service.class);
        gpsStateView.setColorFilter(Help.withStateColor(getApplicationContext(), gpsServiceRunning));
        bleStateView.setColorFilter(Help.withStateColor(getApplicationContext(), bleServiceRunning));
        obdStateView.setColorFilter(Help.withStateColor(getApplicationContext(), obd2ServiceRunning));
    }

    private void toggleService(Class serviceClass) {
        if (serviceClass == GpsService.class) {
            if (gpsServiceRunning) {
                disableGpsLocationUpdates();
            } else {
                enableGpsLocationUpdates();
            }
        } else if (serviceClass == BleService.class) {
            if (bleServiceRunning) {
                stopService(BleService.class);
            } else {
                startService(BleService.class);
            }
        } else if (serviceClass == Obd2Service.class) {
            if (obd2ServiceRunning) {
                stopService(Obd2Service.class);
            } else {
                startService(Obd2Service.class);
            }
        }
    }

    // endregion

    // region menu

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.app_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.start_tracking:
                enableGpsLocationUpdates();
                log("started tracking from menu");
                return true;
            case R.id.stop_tracking:
                disableGpsLocationUpdates();
                log("tracking stopped from menu");
                return true;
            case R.id.ble:
                startActivity(new Intent(this, BleEnergicaActivity.class));
                return true;
            case R.id.obd2:
                startActivity(new Intent(this, Obd2Activity.class));
                return true;
            case R.id.settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            case R.id.logs:
                startActivity(new Intent(this, AppLogsActivity.class));
                return true;
            case R.id.about:
                startActivity(new Intent(this, AboutActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // endregion

    // region lifecycle handling

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public void onResume() {
        super.onResume();
        // connect to service for broadcast messages
        // register broadcast receiver
        IntentFilter filter = new IntentFilter(Const.BROADCAST_FILTER);
        registerReceiver(broadcastReceiver, filter);
        // update state of services
        updateServiceStates();
    }

    @Override
    protected void onPause() {
        unregisterReceiver(broadcastReceiver);
        super.onPause();
    }

    // endregion

    // region permission handling

    @SuppressLint("MissingPermission")// it's in the manifest
    private void checkBluetoothAndLocation() {
        final BluetoothManager btManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        final BluetoothAdapter btAdapter = btManager.getAdapter();
        if (btAdapter != null && !btAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            log("bluetooth disabled on device, check settings");
        }
        // Make sure we have access coarse location enabled, if not, prompt the user to enable it
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(ACCESS_FINE_LOCATION) != PERMISSION_GRANTED) {
                log("bluetooth permission (ACCESS_FINE_LOCATION) requested");
                final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogStyle);
                builder.setTitle("This app needs location access");
                builder.setMessage("Please grant location access so this app can detect peripherals.");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(
                        dialog -> requestPermissions(new String[]{ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_LOCATION));
                builder.show();
            }
        }
    }

    private void checkStoragePermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(WRITE_EXTERNAL_STORAGE) != PERMISSION_GRANTED
                    || checkSelfPermission(READ_EXTERNAL_STORAGE) != PERMISSION_GRANTED) {

                log("storage permission (WRITE_EXTERNAL_STORAGE) requested");
                final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogStyle);
                builder.setTitle("This app needs storage access");
                builder.setMessage("Please grant storage access so this app can export graph data.");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(
                        dialog -> requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_STORAGE));
                builder.show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            @NotNull String[] permissions,
            @NotNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_LOCATION) {
            if (grantResults[0] == PERMISSION_GRANTED) {
                log("bluetooth permission (ACCESS_FINE_LOCATION) granted");
                onGpsEvent(new GpsStateEvent(true)); // act as if GPS was just enabled so service starts
                //connect(); // attempt BLE connection
            } else {
                log("bluetooth permission (ACCESS_FINE_LOCATION) declined");
                final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogStyle);
                builder.setTitle("Functionality limited");
                builder.setMessage(
                        "Since location access has not been granted, this app will not be able to discover beacons when in the background.");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(dialog -> { /* ignore */ });
                builder.show();
            }
        } else if (requestCode == PERMISSION_REQUEST_STORAGE) {
            if (grantResults[0] == PERMISSION_GRANTED) {
                log("storage permission granted");
            } else {
                log("storage permission declined");
            }
        }
    }

    public void onActivityResult(
            int requestCode,
            int resultCode,
            Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_OK) {
            //connect(); // attempts reconnect after bluetooth was enabled on device
        }
    }

    // endregion

    // region GPS service logic

    private boolean locationEnabled;
    private final ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(
                ComponentName name,
                IBinder binder) {
            log("onServiceConnected");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            log("onServiceDisconnected");
        }
    };

    @Subscribe
    public void onGpsEvent(GpsStateEvent event) {
        log("use device GPS " + event.getChecked());
        if (event.getChecked()) {
            enableGpsLocationUpdates();
        } else {
            disableGpsLocationUpdates();
        }
    }

    private void enableGpsLocationUpdates() {
        // no need to enable if already enabled
        if (locationEnabled) {
            return;
        }
        // check for permissions
        if (ActivityCompat.checkSelfPermission(
                MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(
                MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PERMISSION_GRANTED) {

            Toast.makeText(MainActivity.this, "App doesn't have location permissions", Toast.LENGTH_SHORT).show();
            checkBluetoothAndLocation();
            return;
        }
        locationEnabled = true;
        gpsStateView.setColorFilter(ResourcesCompat.getColor(getResources(), R.color.fluo_green, null));
        startGpsService();
    }

    private void startGpsService() {
        startService(GpsService.class);
    }

    private void startService(final Class serviceClass) {
        // start background service for GPS location
        final Intent intent = new Intent(MainActivity.this, serviceClass);
        // to improve background handling on Android 8 and higher run foreground service instead
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent);
            bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
        } else {
            startService(intent);
        }
    }

    private void disableGpsLocationUpdates() {
        locationEnabled = false;
        gpsStateView.setColorFilter(ResourcesCompat.getColor(getResources(), R.color.colorAccent, null));
        stopGpsService();
    }

    private void stopGpsService() {
        stopService(GpsService.class);
    }

    private void stopService(final Class serviceClass) {
        if (serviceConnection != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            try{ unbindService(serviceConnection);}catch(Exception e){ /* ignore */ }
        }
        // stop background service for GPS location
        Intent intent = new Intent(MainActivity.this, serviceClass);
        stopService(intent);
    }

    // endregion

}
