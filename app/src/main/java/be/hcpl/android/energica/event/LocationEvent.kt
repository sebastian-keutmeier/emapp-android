package be.hcpl.android.energica.event

import android.location.Location
import java.io.Serializable

data class LocationEvent(val location: Location) : Serializable
