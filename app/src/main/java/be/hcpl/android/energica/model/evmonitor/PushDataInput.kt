package be.hcpl.android.energica.model.evmonitor

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class PushDataInput(
    @SerializedName("user-agent") val userAgent: String = "Energ1ca App",
    @SerializedName("data[0][general][timestamp]") val timestamp: Long,
    @SerializedName("data[0][general][vehicle]") val vehicle: String,
    // keep null as a default so we don't push 0 values
    // collected from GPS service
    @SerializedName("data[0][general][gps][lat]") val latitude: Double? = null,
    @SerializedName("data[0][general][gps][lng]") val longitude: Double? = null,
    @SerializedName("data[0][general][gps][acc]") val gpsAcc: Float? = null,
    @SerializedName("data[0][general][speed]") val speed: Float? = null,
    // from BLE and OBDII conn. data
    @SerializedName("data[0][general][range]") val range: Int? = null,
    @SerializedName("data[0][general][odometer]") val odometer: Int? = null,
    @SerializedName("data[0][general][watt_100km]") val consumption: Float? = null, //{mean consumption:int}(in watt/100km)

    @SerializedName("data[0][general][batt][0][soc]") val battSoc: Int? = null,
    @SerializedName("data[0][general][batt][0][soh]") val battSoh: Int? = null,
    @SerializedName("data[0][general][batt][0][temp]") val battTemp: Int? = null,
    @SerializedName("data[0][general][batt][0][i_charge]") val battCurrent: Int? = null,

    @SerializedName("data[0][general][motor][0][rpm]") val rpm: Int? = null,
    @SerializedName("data[0][general][motor][0][torque]") val torque: Int? = null,
    @SerializedName("data[0][general][motor][0][watt]") val power: Int? = null,

    @SerializedName("data[0][general][debug]") val debug: String? = null, // debug data
) {


    fun toDataMap(vehicle: String, index: Int) : Map<String, Any> {
        val data = mutableMapOf<String, Any>(
            "user-agent" to this.userAgent,
            "data[${index}][general][timestamp]" to this.timestamp, // full seconds, not ms
            "data[${index}][general][vehicle]" to vehicle, //?:this.vehicle, // overwrites vehicle when set
        )
        // add all optional fields here
        this.latitude?.let{ data.put("data[${index}][general][gps][lat]", this.latitude) }
        this.longitude?.let{ data.put("data[${index}][general][gps][lng]", this.longitude) }
        this.gpsAcc?.let{ data.put("data[${index}][general][gps][acc]", this.gpsAcc) }
        this.speed?.let{ data.put("data[${index}][general][speed]", this.speed) }

        this.range?.let{ data.put("data[${index}][general][range]", this.range) }
        this.odometer?.let{ data.put("data[${index}][general][odometer]", this.odometer) }
        this.consumption?.let{ data.put("data[${index}][general][watt_100km]", this.consumption) }

        this.battSoc?.let{ data.put("data[${index}][general][batt][0][soc]", this.battSoc) }
        this.battSoh?.let{ data.put("data[${index}][general][batt][0][soh]", this.battSoh) }
        this.battTemp?.let{ data.put("data[${index}][general][batt][0][temp]", this.battTemp) }
        this.battCurrent?.let{ data.put("data[${index}][general][batt][0][i_charge]", this.battCurrent) }

        this.rpm?.let{ data.put("data[${index}][general][motor][0][rpm]", this.rpm) }
        this.torque?.let{ data.put("data[${index}][general][motor][0][torque]", this.torque) }
        this.power?.let{ data.put("data[${index}][general][motor][0][watt]", this.power) }

        this.debug?.let{ data.put("data[${index}][general][debug]", this.debug) }

        return data
    }

}

