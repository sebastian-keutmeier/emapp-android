package be.hcpl.android.energica.model.ble;

public class RssiAverager {
    private final int[] mRssiArray;
    private int mRssiArrayIdx;

    public RssiAverager() {
        this.mRssiArray = new int[]{-127, -127, -127, -127};
        this.mRssiArrayIdx = 0;
        this.mRssiArrayIdx = 0;
        for (int i = 0; i < 4; i++) {
            this.mRssiArray[i] = -127;
        }
    }

    public void reset() {
        this.mRssiArrayIdx = 0;
        for (int i = 0; i < 4; i++) {
            this.mRssiArray[i] = -127;
        }
    }

    public void addSample(int rssi) {
        this.mRssiArray[this.mRssiArrayIdx] = rssi;
        this.mRssiArrayIdx++;
        if (this.mRssiArrayIdx >= 4) {
            this.mRssiArrayIdx = 0;
        }
    }

    public int getRssi() {
        int res = 0;
        for (int i = 0; i < 4; i++) {
            res += this.mRssiArray[i];
        }
        return res / 4;
    }
}
