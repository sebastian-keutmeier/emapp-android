package be.hcpl.android.energica.event

data class LastLocationEvent(val latitude: Double, val longitude: Double)
