package be.hcpl.android.energica

import android.os.Build
import android.os.Bundle
import android.os.FileObserver
import android.text.method.ScrollingMovementMethod
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import be.hcpl.android.energica.helpers.ExportData
import be.hcpl.android.energica.helpers.Help
import be.hcpl.android.energica.helpers.Help.withColor
import be.hcpl.android.energica.services.ble.BleService
import be.hcpl.android.energica.services.gps.GpsService
import be.hcpl.android.energica.services.obd2.Obd2Service
import com.google.android.gms.common.stats.StatsEvent
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

class AppLogsActivity : AppCompatActivity(R.layout.activity_data) {

    // toolbar state icons
    private lateinit var gpsStateView: ImageView
    private lateinit var bleStateView: ImageView
    private lateinit var obdStateView: ImageView

    private lateinit var logsView: TextView
    private lateinit var statsview: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // handle toolbar
        val toolbar = findViewById<Toolbar>(R.id.home_topbar)
        setSupportActionBar(toolbar)
        val titleView = findViewById<TextView>(R.id.toolbar_title)
        titleView.text =  getString(R.string.app_logs)

        // state indications
        gpsStateView = findViewById(R.id.gps_state)
        gpsStateView.visibility = View.VISIBLE
        gpsStateView.setColorFilter(withColor(applicationContext, R.color.dark_white))
        bleStateView = findViewById(R.id.ble_state)
        bleStateView.visibility = View.VISIBLE
        bleStateView.setColorFilter(withColor(applicationContext, R.color.dark_white))
        obdStateView = findViewById(R.id.obd_state)
        obdStateView.visibility = View.VISIBLE
        obdStateView.setColorFilter(withColor(applicationContext, R.color.dark_white))

        // stats and app logs
        logsView = findViewById(R.id.data_logs)
        logsView.movementMethod = ScrollingMovementMethod()
        statsview = findViewById(R.id.stats_data)

        // show app version
        val logsLabelView: TextView = findViewById(R.id.logs_label)
        logsLabelView.text = String.format("App Logs for App Version %s build %s", BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE)

        // monitor app logs here
        val logFile = ExportData.getCurrentAppLogFile(applicationContext)
        if( logFile?.exists() == false ){
            logFile.createNewFile()
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q && logFile != null) {
            val observer = object: FileObserver(logFile) {
                override fun onEvent(state: Int, text: String?) {
                    //logsView.append(text)
                    if( state == CLOSE_WRITE || state == CLOSE_NOWRITE) {
                        runOnUiThread { logsView.text = logFile.readText() }
                    }
                }
            }
            observer.startWatching() // TODO need to stop observing on pause and start again on resume?
        } else {
            logsView.text = "Sorry no support for FileObserver used to watch log file of this app (requires Android Q or 29)"
        }

        // manual logs refresh option here
        findViewById<TextView>(R.id.refresh).setOnClickListener { updateLogs() }
    }

    // region lifecycle

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    override fun onResume() {
        super.onResume()
        // update state of services
        updateServiceStates()
        updateLogs()
    }

    private fun updateLogs() {
        // load logs
        val logFile = ExportData.getCurrentAppLogFile(applicationContext)
        logsView.text = logFile?.readText().orEmpty()
    }

    // endregion

    // region stats

    private fun updateServiceStates() {
        // TODO move this boilerplate code away from all views
        val gpsServiceRunning = Help.isServiceRunning(applicationContext, GpsService::class.java)
        val bleServiceRunning = Help.isServiceRunning(applicationContext, BleService::class.java)
        val obd2ServiceRunning = Help.isServiceRunning(applicationContext, Obd2Service::class.java)
        gpsStateView.setColorFilter(Help.withStateColor(applicationContext, gpsServiceRunning))
        bleStateView.setColorFilter(Help.withStateColor(applicationContext, bleServiceRunning))
        obdStateView.setColorFilter(Help.withStateColor(applicationContext, obd2ServiceRunning))
    }

    @Subscribe
    fun onData(event: StatsEvent){
        statsview.text = event.toString()
    }

    // endregion

    // region menu

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.close_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.close -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    // endregion

}