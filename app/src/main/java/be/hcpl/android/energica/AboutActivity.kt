package be.hcpl.android.energica

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import java.io.File
import java.io.IOException
import androidx.core.content.FileProvider
import be.hcpl.android.energica.helpers.ExportData.Companion.log
import be.hcpl.android.energica.helpers.Help
import be.hcpl.android.energica.helpers.Help.withColor
import be.hcpl.android.energica.services.ble.BleService
import be.hcpl.android.energica.services.gps.GpsService
import be.hcpl.android.energica.services.obd2.Obd2Service

class AboutActivity : AppCompatActivity(R.layout.activity_about) {

    // toolbar state icons
    private lateinit var gpsStateView: ImageView
    private lateinit var bleStateView: ImageView
    private lateinit var obdStateView: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        val titleView = findViewById<TextView>(R.id.toolbar_title)
        titleView.text = getString(R.string.menu_about)

        // app version
        findViewById<TextView>(R.id.about_version).text = String.format("App Version %s build %s", BuildConfig.VERSION_NAME, BuildConfig
            .VERSION_CODE)

        // share device crash logs
        findViewById<Button>(R.id.send_logs).setOnClickListener { saveLogcatToFile(this) }

        // state indications
        gpsStateView = findViewById(R.id.gps_state)
        gpsStateView.visibility = View.VISIBLE
        gpsStateView.setColorFilter(withColor(applicationContext, R.color.dark_white))
        bleStateView = findViewById(R.id.ble_state)
        bleStateView.visibility = View.VISIBLE
        bleStateView.setColorFilter(withColor(applicationContext, R.color.dark_white))
        obdStateView = findViewById(R.id.obd_state)
        obdStateView.visibility = View.VISIBLE
        obdStateView.setColorFilter(withColor(applicationContext, R.color.dark_white))
    }

    override fun onResume() {
        super.onResume()
        // update state of services
        updateServiceStates()
    }

    private fun updateServiceStates() {
        val gpsServiceRunning = Help.isServiceRunning(applicationContext, GpsService::class.java)
        val bleServiceRunning = Help.isServiceRunning(applicationContext, BleService::class.java)
        val obd2ServiceRunning = Help.isServiceRunning(applicationContext, Obd2Service::class.java)
        gpsStateView.setColorFilter(Help.withStateColor(applicationContext, gpsServiceRunning))
        bleStateView.setColorFilter(Help.withStateColor(applicationContext, bleServiceRunning))
        obdStateView.setColorFilter(Help.withStateColor(applicationContext, obd2ServiceRunning))
    }

    // region menu

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.close_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.close -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    // endregion

    private fun saveLogcatToFile(context: Context) {
        val appName = getString(R.string.app_name)
        val emailIntent = Intent(Intent.ACTION_SEND)
        emailIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        emailIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        emailIntent.type = "text/plain"
        emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(getString(R.string.dev_contact)))
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "$appName Debug Logs")
        val sb = StringBuilder()
        sb.append("\nManufacturer: ").append(Build.MANUFACTURER)
        sb.append("\nModel: ").append(Build.MODEL)
        sb.append("\nRelease: ").append(Build.VERSION.RELEASE)
        emailIntent.putExtra(Intent.EXTRA_TEXT, sb.toString())
        val fileName = "logcat_${System.currentTimeMillis()}.txt"
        val filePath = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
        val outputFile = File(filePath, fileName)
        val uri = FileProvider.getUriForFile(
            context,
            context.applicationContext.packageName.toString() + ".provider",
            outputFile
        )
        emailIntent.putExtra(Intent.EXTRA_STREAM, uri)
        log( "Going to save logcat to $outputFile")
        context.startActivity(Intent.createChooser(emailIntent, "Pick an Email provider").addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
        try {
            Runtime.getRuntime().exec("logcat -f " + outputFile.absolutePath)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    private fun log(message: String) {
        log(applicationContext, message)
    }

}
