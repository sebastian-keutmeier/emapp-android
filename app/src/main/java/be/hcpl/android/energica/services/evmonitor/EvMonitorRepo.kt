package be.hcpl.android.energica.services.evmonitor

import android.content.Context
import android.content.SharedPreferences
import android.widget.Toast
import be.hcpl.android.energica.R
import be.hcpl.android.energica.config.HybridBatteryPackCommand
import be.hcpl.android.energica.config.OdoMeterCommand
import be.hcpl.android.energica.helpers.ExportData
import be.hcpl.android.energica.model.ble.VehicleStatus
import be.hcpl.android.energica.model.evmonitor.LoginOutput
import be.hcpl.android.energica.model.evmonitor.PushDataInput
import be.hcpl.android.energica.model.evmonitor.PushDataOutput
import be.hcpl.android.energica.model.evmonitor.Vehicle
import be.hcpl.android.energica.model.evmonitor.VehiclesOutput
import com.github.pires.obd.commands.ObdCommand
import com.github.pires.obd.commands.SpeedCommand
import com.github.pires.obd.commands.engine.RPMCommand
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.math.max

class EvMonitorRepo(
    private val context: Context,
    private val prefs: SharedPreferences
) {

    private var service: EvMonitorApiService = EvMonitorApiServiceImpl().getService()
    private val data = mutableListOf<PushDataInput>() // local data

    companion object {

        private const val MAX_DATA_SIZE_GPS = 60 // how much data to keep local before pushing
        private const val MAX_DATA_SIZE_OBD = 10 // how much data to keep local before pushing
        private const val MAX_DATA_SIZE_BLE = 30 // how much data to keep local before pushing
        private const val RESPONSE_OK = "OK" // what we receive when service response was OK

    }

    private fun log(message: String){
        ExportData.log(context, message)
    }

    /**
     * get new token with configured username and password combination
     */
    private fun updateToken(withToken: (token: String, vehicle: String) -> Unit) {
        // TODO check if we can keep track of time for tokens?
        // less of an issue if we flush all data from time to time
        val username = prefs.getString(context.getString(R.string.key_evmonitor_username), null)
        val password = prefs.getString(context.getString(R.string.key_evmonitor_password), null)
        if( username == null || password == null ){
            log("failed to login for token - no login configured")
            Toast.makeText(context, "ev-monitor login failed, check config", Toast.LENGTH_SHORT).show()
            return
        }
        service.login(username, password).enqueue(object : Callback<LoginOutput> {

            override fun onResponse(call: Call<LoginOutput>, response: Response<LoginOutput>) {
                if (!response.isSuccessful) {
                    log("failed to login for token")
                    Toast.makeText(context, "ev-monitor login failed, check config", Toast.LENGTH_SHORT).show()
                    return
                }
                response.body()?.let {
                    val vehicle = prefs.getString(context.getString(R.string.key_evmonitor_vehicle), null).orEmpty()
                    withToken(it.token.orEmpty(), vehicle)
                }
            }

            override fun onFailure(call: Call<LoginOutput>, t: Throwable) {
                log("failed to login for token - " + t.localizedMessage)
                Toast.makeText(context, "ev-monitor login failed, check config", Toast.LENGTH_SHORT).show()
            }
        })
    }

    /**
     * retrieve all vehicles configured for this user
     */
    fun vehicles(withVehicles: (vehicles: List<Vehicle>) -> Unit) {
        updateToken { token, currentVehicleId ->
            service.vehicles(token).enqueue(object : Callback<VehiclesOutput> {

                override fun onResponse(
                    call: Call<VehiclesOutput>,
                    response: Response<VehiclesOutput>
                ) {
                    if (!response.isSuccessful) {
                        log("failed to retrieve vehicle data")
                        Toast.makeText(context, "ev-monitor get vehicles failed, check config", Toast.LENGTH_SHORT).show()
                        return
                    }
                    response.body()?.let {
                        val availableVehicles =
                            it.vehicles?.map { vehicle -> vehicle.copy(selected = vehicle.id == currentVehicleId) }
                                .orEmpty()
                        withVehicles(availableVehicles)
                    }
                }

                override fun onFailure(call: Call<VehiclesOutput>, t: Throwable) {
                    log("failed to retrieve vehicle data - " + t.localizedMessage)
                    Toast.makeText(context, "ev-monitor get vehicles failed, check config", Toast.LENGTH_SHORT).show()
                }
            })
        }
    }

    /**
     * force push all data to remote, use this on finishing services for example
     */
    fun flushData() {
        val dataSet = data.toList()
        data.clear()
        // push all data we've collected at this point, to be triggered when a service stops for example
        updateToken { token, vehicle ->
            // construct a map for all data
            val dataMap = mutableMapOf<String, Any>()
            dataSet.forEachIndexed { index, element ->
                dataMap.putAll(element.toDataMap(vehicle, index))
            }
            // actual data push goes here
            service.pushData(token, dataMap).enqueue(object : Callback<PushDataOutput> {

                override fun onResponse(
                    call: Call<PushDataOutput>,
                    response: Response<PushDataOutput>
                ) {
                    if (!response.isSuccessful) {
                        log("failed to push data")
                        Toast.makeText(context, "ev-monitor push data failed, check config", Toast.LENGTH_SHORT).show()
                        recoverData(dataSet)
                        return
                    }
                    response.body()?.let {
                        if( it.SUCCESS != RESPONSE_OK){
                            log("failed to push data - " + it.error)
                            Toast.makeText(context, "ev-monitor push data failed, check config", Toast.LENGTH_SHORT).show()
                            recoverData(dataSet)
                        }
                        log("pushed data, response was ${it.SUCCESS}")
                    }
                }

                override fun onFailure(call: Call<PushDataOutput>, t: Throwable) {
                    log("failed to push data - " + t.localizedMessage)
                    Toast.makeText(context, "ev-monitor push data failed, check config", Toast.LENGTH_SHORT).show()
                    recoverData(dataSet)
                }
            })
        }
    }

    private fun recoverData(dataSet: List<PushDataInput>) {
        data.addAll(dataSet) // put data back since we failed to publish
    }

    /**
     * record location information from GPS
     */
    fun pushLocation(latitude: Double, longitude: Double, accuracy: Float, speed: Float) {
        // block if feature isn't enabled
        if (!prefs.getBoolean(context.getString(R.string.key_enable_evmonitor_location), false)) return
        // also don't when cloud services aren't enabled
        if (!prefs.getBoolean(context.getString(R.string.key_enable_evmonitor), false)) return

        // prevent pushing empty values
        if( latitude == 0.0 && longitude == 0.0 && accuracy == 0f && speed == 0f) return
        // add some form of data collection and push set from time to time to reduce amount of requests
        data.add(
            PushDataInput(
                timestamp = System.currentTimeMillis()/1000,
                vehicle = "", // update vehicle on pushing data vehicle = vehicle,
                gpsAcc = accuracy,
                latitude = latitude,
                longitude = longitude,
                speed = speed
            )
        )
        // if enough data collected flush
        if (data.size > MAX_DATA_SIZE_GPS) flushData()
    }

    /**
     * push energica specific data (batt. temp, soh, soc, charge current)
     */
    fun pushEnergicaObd2Data(temp1: Int, temp2: Int, soh: Int, soc: Int, currentAc: Int, currentDc: Int) {
        //  don't when cloud services aren't enabled
        if (!prefs.getBoolean(context.getString(R.string.key_enable_evmonitor), false)) return

        // debug data separately handled (since it would be 0 anyway), see #pushEnergicaRawData

        // prevent pushing empty values
        if( temp1 == 0 && temp2 == 0 && soh == 0 && soc == 0 && currentAc == 0 && currentDc == 0) return
        // prepare data to be pushed
        data.add(
            PushDataInput(
                timestamp = System.currentTimeMillis()/1000,
                vehicle = "", // update vehicle on pushing data vehicle = vehicle,
                battTemp = max(temp1, temp2), // always log highest battery temp
                battSoc = soc,
                battSoh = soh,
                battCurrent = max(currentAc, currentDc) // always logs highest charge current (up to 13-15A for AC, up to 75A for DC)
            )
        )
        // if enough data collected flush
        if (data.size > MAX_DATA_SIZE_OBD) flushData()
    }

    /**
     * push generic obd2 data, can be used for zero and other vehicles than energica
     */
    fun pushGenericObd2Data(command: ObdCommand) {
        //  don't when cloud services aren't enabled
        if (!prefs.getBoolean(context.getString(R.string.key_enable_evmonitor), false)) return

        // prepare data to be pushed
        var pushData = PushDataInput(
            timestamp = System.currentTimeMillis()/1000,
            vehicle = "", // update vehicle on pushing data vehicle = vehicle,
        )
        // enrich with debug data when feature is enabled
        if( prefs.getBoolean(context.getString(R.string.key_enable_evmonitor_debug), false) ){
            pushData = pushData.copy(debug = command.result)
        }
        when (command) {
            // is ModuleVoltageCommand -> pushData.copy() // maybe log 12V to data[0][general][batt][{battindex}][v]:{v:int}(Battery Voltage in V)
            // is AmbientAirTemperatureCommand -> ...
            // is EngineCoolantTemperatureCommand -> ...
            is RPMCommand -> data.add(pushData.copy(rpm = command.rpm))
            is SpeedCommand -> data.add(pushData.copy(speed = speedInPreferredUnit(command)))
            is HybridBatteryPackCommand -> data.add(pushData.copy(battSoh = command.calculatedValue())) // Zero has it implemented as SOC
            // is EVehicleSystemDataCommand -> ... //ObdRawCommand("01 9A"), // "Hybrid/EV Vehicle System Data"),
            is OdoMeterCommand -> data.add(pushData.copy(odometer = command.calculatedValue()))
        }
        // if enough data collected flush
        if (data.size > MAX_DATA_SIZE_OBD) flushData()
    }

    private fun speedInPreferredUnit(command: SpeedCommand): Float {
        return if (context.getString(R.string.value_imperial) == context.getString(R.string.key_unit_system))
            command.imperialSpeed
        else
            command.metricSpeed.toFloat()
    }

    /**
     * publish energica specific BLE data
     */
    fun pushEnergicaBleData(status: VehicleStatus) {
        //  don't when cloud services aren't enabled
        if (!prefs.getBoolean(context.getString(R.string.key_enable_evmonitor), false)) return

        // prevent pushing empty values
        if( status.isEmpty ) return
        // add some form of data collection and push set from time to time to reduce amount of requests
        data.add(
            PushDataInput(
                timestamp = System.currentTimeMillis()/1000,
                vehicle = "", // update vehicle on pushing data vehicle = vehicle,
                speed = status.speed.toFloat(),
                range = status.range,
                odometer = status.totalOdometer.toInt(),
                consumption = status.instKwh100Km,
                rpm = status.rpm,
                torque = status.torque,
                power = status.power,
                battSoc = status.soc
            )
        )
        // if enough data collected flush
        if (data.size > MAX_DATA_SIZE_BLE) flushData()
    }

    /**
     * push generic raw data when debug option is enabled
     */
    fun pushEnergicaRawData(rawData: String) {
        //  don't when cloud services aren't enabled
        if (!prefs.getBoolean(context.getString(R.string.key_enable_evmonitor), false)) return
        // debug data separately handled
        if( !prefs.getBoolean(context.getString(R.string.key_enable_evmonitor_debug), false) ) return
        // don't push empty raw responses
        if( rawData.isBlank() ) return

        // prepare data to be pushed
        data.add(
            PushDataInput(
                timestamp = System.currentTimeMillis()/1000,
                vehicle = "", // update vehicle on pushing data vehicle = vehicle,
                debug = rawData
            )
        )
        // if enough data collected flush
        if (data.size > MAX_DATA_SIZE_OBD) flushData()
    }

}